app.directive('schoolairqual', function() {
  return {
    template: [

      //alert banner
      '<div id="airqual-alert" style="display:none;"></div>',

    ].join(''),
    scope: {},
    controller: 'schoolairqualCtrl',
  }
})
app.controller('schoolairqualCtrl', function($timeout, $rootScope, $scope, $state, $stateParams, siteList) {

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      if (sitelist && sitelist.length) {
        //init sequence
        sites = sitelist;
        sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
        if ($stateParams.siteid != '') {
          aq_activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
          initAqBanner();
        }
      } else {
        //timeout re-call
        var retry = function() {
          setTimeout(function() {
            loadReqs();
          },100);
        }
        retry();
      }
    });
  };

  function initAqBanner() {
    //hide banner while loading
    d3.select('#airqual-alert').style('display','none');
    //lookup closest station
    d3.csv('/qClosestAqStation/?lat=' + aq_activesite['lat'] + '&lng=' + aq_activesite['lng'],function(station) {
      //grab that station's data
      d3.csv('/qAqStationData/?id=' + station[0].id + '&lat=' + station[0].lat + '&lng=' + station[0].lng,function(data) {
        aq_activedata = data;
        let banner = d3.select('#airqual-alert')
        banner.append('span')
          .style('float','left')
          .style('font-size','15px')
          .style('font-weight',600)
          .style('margin-right','10px')
          .text('Volcanic Activity Air Quality Data');
        banner.append('span')
          .style('float','left')
          .style('position','relative')
          .style('top','2px')
          .html('<span><a href="https://www.airnow.gov/index.cfm?action=airnow.local_state&stateid=12" target="_blank">AIRNOW</a> Air Quality Monitoring Station ' + Math.floor(station[0].dist * 100)/100 + ' km away</span>');
        banner.append('i')
          .style('float','left')
          .style('margin','0px 5px 0px 20px')
          .style('position','relative')
          .style('top','-2px')
          .style('font-size','20px')
          .style('border','solid 1px #025468')
          .style('border-radius','5px')
          .attr('class','material-icons')
          .style('cursor','pointer')
          .text('file_download')
          .on('mouseover',function() {
            this.style.color = '#fbb157'
            this.style.border = 'solid 1px #fbb157'
            d3.select('#aqbanner-dltext')
              .transition().delay(0).duration(500)
              .style('display','block')
              .style('opacity',1)
          })
          .on('mouseout',function() {
            this.style.color = null
            this.style.border = 'solid 1px #025468'
            d3.select('#aqbanner-dltext')
              .transition().delay(0).duration(500)
              .style('display','none')
              .style('opacity',0)
          })
          .on('click',function() {
            let csvdata = []
            aq_activedata.forEach(function(d) {
              let row = {
                'As_Location':d['location'],
                'As_Lat':+d['lat'],
                'As_Lng':+d['lng'],
                'Timestamp':d['dt_local'],
                'SO2_Conc':d['SO2_ppb'],
                'SO2_Aqi':d['SO2_aqi'],
                'PM2.5_Conc':d['PM2pt5_μg/m3'],
                'PM2.5_Aqi':d['PM2pt5_aqi'],
                'PM10_Conc':d['PM10_μg/m3'],
                'PM10_Aqi':d['PM10_aqi'],
                'CO_Conc':d['CO_ppm'],
                'CO_Aqi':d['CO_aqi'],
                'O3_Conc':d['OZONE_ppb'],
                'O3_Aqi':d['OZONE_aqi']
              };
              csvdata.push(row);
            });
            csvdata.sort(function(a,b) { return d3.ascending(a.Timestamp, b.Timestamp); });
            let meta = ( 
              'data provided by MKThink + RoundhouseOne,' +
              'data sourced from AIRNOW,' +
              'station location: ' + station[0]['location'] + ' (' + Math.floor(station[0].dist * 100)/100 + ' km away),' +
              'station coordinates: ' + station[0]['lat'] + '|' + station[0]['lng'] + ',' +
              'selected date range: 2018-01-01 : ' + formatDate(new Date()) + ',' +
              'file exported on: ' + new Date() + ',').split(',')
            let filenm = 'AirQuality_' + aq_activesite.site_nm_long.split(' ').join('') + '_' + formatDateTimeMatch(new Date()) + '.csv'
            let fields = (Object.keys(csvdata[0]))
            let units = ['','(Degrees)','(Degrees)','(HST)','(Parts per Billion)','(Air Quality Index)','(Micrograms per Cubic Meter)','(Air Quality Index)','(Parts per Million)','(Air Quality Index)','(Parts per Billion)','(Air Quality Index)']
            let replacer = function(key,value) { return value === null ? '' : value }
            //structure csv content
            let csv = csvdata.map(function(row) { // add data rows
              let line =  fields.map(function(field) { 
                return JSON.stringify(row[field],replacer);
              }).join(',');
              return  '\r\n' + line
            });
            let csv3 = [meta.join('\r\n'),fields,units,csv].join('\r\n') //add metadata rows
            let file = new Blob([csv3], {type:'application/csv;charset=utf-8;'});
            //download file
            if (navigator.msSaveBlob) { //iE10
              navigator.msSaveBlob(file,filenm)
            } else { //other browser
              let a = document.createElement('a')
              let url = URL.createObjectURL(file)
              a.href = url
              a.style = 'visibility:hidden'
              a.download = filenm
              document.body.appendChild(a)
              a.click()
              setTimeout(function() {
                document.body.removeChild(a)
              },3000)
            }
          });
          banner.append('span')
            .attr('id','aqbanner-dltext')
            .style('position','relative')
            .style('top','3px')
            .style('float','left')
            .style('display','none')
            .style('opacity',0)
            .style('color','#fbb157')
            .text('download 2018 data')
          setTimeout(function() {
            banner.style('display','block')
          },500)
      });
    });
  }
});



