app.directive('home', function() {
  return {
    template: [

      //home panel
      '<div id="home-pan">',
        //descr element
        '<div class="descr">',
            '<span>Welcome to the Thermal Comfort portal for the Hawaiʻi State Department of Education.</span><span style="font-weight:200; font-style:italic;"> If you are new to the site or have any questions, please visit the  </span>',
            '<a href="#/keepcool#kc-faqs">FAQ</a>',
            '<span style="font-weight:200; font-style:italic;">  page.</span>',
        '</div>',
        //nav shortcut 1 element
        '<div class="navopt">',
          '<div style="font-size:12px;"><i>View outdoor weather conditions and indoor classroom temperatures here:</i></div>',
          '<div class="menu" id="home-sitemenu"><select></select></div>',
          '<div id="gobutton">GO</div>',
        '</div>',
        //nav shortcut 2 element
        '<div class="navopt">',
          '<div style="font-size:12px;"><i>View detailed historical data and compare multiple schools and dates here:</i></div>',
          '<div class="infobutton" id="home-navdtbutton">Data Trends Tool</div>',
        '</div>',
        //nav shortcut 3 element
        '<div class="navopt">',
          '<div style="font-size:12px;"><i>Learn about strategies to improve thermal comfort:</i></div>',
          '<div class="infobutton" id="home-navkcbutton">Cool Strategies</div>',
        '</div>',
      '</div>',
      //map element
      '<div id="home-map"></div>',
      '<div id="home-legends">',
        '<div id="home-sitelegend"></div>',
        '<div id="home-astalegend"></div>',
      '</div>',

    ].join(''),
    scope: {},
    controller: 'homeCtrl',
  }
})
app.controller('homeCtrl', function($timeout, $rootScope, $scope, $state, $stateParams, siteList) {

  /*check content-wrapper height*/
  resizeContentWrapper();
  /*update nav-bar styling*/
  d3.selectAll(".nav-button").classed("active-nav", false);
  d3.selectAll(".nav-button").classed("alt-nav", true);
  d3.select("#home-nav").classed("alt-nav", false);
  d3.select("#home-nav").classed("active-nav", true);

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      if (sitelist && sitelist.length) {
        loadAqData($stateParams.homemap.airqmeasure,function(astalist) {
          sites = sitelist;
          sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
          astas = astalist;
          sitelabels = [];
          astalabels = [];
          //init sequence
          initSiteSelector();
          initMap();
        });
      }
      else {
        //timeout re-call
        var retry = function() {
          setTimeout(function() {
            loadReqs();
          },100);
        }
        retry();
      }
    });
  };

  function loadAqData(param,callback) {
    d3.csv('/qAirQualStations/?param=' + param,function(res) {
      callback(res);
    });
  };
  
  function initMap() {
    //load style
    d3.json('./static/scripts/components/resources/gmap.json',function(gmapstyle) {
      //init gmap object
      map = new google.maps.Map(document.getElementById('home-map'),{
        center: new google.maps.LatLng(20.613834,-157.473497),
        zoom:8,
        gestureHandling: 'greedy',
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        options: {
          streetViewControl:false,
          mapTypeControl:false,
          fullscreenControl:false,
        },
        styles:gmapstyle,
      });
      //load data objects
      loadOverlays(0,'full');
      function loadOverlays(globaltransition,phase) {
        //populate legend
        if (phase == 'full') {
          initLegends();
        }
        //init overlay
        overlay = new google.maps.OverlayView();
        overlay.onAdd = function() {

          //RENDERING ORDER
          adlayer = d3.select(this.getPanes().overlayMouseTarget) //airquality diamond layer
            .append('div')
              .attr('class','layer');
          sclayer = d3.select(this.getPanes().overlayMouseTarget) //site circle layer
            .append('div')
              .attr('class','layer');
          sllayer = d3.select(this.getPanes().overlayMouseTarget) //site label layer
            .append('div')
              .attr('class','layer');
          allayer = d3.select(this.getPanes().overlayMouseTarget) //airquality label layer
            .append('div')
              .attr('class','layer');

          //DRAW ON LOAD
          overlay.draw = function() {
            let projection = this.getProjection(),
                padding = map.getZoom() <= 8? 1.5:(map.getZoom() - 7.5)*4;

            //SITE CIRCLE MARKERS
            if ($stateParams.homemap.sites == true) {
              let sc = sclayer.selectAll('svg')
                  .data(sites)
                  .each(transform)
                .enter().append('svg')
                  .each(transform)
                  .attr('id',function(d) { return 'sc' + d.site_id })
                  .attr('width',padding*2)
                  .attr('height',padding*2)
                  .attr('transform','translate(' + (-padding) + ',' + (-padding) + ')');
              sc.append('circle')
                .attr('class','siteobj')
                .attr('r',padding/2)
                .attr('cx',padding)
                .attr('cy',padding)
                .on('mouseover',function(d) {
                  d3.select(this)
                    .attr('r',padding/1.25)
                    .style('fill-opacity',0.75)
                })
                .on('mouseout',function(d) {
                  d3.select(this)
                    .attr('r',padding/2)
                    .style('fill-opacity',0.33)
                })
                .on('click',function(d) {
                  if (sitelabels.indexOf(d.site_id) == -1) {
                    sitelabels.push(d.site_id);
                    //resize svg to reveal label
                    d3.select('#sl' + d.site_id).attr('width',padding*2 + getTextWidth(d.site_nm_long,13,'Arial') + 50)
                  } else {
                    sitelabels.splice(sitelabels.indexOf(d.site_id),1);
                    //resize svg to hide label
                    d3.select('#sl' + d.site_id).attr('width',padding*2)
                  }
                });
            }

            //AIRQUALITY DIAMOND MARKERS
            if ($stateParams.homemap.airqual == true) {
              let ad = adlayer.selectAll('svg')
                  .data(astas)
                  .each(transform)
                .enter().append('svg')
                  .each(transform)
                  .attr('width',padding*4.5)
                  .attr('height',padding*4.5)
                  .attr('transform','translate(' + (-padding/2.25) + ',' + (-padding/2.25) + ') rotate(45)')
                  .attr('id',function(d) { return 'sc' + d.asta_id });
              ad.append('rect')
                .attr('class','astaobj')
                .attr('width',padding*3)
                .attr('height',padding*3)
                .attr('x',padding/2.25)
                .attr('y',padding/2.25)
                .style('stroke',function(d) { return d.hexcol })
                .style('fill',function(d) { return d.hexcol })
                .on('mouseover',function(d) {
                  d3.select(this)
                    .attr('width',padding*4.5)
                    .attr('height',padding*4.5)
                    .attr('x',0)
                    .attr('y',0)
                    .style('fill-opacity',0.75);
                })
                .on('mouseout',function(d) {
                  d3.select(this)
                    .attr('width',padding*3)
                    .attr('height',padding*3)
                    .attr('x',padding/2.25)
                    .attr('y',padding/2.25)
                    .style('fill-opacity',0.33)
                })
                .on('click',function(d) {
                  if (astalabels.indexOf(d.asta_id) == -1) {
                    //pan to loc
                    map.panTo(new google.maps.LatLng(d.lat, d.lng));
                    //replace label object value
                    astalabels = [d.asta_id];
                    //render layers
                    d3.selectAll('.layer').remove();
                    loadOverlays(3000,'full');
                  } else {
                    astalabels = [];
                    //resize svg to hide label
                    d3.select('#al' + d.asta_id).attr('width',padding*4.5);
                  }
                });
            }

            //SITE LABELS
            if ($stateParams.homemap.sites == true && map.getZoom() > 10) {
              let sl = sllayer.selectAll('svg')
                  .data(sites)
                  .each(transform)
                .enter().append('svg')
                  .each(transform)
                  .attr('id',function(d) { return 'sl' + d.site_id })
                  .attr('class','label')
                  .attr('width',function(d) { return sitelabels.indexOf(d.site_id) == -1? padding*2: padding*2 + getTextWidth(d.site_nm_long,13,'Arial') + 50 })
                  .attr('height',padding*2)
                  .attr('transform','translate(' + (-padding) + ',' + (-padding) + ')');
              sl.append('rect')
                .attr('width',function(d) { return getTextWidth(d.site_nm_long,13,'Arial') + 12 })
                .attr('height',padding)
                .attr('x',padding*2 + 1)
                .attr('y',padding/2)
                .style('stroke','#fbb157');
              sl.append('text')
                .attr('x',padding*2 + 6)
                .attr('y',padding/2 + (padding - 13)/2 + 10)
                .attr('fill','#fbb157')
                .text(function(d) { return d.site_nm_long });
              sl.append('rect')
                .attr('width',30)
                .attr('height',padding)
                .attr('x',function(d) { return  padding*2 + getTextWidth(d.site_nm_long,13,'Arial') + 17 })
                .attr('y',padding/2)
                .style('stroke','#fbb157')
                .style('pointer-events','all')
                .style('cursor','pointer')
                .on('mouseover',function(d) {
                  d3.select(this).transition().delay(0).duration(500).style('fill','#fbb157')
                  d3.select(d3.select(this.parentNode).selectAll('text')._groups[0][1]).transition().delay(0).duration(500).attr('fill','#ffffff')
                })
                .on('mouseout',function(d) {
                  d3.select(this).transition().delay(0).duration(500).style('fill','#ffffff')
                  d3.select(d3.select(this.parentNode).selectAll('text')._groups[0][1]).transition().delay(0).duration(500).attr('fill','#fbb157')
                })
                .on('click',function(d) {
                  navToProfile(d.site_id);
                })
              sl.append('text')
                .attr('x',function(d) { return  padding*2 + getTextWidth(d.site_nm_long,13,'Arial') + 32 })
                .attr('y',padding/2 + (padding - 13)/2 + 10)
                .attr('fill','#fbb157')
                .attr('text-anchor','middle')
                .text('GO');
            }

            //AIRQUALITY LABELS
            if ($stateParams.homemap.airqual == true && map.getZoom() > 7) {
              let al = allayer.selectAll('svg')
                  .data(astas)
                  .each(transform)
                .enter().append('svg')
                  .each(transform)
                  .attr('id',function(d) { return 'al' + d.asta_id })
                  .attr('class','label')
                  .attr('width',function(d) { return astalabels.indexOf(d.asta_id) == -1? padding*4.5: padding*4.5 + 262 })
                  .attr('height',function(d) { return d.aqi == 'NO DATA'? 75: 152 })
                  .attr('transform','translate(0,' + (-padding) + ')');
              al.append('rect')
                .attr('width',230)
                .attr('height',function(d) { return d.aqi == 'NO DATA'? 73: 150 })
                .attr('x',padding*4.5 + 1)
                .attr('y',1)
                .style('stroke',function(d) { return d.hexcol });
              al.append('rect')
                .attr('width',20)
                .attr('height',20)
                .attr('x',padding*4.5 + 236)
                .attr('y',1)
                .style('stroke',function(d) { return d.hexcol })
                .style('pointer-events','all')
                .style('cursor','pointer')
                .on('mouseover',function(d) {
                  d3.select(this).transition().delay(0).duration(500).style('fill',d.hexcol);
                  d3.select(d3.select(this.parentNode).selectAll('text')._groups[0][0]).transition().delay(0).duration(500).attr('fill','#ffffff');
                })
                .on('mouseout',function(d) {
                  d3.select(this).transition().delay(0).duration(500).style('fill','#ffffff');
                  d3.select(d3.select(this.parentNode).selectAll('text')._groups[0][0]).transition().delay(0).duration(500).attr('fill',d.hexcol);
                })
                .on('click',function(d) {
                  astalabels=[];
                  d3.select(this.parentNode).attr('width',padding*4.5);
                });
              al.append('text')
                .attr('x',padding*4.5 + 246)
                .attr('y',15)
                .attr('fill',function(d) { return d.hexcol })
                .attr('text-anchor','middle')
                .text('X');
              astalabels.forEach(function(d) {
                let asta = astas.filter(function(f) { return f.asta_id == d })[0];
                drawAirQualityWidget(asta,globaltransition);
              });
            }

            //LOCAL HELPERS
            function transform(d) {
              d = new google.maps.LatLng(d.lat,d.lng);
              d = projection.fromLatLngToDivPixel(d);
              return d3.select(this)
                .style('left', (d.x) + 'px')
                .style('top', (d.y) + 'px');
            };

            function drawAirQualityWidget(d,transition) {
              //clear existing widget
              d3.select('#al' + d.asta_id).selectAll('svg').remove()

              //write measurement info/status
              let viz = d3.select('#al' + d.asta_id).append('svg')
              let location = viz.append('text')
                .attr('class','heavy')
                .attr('text-anchor','start')
                .attr('x',padding*4.5 + 5)
                .attr('y',20)
              location
                .append('tspan')
                  .text(d.location)
              let timestamp = viz.append('text')
                .attr('class','light')
                .attr('text-anchor','start')
                .attr('x',padding*4.5 + 5)
                .attr('y',35)
              timestamp
                .append('tspan')
                  .text(d.dt_local)
              let pollutant = viz.append('text')
                .attr('class','light')
                .attr('text-anchor','start')
                .attr('x',padding*4.5 + 5)
                .attr('y',48)
              pollutant
                .append('tspan')
                  .text(d.descr0)
                .append('tspan').attr('baseline-shift','sub')
                  .text(d.descr1)
                .append('tspan')
                  .text(d.descr2 + ' Concentration')

              if (d.aqi !== 'NO DATA') {
                //add aqi,conc values
                viz.append('text')
                  .attr('x',padding*4.5 + 187)
                  .attr('y',72)
                  .attr('text-anchor','middle')
                  .attr('class','light')
                  .text('air quality index')
                viz.append('text')
                  .attr('x',padding*4.5 + 187)
                  .attr('y',88)
                  .attr('text-anchor','middle')
                  .attr('class','heavy')
                  .text(d.aqi)
                let conc = viz.append('text')
                  .attr('x',padding*4.5 + 187)
                  .attr('y',101)
                  .attr('text-anchor','middle')
                  .attr('class','light')
                conc.append('tspan').text('(' + d.value + ' ' + d.units0)
                conc.append('tspan').attr('baseline-shift','super').text(d.units1)
                conc.append('tspan').text(')')
                //add airnow info button
                let infobutton = viz.append('foreignObject')
                  .attr('width',20).attr('height',20)
                  .attr('x',padding*4.5 + 10)
                  .attr('y',65)
                infobutton.append('xhtml:i')
                  .style('font-size','18px')
                  .attr('class','material-icons')
                  .style('pointer-events','all')
                  .style('cursor','pointer')
                  .text('info_outline')
                  .on('click',function() {
                    let url = 'https://www.airnow.gov/index.cfm?action=airnow.local_state&stateid=12'
                    window.open(url,'_blank');
                  });
                //load indexes, draw guage scale
                d3.json('./static/scripts/components/resources/aqis.json',function(aqindex) {
                  let w = 226,
                      h = 148,
                      m = {top:65,left:15,bottom:5,right:40},
                      r = w/2 - m.right,
                      i = r*0.7;
                  let pieFunc = d3.pie()
                    .sort(null) 
                    .value(function(f) { return f.maxv - f.minv });
                  let arcFunc = d3.arc()
                    .startAngle(function(f) { return f.startAngle/2 - (90*Math.PI/180) })
                    .endAngle(function(f) { return f.endAngle/2 - (90*Math.PI/180) })
                    .outerRadius(r)
                    .innerRadius(i);
                  viz.append('svg')
                    .attr('width',w)
                    .attr('height',h)
                    .attr('x',padding*4.5 + 1)
                    .attr('y',1)
                    .append('g')
                      .attr('transform','translate(' + (r + m.left) + ',' + (r + m.top) + ')')
                      .selectAll('path')
                        .data(pieFunc(aqindex))
                        .enter()
                        .append('path')
                          .attr('stroke-width','2px')
                          .attr('stroke','#ffffff')
                          .attr('fill',function(f) { return f.data.hexcol })
                          .attr('d',function(f) { return arcFunc(f) });

                  //add needle
                  viz.append('circle')
                    .attr('class','needle-center')
                    .attr('cx',padding*4.5 + w/2 + 2 - 20)
                    .attr('cy',r + m.top)
                    .attr('r',6);
                  viz.append('line')
                    .attr('class','needle')
                    .attr('x1',0).attr('x2', - i + 10).attr('y1',0).attr('y2',0)
                    .attr('transform',function() { 
                      return 'translate(' + (padding*4.5 + w/2 + 2 - 20) + ',' + (m.top + r) + ') rotate(' + 0 + ')'
                    });
                  viz.selectAll('.needle')
                    .transition().ease(d3.easeElasticOut)
                    .duration(transition)
                    .attr('transform',function() { 
                      return 'translate(' + (padding*4.5 + w/2 + 2 - 20) + ',' + (m.top + r) + ') rotate(' + (d.aqi > 500? 180: d.aqi/500 * 180) + ')'
                    });
                });
              } else {
                viz.append('text')
                  .attr('class','heavy')
                  .attr('text-anchor','start')
                  .attr('x',padding*4.5 + 5)
                  .attr('y',67)
                  .text(d.aqi)
              }
            };
          };
        };
        overlay.setMap(map);
        map.addListener('zoom_changed',function() {
          d3.selectAll('.layer').remove();
          loadOverlays(0,'slim');
        });

        function initLegends() {
          //clear existing sites legend
          d3.select('#home-sitelegend').html('');
          //reset sites legend
          let sitel = d3.select('#home-sitelegend');
          let sitet = sitel.append('div')
            .style('float','left')
            .style('width','100%')
            .style('font-weight',200)
            .style('letter-spacing','1px')
            .html('<label><input type="checkbox" id="sites-toggle"><text>HIDOE School Sites</text></label>')
          $('#sites-toggle').prop('checked',$stateParams.homemap.sites);
          d3.select('#sites-toggle')
            .on('change',function(d) {
              $stateParams.homemap.sites = !$stateParams.homemap.sites
              d3.selectAll('.layer').remove();
              loadOverlays(0,'full');
            });
          if ($stateParams.homemap.sites) {
            let icons = sitel.append('div')
              .attr('class','icon-item')
              .html('<span style="width:16px; height:16px; border-radius:16px; border:2px solid #fbb157; background-color:rgba(251,177,87,0.33); float:left;"></span><span style="margin:0px 5px; color:#525252; float:left; line-height:22px">SCHOOL LOCATION</span>');
          }
          
          //clear existing airquality legend
          d3.select('#home-astalegend').html('');
          //reset airquality legend
          let aql = d3.select('#home-astalegend');
          let aqt = aql.append('div')
            .style('float','left')
            .style('width','100%')
            .style('font-weight',200)
            .style('letter-spacing','1px')
            .html('<label><input type="checkbox" id="astas-toggle"><text>Current Air Quality</text></label>');
          $('#astas-toggle').prop('checked',$stateParams.homemap.airqual);
          d3.select('#astas-toggle')
            .on('change',function(d) {
              $stateParams.homemap.airqual = !$stateParams.homemap.airqual
              d3.selectAll('.layer').remove();
              loadOverlays(0,'full');
            });
          if ($stateParams.homemap.airqual) {
            aql.append('i')
              .style('position','absolute')
              .style('right','-10px')
              .style('font-size','14px')
              .attr('class','material-icons')
              .style('cursor','pointer')
              .text('info_outline')
              .on('click',function() {
                let url = 'https://www.airnow.gov/index.cfm?action=airnow.local_state&stateid=12'
                window.open(url,'_blank');
              })
            aql.append('div')
              .style('margin','10px 0px 5px 0px')
              .style('width','100%')
              .style('font-size','8px')
              .text('Select Pollutant');
            aql.append('div')
              .attr('id','pollutant-menu')
              .attr('class','menu')
              .style('position','relative').style('left','-2px')
              .html('<select><select>');
            d3.json('./static/scripts/components/resources/aqis.json',function(aqindex) {
              aqindex.push({
                "descr":"NO DATA",
                "hexcol":"#A9A9A9",
                "minv":"-99999",
                "maxv":"-1",
                "color":"gray",
                "range":"NO DATA"
              });
              let icons = aql.append('div').attr('class','icon-holder');
              aqindex.forEach(function(d,i) {
                icons.append('div')
                  .attr('class','icon-item')
                  .style('height',i == 2? '24px':'12px')
                  .html('<div style="float:left;" class="aqdiamond ' + aqindex[i].color + '"></div>' + 
                    '<span style="float:left;width:75px;margin-left:5px;margin-top:2px;font-size:10px;">' + aqindex[i].range + '</span><span style="float:left;margin:2px 12px 0px 0px;">|</span>' + 
                    '<span style="float:left;font-size:10px;margin-top:2px;max-width:120px;letter-spacing:1px;">' + aqindex[i].descr + '</span>')
              });
              //init select2
              let params = ['Sulfur Dioxide','Particulate Matter 2.5','Particulate Matter 10','Carbon Monoxide','Ozone'];
              let xparams = params.filter(function(d) { return d != $stateParams.homemap.airqmeasure });
              let pollmenu = d3.select('#pollutant-menu select');
              pollmenu.selectAll('option') //data
                .data(d3.map(xparams, function(d) { return d }).keys())
                .enter().append('option')
                .attr('value',function(d) { return d })
                .text(function(d) { return d });
              pollmenu._groups[0][0].innerHTML = '<option></option>' + pollmenu._groups[0][0].innerHTML;
              $('#pollutant-menu select').select2({
                placeholder : $stateParams.homemap.airqmeasure,
                dropdownAutoWidth : true,
                width: '244px',
                sortResults:sortPollutants,
              }).on('change',function(d) {
                $stateParams.homemap.airqmeasure = d.val;
                loadAqData($stateParams.homemap.airqmeasure,function(astalist) {
                  astas = astalist;
                  d3.selectAll('.layer').remove();
                  loadOverlays(3000,'full');
                });
              });
              $('#pollutant-menu').on('select2-open',function() {
                d3.selectAll('.select2-drop').style('max-width','244px').style('width','244px');
              });

              function sortPollutants(poll) {
                let reA = /[^a-zA-Z]/g;
                let reN = /[^0-9]/g;
                return poll.sort(function(a, b) {
                  let aA = a.text.replace(reA, '');
                  let bA = b.text.replace(reA, '');
                  if(aA === bA) {
                    let aN = parseInt(a.text.replace(reN, ''), 10);
                    let bN = parseInt(b.text.replace(reN, ''), 10);
                    return aN === bN ? 0 : aN > bN ? 1 : -1;
                  } else {
                    return aA > bA ? 1 : -1;
                  }
                });
              }
            });
          };
        }
      };
    });
  };

  function initSiteSelector() {
    //initialize selection list
    const sitemenu = d3.select("#home-sitemenu select");
    //populate selection list
    console.log($stateParams.siteid)
    sitemenu.selectAll('option')
      .data(d3.map(sites.filter(function(d) { return d.site_id != $stateParams.siteid }), function (d) { return d.site_nm_long }).keys())
      .enter().append('option')
      .text(function(d) { return d });
    sitemenu._groups[0][0].innerHTML = '<option></option>' + sitemenu._groups[0][0].innerHTML;
    $('#home-sitemenu select').select2({
      placeholder: $stateParams.siteid == ''?'Select Site':sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0].site_nm_long, 
      dropdownAutoWidth:true,
      width:'400px',
    }).on('change',changeSite);
    $('#home-sitemenu').on('select2-open',function() {
      d3.selectAll('.select2-drop')
        .style('max-width',$('#home-sitemenu').width() + 'px')
        .style('width',$('#home-sitemenu').width() + 'px');
    });

    function changeSite() {
      //update siteid parameter
      const site = sites.filter(function(d) { return d.site_nm_long == sitemenu.property('value') })[0];
      //reveal go button
      d3.select('#home-sitemenu').style('width','335px');
      d3.selectAll('#home-sitemenu .select2-container').style('width','335px');
      d3.select('#home-pan #gobutton')
        .style('display','inline-block')
        .on('click',function() {
          navToProfile(site.site_id);
        });
      //zoom to new loc
      map.panTo(new google.maps.LatLng(site.lat, site.lng));
      map.setZoom(15);
      if (sitelabels.indexOf(site.site_id) == -1) {
        sitelabels.push(site.site_id);
      }
    };
  };

  //HELPERS
  function getTextWidth(txt,fontname,fontsize){
    if(getTextWidth.c === undefined){
        getTextWidth.c=document.createElement('canvas');
        getTextWidth.ctx=getTextWidth.c.getContext('2d');
    }
    getTextWidth.ctx.font = fontsize + ' ' + fontname;
    return getTextWidth.ctx.measureText(txt).width * 1.1;
  }


  //NAV FUNCS
  $('#home-navdtbutton').on('click',navToDataTrends);
  $('#home-navkcbutton').on('click',navToKeepCool);

  //go button listener
  function navToProfile(id) {
    $state.go('schools',{ 
      siteid: id,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };

  //header page nav listeners
  $('#schools-nav').on('click',navToSchools);
  function navToSchools() {
    $state.go('schools',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#datatrends-nav').on('click',navToDataTrends);
  function navToDataTrends() {
    $state.go('datatrends',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#keepcool-nav').on('click',navToKeepCool);
  function navToKeepCool() {
    $state.go('keepcool',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
});







