app.directive('schoolcrprofiles', function() {
  return {
    template: [
      '<div id="schools-crprofiles">',
        '<div class="s-title">CLASSROOM TEMPERATURE PROFILES</div>',
          '<div id="cp-panel">',
            '<div id="cp-selectortxt0" style="margin:2px 0px 6px 2px; font-size:10px;visibility:hidden;">Select Profile Year:</div>',
            '<div id="cp-yearmenu" class="menu"><select></select></div>',
            '<div id="cp-selectortxt1" style="margin:17px 0px 6px 2px; font-size:10px;visibility:hidden;">Select Profile Month:</div>',
            '<div id="cp-monthmenu" class="menu"><select></select></div>',
            '<div id="cp-selectortxt2" style="margin:17px 0px 6px 2px; font-size:10px;visibility:hidden;">Monitored Room List:</div>',
            '<div id="cp-roomlist"></div>',
          '</div>',
          '<div id="cp-viz"></div>',
          '<br><p style="height:1px;width:100%;background-color:#d3d3d3;margin:10px 0px 10px 0px;float:left;"></p>',
          '<div id="ws-info"></div><div id="ws-heatmap"></div>',
          '<br><p style="height:1px;width:100%;background-color:#d3d3d3;margin:10px 0px 10px 0px;float:left;"></p>',
          '<div id="cr-info"></div><div id="cr-heatmap"></div>',
        '</div>',
      '</div>',

    ].join(''),
    scope: {},
    controller: 'SchoolCrProfilesCtrl',
  }
})
app.controller('SchoolCrProfilesCtrl', function($rootScope, $scope, $state, $stateParams, siteList, roomList) {
  
  /*substantiate reference vars*/
  const spinopts = {lines:9, length:9, width:5, radius:14, color:'#525252', speed:1.9, trail:40, className:'spinner'}
  const allmos = ['January','February','March','April','May','June','July','August','September','October','November','December']
  const allhrs = [d3.timeParse('%H:%M')('00:00'),d3.timeParse('%H:%M')('02:00'),d3.timeParse('%H:%M')('04:00'),d3.timeParse('%H:%M')('06:00'),d3.timeParse('%H:%M')('08:00'),d3.timeParse('%H:%M')('10:00'),d3.timeParse('%H:%M')('12:00'),d3.timeParse('%H:%M')('14:00'),d3.timeParse('%H:%M')('16:00'),d3.timeParse('%H:%M')('18:00'),d3.timeParse('%H:%M')('20:00'),d3.timeParse('%H:%M')('22:00')]

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      roomList.getRooms(function(roomlist) {
        if (sitelist && sitelist.length && roomlist && roomlist.length) {
          //init sequence
          sites = sitelist;
          sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
          rooms = roomlist;
          rooms.sort(function(a,b) { return d3.ascending(a.room_nm,b.room_nm) });
          if ($stateParams.siteid != '') {
            cp_activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
            cp_activesrms = rooms.filter(function(d) { return d.site_id == $stateParams.siteid });
            cp_activeyear = -999;
            cp_activemonth = -999;
            if (cp_activesrms.length > 0) {
              initClassroomProfiles('ready');
            } else {
              listNearbySiteOptions();
            }
          } else {
            initClassroomProfiles('blank');
          }
        } else {
          //timeout re-call
          var retry = function() {
            setTimeout(function() {
              loadReqs();
            },100);
          }
          retry();
        }
      });
    });
  };

  function initClassroomProfiles(status) {
    if (status == 'ready') {
      //hide selectors, spin while data loads
      d3.select('#cp-yearmenu').style('visibility','hidden');
      d3.select('#cp-monthmenu').style('visibility','hidden');
      cp_spinner = new Spinner(spinopts).spin(document.getElementById('cp-viz'));
      loadProfileData();
      loadProfileStats();

      function loadProfileData() {
        //format data request
        d3.csv('/qClassroomProfileData/?siteid=' + $stateParams.siteid + '&srmids=' + [...new Set(cp_activesrms.map(rm => rm.room_id))].join('|'), function(data) {
          if (data.length > 0) {
            cp_activedata = [];
            data.forEach (function(d) {
              //format successfully loaded data
              d.time_str = d.time_str;
              d.time = d3.timeParse('%H:%M')(d.time_str);
              d.dt = new Date(+d.year_i,+d.month_i,1,d.time.getHours(),d.time.getMinutes());
              d.temp_f = +d.temp_f;
              d.time_i = +d.time_i;
              d.month_i = +d.month_i;
              d.year_i = +d.year_i;
              cp_activedata.push(d);
            });
            cp_activedata.sort(function(a,b) { return d3.ascending(a.time,b.time) });
            //adjust stateparams to latest month available in dataset
            cp_activeyear = d3.max(cp_activedata,function(d) { return d.year_i });
            cp_activemonth = d3.max(cp_activedata.filter(function(d) {return d.year_i == cp_activeyear }),function(d) { return d.month_i });
            initYearSelection();
            initMonthSelection();
            initRoomList();
            initProfilesViz('ready');
            initWsHeatMap('ready');
          } else {
            console.log('error... no data loaded')
          }
        });
      };

      function loadProfileStats() {
        d3.csv('/qClassroomProfileStats/?siteid=' + $stateParams.siteid + '&srmids=' + [...new Set(cp_activesrms.map(rm => rm.room_id))].join('|'), function(data) {
          cp_activestats = [];
          data.forEach (function(d) {
            //format successfully loaded data
            d.temp_min = +d.temp_min;
            d.temp_avg = +d.temp_avg;
            d.temp_max = +d.temp_max;
            d.temp_std = +d.temp_std;
            d.temp_o85 = +d.temp_o85/d.temp_n;
            d.temp_o90 = +d.temp_o90/d.temp_n;
            d.month_i = +d.month_i;
            d.year_i = +d.year_i;
            cp_activestats.push(d);
          });
          initWsStats();
        });
      };
      
    } else {
      console.log('no data available');
    }
  };

  function initYearSelection() {
    //reset, reveal selection elements
    $('#cp-yearmenu select').select2('destroy');
    $('#cp-yearmenu').html('<select></select>');
    d3.select('#cp-yearmenu').style('visibility','visible');
    d3.select('#cp-selectortxt0').style('visibility','visible');
    //populate selection list
    const yearmenu = d3.select('#cp-yearmenu select');
    yearmenu.selectAll('option')
      .data(d3.map(getSingleColumn(cp_activedata.filter(function(d) { return d.year_i !== cp_activeyear }),'year_i').filter(uniqueValsOnly),function(d) { return d }).keys())
      .enter().append('option')
      .attr('value',function(d) { return d })
      .text(function(d) { return d });
    yearmenu._groups[0][0].innerHTML = '<option></option>' + yearmenu._groups[0][0].innerHTML;
    //init select2 library
    $('#cp-yearmenu select').select2({
      dropdownAutoWidth:true,
      placeholder:cp_activeyear,
      width:'100%',
    }).on('change',function(d) {
      cp_activeyear = d.val * 1;
      d3.select('#cp-yearmenu').style('visibility','hidden');
      $('#cp-yearmenu select').select2('destroy');
      $('#cp-yearmenu').html('<select></select>');
      initYearSelection();
      initMonthSelection();
      initRoomList();
      drawProfileLines('redraw');
      initWsHeatMap('redraw');
      initWsStats();
      if (typeof cp_activeroom != 'undefined' && cp_activeroom != '') {
        initCrHeatMap('redraw');
        initCrStats();
      }
    });
  };

  function initMonthSelection() {
    //reset, reveal selection elements
    $('#cp-monthmenu select').select2('destroy');
    $('#cp-monthmenu').html('<select></select>');
    d3.select('#cp-monthmenu').style('visibility','visible');
    d3.select('#cp-selectortxt1').style('visibility','visible');
    //populate selection list
    const mnthmenu = d3.select('#cp-monthmenu select');
    let ymonths = [];
    allmos.forEach(function(d) {
      if (cp_activedata.filter(function(e) { return e.year_i == cp_activeyear && e.month_i == allmos.indexOf(d) }).length > 0) {
        ymonths.push(d);
      }
    });
    mnthmenu.selectAll('option')
      .data(d3.map(ymonths.filter(function(d) { return d !== allmos[cp_activemonth] }),function(d) { return d }).keys())
      .enter().append('option')
      .attr('value',function(d) { return d })
      .text(function(d) { return d });
    mnthmenu._groups[0][0].innerHTML = '<option></option>' + mnthmenu._groups[0][0].innerHTML;
    //init select2 library
    $('#cp-monthmenu select').select2({
      dropdownAutoWidth:true,
      placeholder:allmos[cp_activemonth],
      width:'100%',
    }).on('change',function(d) {
      cp_activemonth = allmos.indexOf(d.val);
      d3.select('#cp-monthmenu').style('visibility','hidden');
      $('#cp-monthmenu select').select2('destroy');
      $('#cp-monthmenu').html('<select></select>');
      initMonthSelection();
      initRoomList();
      drawProfileLines('redraw');
      initWsStats();
      if (typeof cp_activeroom != 'undefined' && cp_activeroom != '') {
        initCrHeatMap('redraw');
        initCrStats();
      }
    });
  };

  function initProfilesViz(phase) {
    //init margin/dimensions vars
    cp_svgm = {top: 0, right: 25, bottom: 15, left: 40};
    cp_svgw = $('#cp-viz').width();
    cp_svgh = 390 - cp_svgm.top - cp_svgm.bottom;
    //define ymax/ymin
    if (phase !== 'blank') {
      cp_ymin = Math.floor(d3.min(cp_activedata, function(d) { return d.temp_f }) / 2) * 2;
      cp_ymax = Math.ceil(d3.max(cp_activedata, function(d) { return d.temp_f }) / 2) * 2;
    } else {
      cp_ymin = 60;
      cp_ymax = 90;
    }
    //define scales, axes, grid lines, voronois
    cp_xScale = d3.scaleTime();
    cp_xScale.domain([d3.timeParse('%H:%M')('00:00'),d3.timeParse('%H:%M')('24:00')]).range([cp_svgm.left, cp_svgw - cp_svgm.right]);
    cp_xAxis = d3.axisBottom();
    cp_xAxis.scale(cp_xScale).tickSize(0).tickValues(allhrs).tickFormat(d3.timeFormat('%H:00'));
    cp_yScale = d3.scaleLinear();
    cp_yScale.domain([cp_ymin,cp_ymax]).range([cp_svgh - cp_svgm.top,cp_svgm.bottom]);
    cp_yAxis = d3.axisLeft();
    cp_yAxis.scale(cp_yScale).tickSize(0).tickFormat(function(a) {return a + '' });
    cp_yGrid = d3.axisLeft();
    cp_yGrid.scale(cp_yScale).tickSize(-cp_svgw + cp_svgm.left + cp_svgm.right).tickFormat('');
    //define line function
    cp_lineFunction = d3.line()
      .curve(d3.curveCardinal)
      .defined(function(d) { return typeof d.temp_f !== 'undefined' })
      .x(function(d) { return cp_xScale(d.time) })
      .y(function(d) { return cp_yScale(d.temp_f) });
    //define voronoi function
    cp_voronoiFunction = d3.voronoi()
      .x(function(d) { return cp_xScale(d.time) })
      .y(function(d) { return cp_yScale(d.temp_f) })
      .extent([[cp_svgm.left,cp_svgm.top], [cp_svgw - cp_svgm.right,cp_svgh + cp_svgm.top]])
    //define key function
    cp_key = function(d,i) { return cp_activesite.site_nm_shrt.replace(/\W+/g,'') + cp_activemonth + d.key }
    //remove existing svg
    d3.selectAll('#cp-viz svg').remove();
    //append new svg
    cp_chart = d3.select('#cp-viz').append('svg')
      .attr('width', cp_svgw  + cp_svgm.left + cp_svgm.right)
      .attr('height', cp_svgh + cp_svgm.bottom + cp_svgm.top)
    //draw school hour gray box
    cp_chart.append('g') 
      .attr('class','school-hours')
      .append('rect')
      .attr('x',cp_xScale(d3.timeParse('%H:%M')('08:00')))
      .attr('y',cp_svgm.bottom)
      .attr('width',(cp_svgw - cp_svgm.right - cp_svgm.left) * 6 / 24)
      .attr('height',cp_svgh - cp_svgm.top - cp_svgm.bottom)
    //draw axes
    cp_chart.append('g')
      .attr('class','cp x axis')
      .attr('transform','translate(0,' + (cp_svgh - cp_svgm.top) + ')')
      .attr('width',cp_svgw + 'px')
      .call(cp_xAxis)
      .selectAll('.tick text')
      .attr('y','5')
      .attr('dx','10')
    cp_chart.append('g')
      .attr('class','cp y axis')
      .attr('height',cp_svgh + 'px')
      .call(cp_yAxis)
      .selectAll('.tick text, .tick line, .domain')
      .attr('transform','translate(' + cp_svgm.left + ',0)')
    //draw gridlines
    cp_chart.append('g')
      .attr('class','cp y grid')
      .attr('height',cp_svgh + 'px')
      .call(cp_yGrid)
      .selectAll('.tick text, .tick line, .domain')
      .attr('transform','translate(' + cp_svgm.left + ',0)')
    //create data elements
    cp_linesvg = cp_chart.append('g')
      .attr('class','lines')
      .attr('transform','translate(0,' + cp_svgm.top + ')')
    cp_voronoiGroup = cp_chart.append('g') 
      .attr('class','voronoi')
    cp_focus = cp_chart.append('g')
      .attr('class','focus')
      .attr('transform','translate(-100,-100)')
    //line legend
    cp_chart.append('rect') //room line visual
      .attr('x',0).attr('y',0)
      .attr('transform','translate(' + (cp_svgw - cp_svgm.right - 20) + ',' + (cp_svgm.top + 17) + ')')
      .attr('width',20)
      .attr('height',3)
      .attr('fill','#637e8d');
    cp_chart.append('text') //room line text
      .attr('x',0).attr('y',0)
      .attr('transform','translate(' + (cp_svgw - cp_svgm.right - 104) + ',' + (cp_svgm.top + 21) + ')')
      .style('font-size','10px')
      .style('display','block')
      .style('width','100px')
      .style('text-align','right')
      .text('Avg Indoor Profile');
    for (i = 0; i < 4; i++) {
      cp_chart.append('rect') //outdoor line visual
        .attr('x',0 + i * 5.5).attr('y',0)
        .attr('transform','translate(' + (cp_svgw - cp_svgm.right - 20) + ',' + (cp_svgm.top + 30) + ')')
        .attr('width',4)
        .attr('height',3)
        .attr('fill','#637e8d');
    }
    cp_chart.append('text') //outdoor line text
      .attr('x',0).attr('y',0)
      .attr('transform','translate(' + (cp_svgw - cp_svgm.right - 112) + ',' + (cp_svgm.top + 34) + ')')
      .style('font-size','10px')
      .style('width','100px')
      .style('text-align','right')
      .text('Avg Outdoor Profile');
    //add annotations
    cp_chart.append('text')
      .attr('x',0)
      .attr('y',0)
      .attr('transform','translate(' + (cp_svgm.left + 4) + ',' + (cp_svgm.bottom + 4) + ')')
      .text('°F')
      .style('font-size','9px');
    cp_chart.append('text')
      .attr('class','main-axis-label')
      .attr('x',cp_xScale(d3.timeParse('%H:%M')('11:00')))
      .attr('y',cp_svgh + cp_svgm.top - 4)
      .text('A school day typically runs from 8am to 2pm')
      .style('text-anchor','middle')
      .style('font-size','9px')
      .style('font-weight',300);
    //stop spinner
    cp_spinner.stop()
    //draw, redraw lines
    if (phase !== 'blank') {
      drawProfileLines(phase);
    }
  }

  function initRoomList() {
    //reveal selector text
    d3.select('#cp-selectortxt2').style('visibility','visible');
    //remove existing room list buttons
    d3.selectAll('#cp-roomlist div').remove();
    //generate new room list buttons
    let rlist = d3.select('#cp-roomlist').selectAll('div')
      .enter()
      .data(d3.map(cp_activesrms,function(d) { return d.room_nm })
        .keys()
        .sort(function(a,b) { return d3.ascending(a,b) }));
    rlist.enter()
      .append('div')
      .attr('class', function(d) { return ('roomlabel r' + d.replace(/\W+/g, '')) })
      .text(function(d) { return d })
      .on('click',function (d) {
        if (d3.selectAll('.roomlabel.r' + d.replace(/\W+/g, '')).classed('selectable')) {
          selectRoomProfile(d);
        }
      });
    //adjust width of buttons
    $('#cp-roomlist').css('columnCount',Math.ceil(cp_activesrms.length/14));
    if (Math.ceil(cp_activesrms.length/12) == 1) {
      d3.selectAll('.roomlabel').style('width','282px')
    } else if (Math.ceil(cp_activesrms.length/12) == 2) {
      d3.selectAll('.roomlabel').style('width','131px')
    } else if (Math.ceil(cp_activesrms.length/12) == 3) {
      d3.selectAll('.roomlabel').style('width','89px')
    } else if (Math.ceil(cp_activesrms.length/12) == 4) {
      d3.selectAll('.roomlabel').style('width','65px')
    }
    //make rooms with data selectable
    let msrms = [];
    cp_activesrms.forEach(function(d) {
      if (cp_activedata.filter(function(e) { return e.room_id == d.room_id && e.year_i == cp_activeyear && e.month_i == cp_activemonth }).length > 0) {
        msrms.push(d);
      }
    });
    d3.selectAll('.roomlabel').classed('selectable',false);
    d3.select('#cp-roomlist').selectAll('div')
      .each(function(d) {
        let _room = d3.select(this);
        msrms.forEach(function(key) {
          if (d == key.room_nm) {
            _room.classed('selectable',true);
          }
        });
      });
    //highlight activeroom button
    if (typeof cp_activeroom !== 'undefined' && cp_activeroom !== '') {
      d3.selectAll('.roomlabel.r' + cp_activeroom.replace(/\W+/g, ''))
        .classed('room-selected',true);
    }
  };

  function drawProfileLines(phase) {
    //filter active dataset to selected year, month
    let _data = cp_activedata.filter(function(d) { return d.year_i == cp_activeyear && d.month_i == cp_activemonth });
    if (_data.length > 0) {
      let _monthrooms = d3.map(_data, function(d) { return d.room_id }).keys();
      //nest filtered dataset into final holder
      cp_nesteddata = d3.nest()
        .key(function(d) { return d.room_id })
        .entries(_data);

      //join lines
      let lines = cp_linesvg.selectAll('path')
        .data(cp_nesteddata,cp_key);

      //enter new lines
      let newlines = lines.enter()
        .append('path')
        .style('opacity','0.001')
        .attr('class', function(d) { return 'main line ' + cp_activemonth + ' ' + cp_activeyear + ' r' + d.key.replace(/\W+/g,'') + ' ' + cp_activesite.site_nm_shrt.replace(/\W+/g,'') })
        .attr('d', function(d) { return cp_lineFunction(d.values) })
        .transition().duration( function(d) {return phase == 'redraw'? 0: (cp_activesite.site_id == 252? 3500: 1500)})
        .style('opacity','0.9');
      //merge lines
      lines.merge(newlines)
        .transition().duration(2000)
        .attr('class', function(d) {
          let currnt = 'main line ' + cp_activemonth + ' ' + cp_activeyear + ' r' + d.key + ' ' + cp_activesite.site_nm_shrt.replace(/\W+/g,'');
          let button = d3.select('.roomlabel.r' + d.key);
          if (d.key != 'Outdoor') {
            if (button.classed('room-selected')) {
              currnt = currnt + ' line-selected';
            }
          }
          return currnt
        })
        .attr('d', function(d) { return cp_lineFunction(d.values) })
        .style('opacity', null)
        .on('end',function(d) {
          //clear empty lines
          d3.selectAll('.main.line')
            .each(function(d) {  
              let _line = d3.select(this)
              let _room = _line.attr('class').split(' ');
              let linemnth = Math.round(_room[2])
              let lineyear = Math.round(_room[3])
              if (linemnth !== cp_activemonth || lineyear !== cp_activeyear) {
                _line   
                  .style('opacity','0.9')
                  .transition().duration(250)
                  .style('opacity','0.01')
                  .remove();
              }
            });
          });
      //reset voronoi
      cp_voronoiGroup.selectAll('path').remove();
      var voronois = cp_voronoiGroup.selectAll('path')
        .data(cp_voronoiFunction.polygons(d3.merge(cp_nesteddata.map(function(d) { return d.values }))));
      //enter voroinoi
      voronois.enter()
        .append('path')
        .attr('d',function(d) { return d? 'M' + d.join('L') + 'Z':null })
        .on('mouseover',function(d) { mouseoverCpLine(d) })
        .on('mouseout',function(d) { mouseoutCpLine(d) })
        .on('click',function(d) {
            if (d.data.room_id !== 'Outdoor') {
              selectRoomProfile(d.data.room_id);
            }
          });
      cp_focus.append('line')
        .attr('class','focus-line');
      cp_focus.append('text')
        .attr('class','focus-label')
        .attr('x',5).attr('y',-3)
        .style('z-index',99999);
      cp_focus.append('text')
        .attr('class','focus-time')
        .attr('x',-10);
    } else {
      cp_linesvg.selectAll('path').remove();
      cp_voronoiGroup.selectAll('path').remove();
    }
  };

  function mouseoverCpLine(d) {
    //highlight target line
    d3.selectAll('.line').style('opacity',0.1);
    d3.selectAll('.line.r' + d.data.room_id)
      .classed('line-hover',true)
      .style('opacity',0.9)
    //hide ticks
    d3.select('.cp.x.axis').selectAll('.tick').attr('display','none')
    //adjust focus
    cp_focus.attr('transform','translate(' + cp_xScale(d.data.time) + ',' + cp_yScale(d.data.temp_f) + ')')
    cp_focus.select('.focus-label')
      .style('font-size','9px')
      .text(d.data.room_id + ': ' + d3.format('.2F')(d.data.temp_f) + 'ºF');
    cp_focus.select('.focus-line')
      .attr('y1',cp_svgm.top)
      .attr('y2',cp_svgh - cp_svgm.top - cp_yScale(d.data.temp_f));
    cp_focus.select('.focus-time')
      .attr('y',cp_svgh - cp_yScale(d.data.temp_f) + 10)
      .style('font-size','9px')
      .text(d.data.time_str)
  };

  function mouseoutCpLine(d) {
    //unhighlight lines
    d3.selectAll('.line').style('opacity',null);
    d3.selectAll('.line.r' + d.data.room_id)
      .classed('line-hover',false)
      .style('opacity',null)
    //unhide ticks
    d3.select('.cp.x.axis').selectAll('.tick').attr('display',null)
    //move focus off canvas
    cp_focus.attr('transform','translate(-100, -100)')
  };

  function selectRoomProfile(d) {
    let wasActive = d3.selectAll('.roomlabel.r' + d).classed('room-selected');
    if (wasActive) {
      //remove existing selection
      cp_activeroom = '';
      d3.selectAll('path').classed('line-selected',false);
      d3.selectAll('.roomlabel').classed('room-selected',false);
      //remove cr heatmap
      d3.selectAll('#cr-heatmap svg').remove();
      d3.selectAll('#cr-heatmap div').remove();
      //remove cr info
      d3.selectAll('#cr-info div').remove();
      d3.selectAll('#cr-info table').remove();
    } else {
      //remove existing selection
      d3.selectAll('#cp-viz path').classed('line-selected',false);
      d3.selectAll('#cp-roomlist .roomlabel').classed('room-selected',false);
      //replace w/ new selection
      cp_activeroom = d;
      d3.selectAll('#cp-viz .line.r' + d + '.' + cp_activesite.site_nm_shrt.replace(/\W+/g,'')).classed('line-selected',true);
      d3.selectAll('#cp-roomlist .roomlabel.r' + d).classed('room-selected',true);
      //re-init cr heatmap
      initCrHeatMap('redraw');
      initCrStats();
    }
  };

  function initWsHeatMap(phase) {
    //init margin/dimensions vars
    const wshm_svgm = {top: 20, right: 25, bottom: 0, left: 40};
    const wshm_svgw = $('#ws-heatmap').width();
    const wshm_svgh = 150 - wshm_svgm.top - wshm_svgm.bottom;
    //init formatting/layout vars
    const grid = {
      mnths : allmos,
      hours : ['0:00','0:30','1:00','1:30','2:00','2:30','3:00','3:30','4:00','4:30','5:00','5:30','6:00','6:30','7:00','7:30','8:00','8:30','9:00','9:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30']};
    const colors = ['#d73027','#fc8d59','#fee090','#e0f3f8','#91bfdb','#4575b4'].reverse();
    const cScale = d3.scaleThreshold()
      .domain([70, 75, 80, 85, 90, 95])
      .range(colors);
    const grid_s = (wshm_svgw-wshm_svgm.left-wshm_svgm.right)/48;
    const grid_h = wshm_svgh/12;
    //remove existing hm + legend
    d3.selectAll('#ws-heatmap svg').remove();
    //append new svg
    let ws_map = d3.select('#ws-heatmap').append('svg')
      .attr('width', wshm_svgw + wshm_svgm.left + wshm_svgm.right)
      .attr('height', wshm_svgh + wshm_svgm.bottom + wshm_svgm.top);
    //draw x-axis
    ws_xScale = d3.scaleTime();
    ws_xScale.domain([d3.timeParse('%H:%M')('00:00'),d3.timeParse('%H:%M')('24:00')]).range([wshm_svgm.left, wshm_svgw - wshm_svgm.right]);
    ws_xAxis = d3.axisBottom();
    ws_xAxis.scale(ws_xScale).tickSize(0).tickValues(allhrs).tickFormat(d3.timeFormat('%H:00'));
    ws_map.append('g')
      .attr('class','cp x axis')
      .attr('transform','translate(0,' + wshm_svgh + ')')
      .attr('width', wshm_svgw + 'px')
      .call(ws_xAxis)
      .selectAll('.tick text')
      .attr('y',5)
      .attr('dx',10);
    //draw y-labels
    let mLabelGroup = ws_map.append('g')
      .attr('class','month-labels');
    let mLabels = mLabelGroup.selectAll('text.monthlab')
      .data(allmos)
      .enter().append('text')
      .attr('x',3)
      .attr('y',function (d,i) { return (i * grid_h + 3) })
      .attr('transform','translate(' + (wshm_svgm.left - 6) + ',' + (grid_h / 2) + ')')
      .attr('class','monthlab ws')
      .text(function (d) { return d.slice(0,3) })
      .style('text-anchor','end')
      .style('font-size','9px');
    //format data,fill missing data with -999 utci values
    let wsdata = [];
    grid.mnths.forEach(function(mo) { 
      grid.hours.forEach(function(hr) {
        let _data = cp_activedata.filter(function(d) { return d.room_id == 'Outdoor' && d.year_i == cp_activeyear && allmos[d.month_i] == mo && d.time_str == hr })
        if (_data.length == 0) {
          let row = {
            time_str: hr,
            time_i: grid.hours.indexOf(hr),
            temp_f: -999,
            month_i: grid.mnths.indexOf(mo),
            year_i: cp_activeyear };
          wsdata.push(row);
        } else {
          let row = {
            time_str: hr,
            time_i: grid.hours.indexOf(hr),
            temp_f: _data[0].temp_f,
            month_i: grid.mnths.indexOf(mo),
            year_i: cp_activeyear };
          wsdata.push(row);
        }
      });
    });
    //append tiles
    let ws_tiles = ws_map.append('g')
      .attr('class','tiles')
      .attr('transform','translate(' + wshm_svgm.left + ',0)');
    //format tiles
    let ws_cards = ws_tiles.selectAll('rect.ws')
        .data(wsdata,function(d) { return allmos.indexOf(d.month_i) + ' ' + d.time })
      .enter().append('rect')
        .attr('class', function(d) { return 'tile ws ' + allmos[d.month_i] + ' h' + d.time_str })
        .classed('school', function(d) { return (d.time_i <= 28 & d.time_i > 16 & d.month_i != 6 & d.month_i != 7) })
        .attr('x', function(d) { return (d.time_i * grid_s) })
        .attr('y', function(d) { return (d.month_i * grid_h) })
        .attr('rx', 4)
        .attr('ry', 4)
        .attr('width', grid_s - 0.3)
        .attr('height', grid_h)
        .style('fill', 'rgba(211, 211, 211, 0.15)')
        .style('fill-opacity', 0.8)
        .transition().delay(0).duration(1000)
        .style('fill', function(d) { return (d.temp_f == -999 ? 'rgba(211, 211, 211, 0.15)' : cScale(d.temp_f)) });
    //append legend
    let lwidth = 55;
    let legendGroup = d3.select('#ws-heatmap').append('svg')
      .attr('width',wshm_svgw + wshm_svgm.left + wshm_svgm.right)
      .attr('height',18)
      .append('g')
      .attr('class','legends')
      .attr('transform','translate(' + wshm_svgm.left + ',1)');
    let legend = legendGroup.selectAll('.legend')
      .data(cScale.domain())
      .enter().append('g')
      .attr('class','legend');
    legend.append('rect')
      .attr('x',function(d, i) { return lwidth * i })
      .attr('y',0)
      .attr('width',lwidth)
      .attr('height',15)
      .style('fill',function(d,i) { return colors[i] })
      .style('opacity',0.8);
    legend.append('text')
      .attr('x',function(d, i) { return lwidth * i + (lwidth / 2) })
      .text(function(d) { return d==70 ? '<' + d + ' \xB0F' : (d-5) + ' \xB0F +' })
      .attr('dy','1.10em')
      .style('text-anchor','middle')
      .style('fill',function(d,i) { return i == 2 || i == 3? '#637e8d': '#ffffff' });
  };

  function initCrHeatMap(phase) {
    //init margin/dimensions vars
    const crhm_svgm = {top: 20, right: 25, bottom: 0, left: 40};
    const crhm_svgw = $('#ws-heatmap').width();
    const crhm_svgh = 150 - crhm_svgm.top - crhm_svgm.bottom;
    //init formatting/layout vars
    const grid = {
      mnths : allmos,
      hours : ['0:00','0:30','1:00','1:30','2:00','2:30','3:00','3:30','4:00','4:30','5:00','5:30','6:00','6:30','7:00','7:30','8:00','8:30','9:00','9:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30']};
    const colors = ['#d73027','#fc8d59','#fee090','#e0f3f8','#91bfdb','#4575b4'].reverse();
    const cScale = d3.scaleThreshold()
      .domain([70, 75, 80, 85, 90, 95])
      .range(colors);
    const grid_s = (crhm_svgw-crhm_svgm.left-crhm_svgm.right)/48;
    const grid_h = crhm_svgh/12;
    //remove existing hm + legend
    d3.selectAll('#cr-heatmap svg').remove();
    //append new svg
    let cr_map = d3.select('#cr-heatmap').append('svg')
      .attr('width', crhm_svgw + crhm_svgm.left + crhm_svgm.right)
      .attr('height', crhm_svgh + crhm_svgm.bottom + crhm_svgm.top);
    //draw x-axis
    cr_xScale = d3.scaleTime();
    cr_xScale.domain([d3.timeParse('%H:%M')('00:00'),d3.timeParse('%H:%M')('24:00')]).range([crhm_svgm.left, crhm_svgw - crhm_svgm.right]);
    cr_xAxis = d3.axisBottom();
    cr_xAxis.scale(cr_xScale).tickSize(0).tickValues(allhrs).tickFormat(d3.timeFormat('%H:00'));
    cr_map.append('g')
      .attr('class','cp x axis')
      .attr('transform','translate(0,' + crhm_svgh + ')')
      .attr('width', crhm_svgw + 'px')
      .call(cr_xAxis)
      .selectAll('.tick text')
      .attr('y',5)
      .attr('dx',10);
    //draw y-labels
    let mLabelGroup = cr_map.append('g')
      .attr('class','month-labels');
    let mLabels = mLabelGroup.selectAll('text.monthlab')
      .data(allmos)
      .enter().append('text')
      .attr('x',3)
      .attr('y',function (d,i) { return (i * grid_h + 3) })
      .attr('transform','translate(' + (crhm_svgm.left - 6) + ',' + (grid_h / 2) + ')')
      .attr('class','monthlab ws')
      .text(function (d) { return d.slice(0,3) })
      .style('text-anchor','end')
      .style('font-size','9px');
    //format data,fill missing data with -999 utci values
    let crdata = [];
    grid.mnths.forEach(function(mo) { 
      grid.hours.forEach(function(hr) {
        let _data = cp_activedata.filter(function(d) { return d.room_id == cp_activeroom && d.year_i == cp_activeyear && allmos[d.month_i] == mo && d.time_str == hr })
        if (_data.length == 0) {
          let row = {
            time_str: hr,
            time_i: grid.hours.indexOf(hr),
            temp_f: -999,
            month_i: grid.mnths.indexOf(mo),
            year_i: cp_activeyear };
          crdata.push(row);
        } else {
          let row = {
            time_str: hr,
            time_i: grid.hours.indexOf(hr),
            temp_f: _data[0].temp_f,
            month_i: grid.mnths.indexOf(mo),
            year_i: cp_activeyear };
          crdata.push(row);
        }
      });
    });
    //append tiles
    let cr_tiles = cr_map.append('g')
      .attr('class','tiles')
      .attr('transform','translate(' + crhm_svgm.left + ',0)');
    //format tiles
    let cr_cards = cr_tiles.selectAll('rect.cr')
        .data(crdata,function(d) { return allmos.indexOf(d.month_i) + ' ' + d.time })
      .enter().append('rect')
        .attr('class', function(d) { return 'tile cr ' + allmos[d.month_i] + ' h' + d.time_str })
        .classed('school', function(d) { return (d.time_i <= 28 & d.time_i > 16 & d.month_i != 6 & d.month_i != 7) })
        .attr('x', function(d) { return (d.time_i * grid_s) })
        .attr('y', function(d) { return (d.month_i * grid_h) })
        .attr('rx', 4)
        .attr('ry', 4)
        .attr('width', grid_s - 0.3)
        .attr('height', grid_h)
        .style('fill', 'rgba(211, 211, 211, 0.15)')
        .style('fill-opacity', 0.8)
        .transition().delay(0).duration(1000)
        .style('fill', function(d) { return (d.temp_f == -999 ? 'rgba(211, 211, 211, 0.15)' : cScale(d.temp_f)) });
    //append legend
    let lwidth = 55;
    let legendGroup = d3.select('#cr-heatmap').append('svg')
      .attr('width',crhm_svgw + crhm_svgm.left + crhm_svgm.right)
      .attr('height',18)
      .append('g')
      .attr('class','legends')
      .attr('transform','translate(' + crhm_svgm.left + ',1)');
    let legend = legendGroup.selectAll('.legend')
      .data(cScale.domain())
      .enter().append('g')
      .attr('class','legend');
    legend.append('rect')
      .attr('x',function(d, i) { return lwidth * i })
      .attr('y',0)
      .attr('width',lwidth)
      .attr('height',15)
      .style('fill',function(d,i) { return colors[i] })
      .style('opacity',0.8);
    legend.append('text')
      .attr('x',function(d, i) { return lwidth * i + (lwidth / 2) })
      .text(function(d) { return d==70 ? '<' + d + ' \xB0F' : (d-5) + ' \xB0F +' })
      .attr('dy','1.10em')
      .style('text-anchor','middle')
      .style('fill',function(d,i) { return i == 2 || i == 3? '#637e8d': '#ffffff' });
  };

  function initWsStats() {
    if (cp_activeyear !== -999 && cp_activemonth !== -999) {
      let stats = cp_activestats.filter(function(d) { return d.year_i == cp_activeyear && d.month_i == cp_activemonth && d.room_id == 'Outdoor' })[0];
      let ws_info = d3.select('#ws-info').html('');
      if (typeof stats != 'undefined') {
        let title = ws_info.append('div')
        title.append('span')
          .style('font-size','15px')
          .style('font-weight',400)
          .text('Outdoor Profile');
        title.append('span')
          .style('font-size','12px')
          .style('font-weight',200)
          .style('font-style','italic')
          .style('margin','0px 0px 0px 5px')
          .text('measured w/in 10km of selected site');
        ws_info.append('div')
          .style('font-size','10px')
          .style('font-weight',200)
          .text(allmos[cp_activemonth] + ' ' + cp_activeyear + ' (8am to 2pm, M-F only)');
        let ws_stats = ws_info.append('div')
          .style('margin','15px 0px 0px 0px');
        let ws_mintemp = ws_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
        ws_mintemp.append('span')
          .style('float','left')
          .style('width','70px')
          .style('font-weight',700)
          .style('font-size','13px')
          .text('MIN');
        ws_mintemp.append('span')
          .style('float','left')
          .style('width','calc(100% - 70px)')
          .style('font-weight',200)
          .style('font-size','12px')
          .text(d3.format('.1f')(stats.temp_min) + ' °F');
        let ws_avgtemp = ws_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
        ws_avgtemp.append('span')
          .style('float','left')
          .style('width','70px')
          .style('font-weight',700)
          .style('font-size','13px')
          .text('AVG');
        ws_avgtemp.append('span')
          .style('float','left')
          .style('width','calc(100% - 70px)')
          .style('font-weight',200)
          .style('font-size','12px')
          .text(d3.format('.1f')(stats.temp_avg) + ' °F');
        let ws_maxtemp = ws_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
        ws_maxtemp.append('span')
          .style('float','left')
          .style('width','70px')
          .style('font-weight',700)
          .style('font-size','13px')
          .text('MAX');
        ws_maxtemp.append('span')
          .style('float','left')
          .style('width','calc(100% - 70px)')
          .style('font-weight',200)
          .style('font-size','12px')
          .text(d3.format('.1f')(stats.temp_max) + ' °F');
        let ws_stdtemp = ws_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
        ws_stdtemp.append('span')
          .style('float','left')
          .style('width','70px')
          .style('font-weight',700)
          .style('font-size','13px')
          .text('STD');
        ws_stdtemp.append('span')
          .style('float','left')
          .style('width','calc(100% - 70px)')
          .style('font-weight',200)
          .style('font-size','12px')
          .text(d3.format('.2f')(stats.temp_std) + ' °F');
        let ws_pcto85 = ws_stats.append('div').style('height','15px').style('margin','13px 0px 0px 0px')
          .style('font-weight',200)
          .style('font-size','12px')
          .text(d3.format('.1f')(stats.temp_o85*100.0) + '% of School Hours reached 85°F');
        let ws_pcto90 = ws_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px')
          .style('font-weight',200)
          .style('font-size','12px')
          .text(d3.format('.1f')(stats.temp_o90*100.0) + '% of School Hours reached 90°F');
      }
    } else {
      setTimeout(function() { initWsStats() },100);
    }
  };

  function initCrStats() {
    let stats = cp_activestats.filter(function(d) { return d.year_i == cp_activeyear && d.month_i == cp_activemonth && d.room_id == cp_activeroom })[0];
    let cr_info = d3.select('#cr-info').html('');
    let title = cr_info.append('div')
    title.append('span')
      .style('font-size','15px')
      .style('font-weight',400)
      .text('Indoor Profile');
    title.append('span')
      .style('font-size','15px')
      .style('font-weight',400)
      .style('margin','0px 0px 0px 5px')
      .text( '[' + cp_activeroom + ']');
    cr_info.append('div')
      .style('font-size','10px')
      .style('font-weight',200)
      .text(allmos[cp_activemonth] + ' ' + cp_activeyear + ' (8am to 2pm, M-F only)');
    let cr_stats = cr_info.append('div')
      .style('margin','15px 0px 0px 0px');
    let cr_mintemp = cr_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
    cr_mintemp.append('span')
      .style('float','left')
      .style('width','70px')
      .style('font-weight',700)
      .style('font-size','13px')
      .text('MIN');
    cr_mintemp.append('span')
      .style('float','left')
      .style('width','calc(100% - 70px)')
      .style('font-weight',200)
      .style('font-size','12px')
      .text(d3.format('.1f')(stats.temp_min) + ' °F');
    let cr_avgtemp = cr_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
    cr_avgtemp.append('span')
      .style('float','left')
      .style('width','70px')
      .style('font-weight',700)
      .style('font-size','13px')
      .text('AVG');
    cr_avgtemp.append('span')
      .style('float','left')
      .style('width','calc(100% - 70px)')
      .style('font-weight',200)
      .style('font-size','12px')
      .text(d3.format('.1f')(stats.temp_avg) + ' °F');
    let cr_maxtemp = cr_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
    cr_maxtemp.append('span')
      .style('float','left')
      .style('width','70px')
      .style('font-weight',700)
      .style('font-size','13px')
      .text('MAX');
    cr_maxtemp.append('span')
      .style('float','left')
      .style('width','calc(100% - 70px)')
      .style('font-weight',200)
      .style('font-size','12px')
      .text(d3.format('.1f')(stats.temp_max) + ' °F');
    let cr_stdtemp = cr_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px');
    cr_stdtemp.append('span')
      .style('float','left')
      .style('width','70px')
      .style('font-weight',700)
      .style('font-size','13px')
      .text('STD');
    cr_stdtemp.append('span')
      .style('float','left')
      .style('width','calc(100% - 70px)')
      .style('font-weight',200)
      .style('font-size','12px')
      .text(d3.format('.2f')(stats.temp_std) + ' °F');
    let cr_pcto85 = cr_stats.append('div').style('height','15px').style('margin','13px 0px 0px 0px')
      .style('font-weight',200)
      .style('font-size','12px')
      .text(d3.format('.1f')(stats.temp_o85*100.0) + '% of School Hours reached 85°F');
    let cr_pcto90 = cr_stats.append('div').style('height','15px').style('margin','3px 0px 0px 0px')
      .style('font-weight',200)
      .style('font-size','12px')
      .text(d3.format('.1f')(stats.temp_o90*100.0) + '% of School Hours reached 90°F');
  };

  function listNearbySiteOptions() {
    //omit activesite
    let nsites = []
    sites.forEach(function(d) {
      if (d.site_id !== cp_activesite.site_id) {
        nsites.push({
          'id':d.site_id,
          'nm':d.site_nm_long,
          'dist': distance(cp_activesite.lat,cp_activesite.lng,d.lat,d.lng,'M'),
          'rooms': rooms.filter(function(e) {return e.site_id == d.site_id }).length,
        });
      }
    });
    //clear sites with 0 rooms measured
    nsites = $.grep(nsites, function(e) { return e.rooms > 0; });
    //sort remaining sites by distance
    nsites.sort(function(a,b) { return d3.ascending(a.dist, b.dist) });
    //draw options
    d3.selectAll('#cp-panel,#cp-viz,#cr-heatmap,#cr-info,#ws-heatmap,#ws-info,#schools-crprofiles br,#schools-crprofiles p').remove();
    d3.select('#schools-crprofiles').append('div').attr('id','cp-content');
    d3.select('#cp-content').append('div')
      .html('No classroom measurements available for ' + cp_activesite.site_nm_long + '.')
      .style('font-weight','400')
      .style('font-size','14px')
      .style('margin','10px 0px 0px 5px');
    d3.select('#cp-content').append('div')
      .html('<i>Click below to view nearby sites with classroom temperature profiles:</i>')
      .style('color','#525252')
      .style('font-weight','200')
      .style('font-size','12px')
      .style('margin','10px 0px 0px 5px');

    for (var i = 0; i < 6; i++) {
      d3.select('#cp-content').append('div')
        .html(
          '<a href="#/schools/' + nsites[i].id +'">' + nsites[i].nm + '</a>' + 
          '<span style="width:30px; display:block;"></span>' + 
          '<span><i> ' + d3.format(0.3)(nsites[i].dist) + ' miles away </i></span>' +
          '<span style="width:30px; display:block;"></span>' + 
          '<span><i> ' + nsites[i].rooms + ' room(s) measured </i></span>'
          )
        .style('color','#525252')
        .style('font-weight','200')
        .style('font-size','12px')
        .style('margin','10px 0px 0px 5px');
    }
  };

  /*helper functions*/
  function getSingleColumn(matrix,col){
    let column = [];
    for (let i = 0; i < matrix.length; i++){
      column.push(matrix[i][col]);
    }
    return column;
  };

  function uniqueValsOnly(value, index, self) { 
    return self.indexOf(value) === index;
  };

  /*resize function*/
  $(window).on('resize', function() {
    if ($('#schools-crprofiles').length) {
      if (typeof cp_activedata !== 'undefined') {
        if (cp_activedata.length > 0) {
          initProfilesViz('ready');
          initWsHeatMap('redraw');
          if (typeof cp_activeroom != 'undefined' && cp_activeroom != '') {
            initCrHeatMap('redraw');
          }
        }
      }
    }
  });
})


