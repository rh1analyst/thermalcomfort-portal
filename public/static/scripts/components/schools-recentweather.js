app.directive('schoolrecentweather', function() {
  return {
    template: [

      '<div id="schools-recentweather">',
        '<div class="s-title">RECENT WEATHER</div>',
        '<div id="rw-panel">',
          '<div id="rw-selectortxt0" style="margin:2px 0px 6px 2px; font-size:10px;visibility:hidden;">Select Monitoring Location:</div>',
          '<div id="rw-stationmenu" style="visibility:hidden;"><select></select></div>',
          '<div id="rw-selectortxt1" style="margin:17px 0px 6px 2px; font-size:10px;visibility:hidden;">Select Date Range:</div>',
          '<div id="rw-daterngselector"></div>',
          '<div id="rw-selectortxt2" style="margin:17px 0px 6px 2px; font-size:10px;visibility:hidden;">Select Weather Factors:</div>',
          '<div id="rw-measureoptions"></div>',
          '<div style="height:1px;width:100%;background-color:#d3d3d3;margin:10px 0px 10px 0px;"></div>',
          '<div id="rw-downloaddata" style="margin-top:16px;"></div>',
        '</div>',
        '<div id="rw-viz"></div>',
      '</div>',

    ].join(''),
    scope: {},
    controller: 'schoolrecentweatherCtrl',
  }
})
app.controller('schoolrecentweatherCtrl', function($rootScope, $scope, $state, $stateParams, siteList, stationList) {

  /*substantiate reference vars*/
  const spinopts = {lines:9, length:9, width:5, radius:14, color:'#525252', speed:1.9, trail:40, className:'spinner'};
  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      stationList.getStations(function(stationlist) {
        if (sitelist && sitelist.length && stationlist && stationlist.length) {
          //init sequence
          sites = sitelist;
          sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
          stations = stationlist;
          stations.sort(function(a,b) { return d3.ascending(a.id,b.id) });
          if ($stateParams.siteid != '') {
            rw_activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
            rw_activewsta = 'Composite';
            rw_activespan = {
              start:formatDate(new Date().setDate(new Date().getDate()-6)),
              end:formatDate(new Date()),
              span:6,
            };
            rw_spanagglim = 5;
            //reset selectors
            initStationSelection();
            initRwDateRangePicker();
            initMeasuresOptions();
            //init data/viz
            initRecentWeather('ready');
          } else {
            rw_activesite = '';
            rw_activewsta = 'Composite';
            rw_activespan = {
              start:formatDate(new Date().setDate(new Date().getDate()-6)),
              end:formatDate(new Date()),
              span:6,
            };
            initRecentWeather('blank');
          }
        }
        else {
          //timeout re-call
          var retry = function() {
            setTimeout(function() {
              loadReqs();
            },100);
          }
          retry();
        }
      });
    });
  };

  function initRecentWeather(status) {
    if (status == 'ready') {
      rw_spinner = new Spinner(spinopts).spin(document.getElementById('rw-viz'));
      //format data request
      if (rw_activewsta == 'Composite') { 
        loadRwData('/qSiteWeatherSpan/?siteid=' + rw_activesite.site_id + '&start=' + rw_activespan.start + '&end=' + rw_activespan.end + '&span=' + rw_activespan.span);
      } else { 
        loadRwData('/qStationWeatherSpan/?wsid=' + rw_activewsta + '&start=' + rw_activespan.start + '&end=' + rw_activespan.end + '&span=' + rw_activespan.span);
      }

      function loadRwData(request) {
        d3.csv(request,function(data) {
          data.sort(function(a,b) { return d3.ascending(a.dt, b.dt) });
          //reveal elements
          rw_spinner.stop();
          //process data object
          rw_activedata = []
          if (data.length > 0 && rw_activespan.span <= rw_spanagglim) {
            data.forEach (function(d) {
              d.dt = d.dt;
              d.temp = +d.temp >= 0? +d.temp: null;
              d.relh = +d.relh >= 0? +d.relh: null;
              d.wspd = +d.wspd >= 0? +d.wspd: null;
              d.solr = +d.solr >= 0? +d.solr: null;
              d.rain = +d.rain >= 0? +d.rain: null;
              rw_activedata.push(d);
            });
            initRwFile();
          } else if (data.length > 0) {
            data.forEach(function(d) {
              d.dt = d.dt;
              d.temp_min = +d.temp_min >= 0? +d.temp_min: null;
              d.temp = +d.temp >= 0? +d.temp: null;
              d.temp_max = +d.temp_max >= 0? +d.temp_max: null;
              d.relh_min = +d.relh_min >= 0? +d.relh_min: null;
              d.relh = +d.relh >= 0? +d.relh: null;
              d.relh_max = +d.relh_max >= 0? +d.relh_max: null;
              d.wspd_min = +d.wspd_min >= 0? +d.wspd_min: null;
              d.wspd = +d.wspd >= 0? +d.wspd: null;
              d.wspd_max = +d.wspd_max >= 0? +d.wspd_max: null;
              d.solr_min = +d.solr_min >= 0? +d.solr_min: null;
              d.solr = +d.solr >= 0? +d.solr: null;
              d.solr_max = +d.solr_max >= 0? +d.solr_max: null;
              d.rain_min = +d.rain_min >= 0? +d.rain_min: null;
              d.rain = +d.rain >= 0? +d.rain: null;
              d.rain_max = +d.rain_max >= 0? +d.rain_max: null;
              rw_activedata.push(d);
            });
            initRwFile();
          } else {
            console.log('no recent weather data...');
          }
          initRwViz(status);
        });
      };
    } else {
      initRwViz(status);
    }
  };

  function initRwViz(status) {
    //remove existing elements, spin while data loads
    d3.selectAll('#rw-viz svg').remove();
    //init margin/dimensions vars
    rw_svgm = {top: 5, right: 25, bottom: 25, left: 40};
    rw_svgw = $('#rw-viz').width();
    rw_svgh = 0 - rw_svgm.top - rw_svgm.bottom
    rw_rowh = 120
    rw_measures = []
    Object.keys($stateParams.schools.rwmeasures).forEach(function(d) {
      if ($stateParams.schools.rwmeasures[d]) { 
        rw_svgh = rw_svgh + 120;``
        rw_measures.push(d);
      }
    });
    $('#schools-recentweather').height(rw_svgh + 100);

    //init min/max boundaries
    if (status !== 'blank') {
      if (rw_activespan.span < rw_spanagglim) {
        rw_ymintemp = Math.floor(d3.min(rw_activedata,function(d) { return d.temp })/5) * 5;
        rw_ymaxtemp = Math.ceil(d3.max(rw_activedata,function(d) { return d.temp })/5) * 5;
        rw_yminrelh = Math.floor(d3.min(rw_activedata,function(d) { return d.relh })/5) * 5;
        rw_ymaxrelh = Math.ceil(d3.max(rw_activedata,function(d) { return d.relh })/5) * 5;
        rw_ymaxwspd = Math.ceil(d3.max(rw_activedata,function(d) { return d.wspd })/2) * 2;
        rw_ymaxwspd = rw_ymaxwspd >= 0? rw_ymaxwspd: 10;
        rw_ymaxsolr = Math.ceil(d3.max(rw_activedata,function(d) { return d.solr })/100) * 100;
        rw_ymaxsolr = rw_ymaxsolr >= 0? rw_ymaxsolr: 1000;
        rw_ymaxrain = Math.ceil(d3.max(rw_activedata,function(d) { return d.rain }));
        rw_ymaxrain = rw_ymaxrain >= 0? rw_ymaxrain: 5;
      }
      else {
        rw_ymintemp = Math.floor(d3.min(rw_activedata,function(d) { return d.temp_min })/5) * 5;
        rw_ymaxtemp = Math.ceil(d3.max(rw_activedata,function(d) { return d.temp_max })/5) * 5;
        rw_yminrelh = Math.floor(d3.min(rw_activedata,function(d) { return d.relh_min })/5) * 5;
        rw_ymaxrelh = Math.ceil(d3.max(rw_activedata,function(d) { return d.relh_max })/5) * 5;
        rw_ymaxwspd = Math.ceil(d3.max(rw_activedata,function(d) { return d.wspd_max })/2) * 2;
        rw_ymaxwspd = rw_ymaxwspd >= 0? rw_ymaxwspd: 10;
        rw_ymaxsolr = Math.ceil(d3.max(rw_activedata,function(d) { return d.solr_max })/100) * 100;
        rw_ymaxsolr = rw_ymaxsolr >= 0? rw_ymaxsolr: 1000;
        rw_ymaxrain = Math.ceil(d3.max(rw_activedata,function(d) { return d.rain_max }));
        rw_ymaxrain = rw_ymaxrain >= 0? rw_ymaxrain: 5;
      }
    } 
    else {
      rw_ymintemp = 60
      rw_ymaxtemp = 90
      rw_yminrelh = 50
      rw_ymaxrelh = 100
      rw_ymaxwspd = 10
      rw_ymaxsolr = 1000
      rw_ymaxrain = 5
    }
    //determine timezone offset to US/Hawaii, adjust x-axis domain
    if (rw_activespan.span < 60) {
      var offset = new Date().toString().match(/([-\+][0-9]+)\s/)[1]*1/100
    }
    else { 
      var offset = 0
    }
    var xs = new Date(rw_activespan.start).setHours(new Date(rw_activespan.start).getHours() - offset)
    var xe = new Date(rw_activespan.end).setHours(new Date(rw_activespan.end).getHours() - offset) + 24*60*60*1000 - 1
    //define scales, axes, grid lines, voronois
    rw_xScale = d3.scaleTime() //x scale
    rw_xScale.domain([new Date(xs),new Date(xe)]).range([rw_svgm.left,rw_svgw - rw_svgm.right])
    rw_xAxis = d3.axisBottom() //x axis
    rw_xAxis.scale(rw_xScale).tickSize(0)
    
    if (rw_measures.indexOf('temp') !== -1) {
      rw_yScaleTemp = d3.scaleLinear() //y1 scale
      rw_yScaleTemp.domain([rw_ymintemp,rw_ymaxtemp]).range([rw_rowh * (rw_measures.indexOf('temp') + 1) - 15, rw_rowh*rw_measures.indexOf('temp') + 10])
      rw_yAxisTemp = d3.axisLeft() //y1 axis
      rw_yAxisTemp.scale(rw_yScaleTemp).ticks(6).tickSize(0).tickFormat(function(a) { return a + '' })
      rw_yGridTemp = d3.axisLeft() //y1 grid
      rw_yGridTemp.scale(rw_yScaleTemp).ticks(6).tickSize(-rw_svgw + rw_svgm.left + rw_svgm.right).tickFormat('')
    }
    
    if (rw_measures.indexOf('relh') !== -1) {
      rw_yScaleRelh = d3.scaleLinear() //y1 scale
      rw_yScaleRelh.domain([rw_yminrelh,rw_ymaxrelh]).range([rw_rowh * (rw_measures.indexOf('relh') + 1) - 15, rw_rowh*rw_measures.indexOf('relh') + 10])
      rw_yAxisRelh = d3.axisLeft() //y1 axis
      rw_yAxisRelh.scale(rw_yScaleRelh).ticks(6).tickSize(0).tickFormat(function(a) { return a + '' })
      rw_yGridRelh = d3.axisLeft() //y1 grid
      rw_yGridRelh.scale(rw_yScaleRelh).ticks(6).tickSize(-rw_svgw + rw_svgm.left + rw_svgm.right).tickFormat('')
    }

    if (rw_measures.indexOf('wspd') !== -1) {
      rw_yScaleWspd = d3.scaleLinear() //y2 scale
      rw_yScaleWspd.domain([0,rw_ymaxwspd]).range([rw_rowh * (rw_measures.indexOf('wspd') + 1) - 15, rw_rowh*rw_measures.indexOf('wspd') + 10])
      rw_yAxisWspd = d3.axisLeft() //y2 axis
      rw_yAxisWspd.scale(rw_yScaleWspd).ticks(6).tickSize(0).tickFormat(function(a) { return a + '' })
      rw_yGridWspd = d3.axisLeft() //y2 grid
      rw_yGridWspd.scale(rw_yScaleWspd).ticks(6).tickSize(-rw_svgw + rw_svgm.left + rw_svgm.right).tickFormat('')
    }
    
    if (rw_measures.indexOf('solr') !== -1) {
      rw_yScaleSolr = d3.scaleLinear() //y3 scale
      rw_yScaleSolr.domain([0,rw_ymaxsolr]).range([rw_rowh * (rw_measures.indexOf('solr') + 1) - 15, rw_rowh*rw_measures.indexOf('solr') + 10])
      rw_yAxisSolr = d3.axisLeft() //y3 axis
      rw_yAxisSolr.scale(rw_yScaleSolr).ticks(6).tickSize(0).tickFormat(function(a) {return a + '' })
      rw_yGridSolr = d3.axisLeft() //y3 grid
      rw_yGridSolr.scale(rw_yScaleSolr).ticks(6).tickSize(-rw_svgw + rw_svgm.left + rw_svgm.right).tickFormat('')
    }

    if (rw_measures.indexOf('rain') !== -1) {
      rw_yScaleRain = d3.scaleLinear() //y2 scale
      rw_yScaleRain.domain([0,rw_ymaxrain]).range([rw_rowh * (rw_measures.indexOf('rain') + 1) - 15, rw_rowh*rw_measures.indexOf('rain') + 10])
      rw_yAxisRain = d3.axisLeft() //y2 axis
      rw_yAxisRain.scale(rw_yScaleRain).ticks(6).tickSize(0).tickFormat(function(a) { return a + '' })
      rw_yGridRain = d3.axisLeft() //y2 grid
      rw_yGridRain.scale(rw_yScaleRain).ticks(6).tickSize(-rw_svgw + rw_svgm.left + rw_svgm.right).tickFormat('')
    }
    
    rw_yScaleVori = d3.scaleLinear() //yV scale
    rw_yScaleVori.domain([0,1]).range([rw_svgh - rw_svgm.top, rw_svgm.bottom])
    //define line functions
    rw_tempLineFunction = d3.line()
      .defined(function(d) { return d.temp >= 0 && d.temp !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y(function(d) { return rw_yScaleTemp(d.temp) });
    rw_relhLineFunction = d3.line()
      .defined(function(d) { return d.relh >= 0 && d.relh !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y(function(d) { return rw_yScaleRelh(d.relh) });
    rw_wspdLineFunction = d3.line()
      .defined(function(d) { return d.wspd >= 0 && d.wspd !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y(function(d) { return rw_yScaleWspd(d.wspd) });
    rw_solrLineFunction = d3.line()
      .defined(function(d) { return d.solr >= 0 && d.solr !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y(function(d) { return rw_yScaleSolr(d.solr) });
    rw_rainLineFunction = d3.line()
      .defined(function(d) { return d.rain >= 0 && d.rain !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y(function(d) { return rw_yScaleRain(d.rain) });
    //define voronoi function
    rw_voronoiFunction = d3.voronoi()
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y(function(d) { return rw_yScaleVori(0) })
      .extent([[rw_svgm.left,rw_svgm.top],[rw_svgw - rw_svgm.right,rw_svgh + rw_svgm.top]]);
    //define range functions
    rw_tempRangeFunction = d3.area()
      .defined(function(d) { return d.temp_min >= 0 && d.temp_max >= 0  && d.temp !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y0(function(d) { return rw_yScaleTemp(d.temp_min) })
      .y1(function(d) { return rw_yScaleTemp(d.temp_max) });
    rw_relhRangeFunction = d3.area()
      .defined(function(d) { return d.relh_min >= 0 && d.relh_max >= 0  && d.relh !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y0(function(d) { return rw_yScaleRelh(d.relh_min) })
      .y1(function(d) { return rw_yScaleRelh(d.relh_max) });
    rw_wspdRangeFunction = d3.area()
      .defined(function(d) { return d.wspd_min >= 0 && d.wspd_max >= 0  && d.wspd !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y0(function(d) { return rw_yScaleWspd(d.wspd_min) })
      .y1(function(d) { return rw_yScaleWspd(d.wspd_max) });
    rw_solrRangeFunction = d3.area()
      .defined(function(d) { return d.solr_min >= 0 && d.solr_max >= 0  && d.solr !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y0(function(d) { return rw_yScaleSolr(d.solr_min) })
      .y1(function(d) { return rw_yScaleSolr(d.solr_max) });
    rw_rainRangeFunction = d3.area()
      .defined(function(d) { return d.rain_min >= 0 && d.rain_max >= 0  && d.rain !== null })
      .x(function(d) { return rw_xScale(new Date(d.dt)) })
      .y0(function(d) { return rw_yScaleRain(d.rain_min) })
      .y1(function(d) { return rw_yScaleRain(d.rain_max) });
    //append new svg
    rw_vis = d3.select('#rw-viz')
    rw_chart = rw_vis.append('svg')
      .style('margin-top',rw_svgm.top)
      .attr('width',rw_svgw)
      .attr('height',rw_svgh + rw_svgm.bottom + rw_svgm.top)
    //draw x-axes
    rw_measures.forEach(function (d,i) {
      rw_chart.append('g')
        .attr('class','rwc x axis')
        .attr('transform','translate(0,' + (rw_svgh + 15 - rw_rowh*i) + ')')
        .attr('width',rw_svgw + 'px')
        .call(rw_xAxis)
        .selectAll('.tick text')
        .attr('y',5)
        .attr('dx',10);
    });

    //draw y-axes and grid lines
    if (rw_measures.indexOf('temp') !== -1) {
      rw_chart.append('g') //y1
        .attr('class','rwc y axis')
        .attr('height',rw_rowh + 'px')
        .call(rw_yAxisTemp)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
      rw_chart.append('g') //g1
        .attr('class','rwc y grid')
        .attr('height',rw_rowh + 'px')
        .call(rw_yGridTemp)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
    }
    if (rw_measures.indexOf('relh') !== -1) {
      rw_chart.append('g') //y1
        .attr('class','rwc y axis')
        .attr('height',rw_rowh + 'px')
        .call(rw_yAxisRelh)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
      rw_chart.append('g') //g1
        .attr('class','rwc y grid')
        .attr('height',rw_rowh + 'px')
        .call(rw_yGridRelh)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
    }
    if (rw_measures.indexOf('wspd') !== -1) {
      rw_chart.append('g') //y2
        .attr('class','rwc y axis')
        .attr('height',rw_rowh + 'px')
        .call(rw_yAxisWspd)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
      rw_chart.append('g') //g1
        .attr('class','rwc y grid')
        .attr('height',rw_rowh + 'px')
        .call(rw_yGridWspd)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
    }
    if (rw_measures.indexOf('solr') !== -1) {
      rw_chart.append('g') //y3
        .attr('class','rwc y axis')
        .attr('height',rw_rowh + 'px')
        .call(rw_yAxisSolr)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
      rw_chart.append('g') //g1
        .attr('class','rwc y grid')
        .attr('height',rw_rowh + 'px')
        .call(rw_yGridSolr)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
    }
    if (rw_measures.indexOf('rain') !== -1) {
      rw_chart.append('g') //y3
        .attr('class','rwc y axis')
        .attr('height',rw_rowh + 'px')
        .call(rw_yAxisRain)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
      rw_chart.append('g') //g1
        .attr('class','rwc y grid')
        .attr('height',rw_rowh + 'px')
        .call(rw_yGridRain)
        .selectAll('.tick text, .tick line, .domain')
        .attr('transform','translate(' + rw_svgm.left + ',0)');
    }
    rw_linesGroup = rw_chart.append('g')
      .attr('class','lines');
    //draw voronois
    rw_voronoiGroup = rw_chart.append('g')
      .attr('class','voronoi');

    //reveal hidden elements
    if (status == 'ready') {
      setTimeout(function() {
        rw_spinner.stop();
        $('#rw-viz').css('visibility','visible');
      },500);
      //(redraw) lines
      drawRwViz(status);
    }
  };

  function initStationSelection() {
    //fetch possible source list
    d3.csv('/qNearbyWeatherStations/?siteid=' + rw_activesite.site_id + '&island=' + rw_activesite.island + '&lat=' + rw_activesite.lat + '&lng=' + rw_activesite.lng + '&start=' + rw_activespan.start + '&end=' + rw_activespan.end,function(data) {
      if (data == 'None') {
        console.log('no data returned...');
      } else {
        if (data == null) {
          initStationSelection();
        } else {
          data = data.filter(function(d) { return +d.data_compl > 0 });
          rw_wstas = data;
          drawStationSelection(data);
        }
  
        function drawStationSelection(wstas) {
          //variable distance based on isolated sites
          if (rw_activesite.site_id == '402') {
            dist = '15';
          } else {
            dist = '10';
          }
          //reset selection
          $('#rw-stationmenu select').select2('destroy');
          $('#rw-stationmenu').html('<select></select>');
          //determine how to characterize the active selection
          if (rw_activewsta == 'Composite') {
            rw_nestedwstas = [{id:0,label:'Composite Sources',children:[]},{id:1,label:'Other Stations Within ' + dist + 'km',children:[]}]
            source = wstas.filter(function(d) { return d.ws_id == rw_activewsta })[0];
            wstas = wstas.filter(function(d) { return d.ws_id !== rw_activewsta });
          } else {
            rw_nestedwstas = [{id:0,label:'Composite from Multiple Sources (Validated)',children:[]},{id:1,label:'Composite Sources',children:[]},{id:2,label:'Other Stations Within ' + dist + 'km',children:[]}]
            source = wstas.filter(function(d) { return d.ws_id == rw_activewsta })[0];
            wstas = wstas.filter(function(d) { return d.ws_id !== rw_activewsta });
          }
          //initialize selection list
          d3.select('#rw-stationmenu').style('visibility','visible');
          d3.select('#rw-selectortxt0').style('visibility','visible');
          const wstamenu = d3.select('#rw-stationmenu select');
          // //populate selection list
          wstas.forEach(function(d) {
            let obj = [];
            if (d.ws_id == 'Composite') {
              obj = rw_nestedwstas.filter(function(e) { return e.label == 'Composite from Multiple Sources (Validated)' })[0].children;
              obj.push(d);
            } else if (rw_wstas.filter(function(d) { return d.ws_id == 'Composite' })[0].sources.split('|').indexOf(d.ws_id) >= 0) {
                if (rw_nestedwstas.length > 1) {
                  obj = rw_nestedwstas.filter(function(e) { return e.label == 'Composite Sources' })[0].children;
                  obj.push(d);
                }
            } else {
              obj = rw_nestedwstas.filter(function(e) { return e.label == 'Other Stations Within ' + dist + 'km' })[0].children;
              obj.push(d);
            }
          });

          //populate selection list
          rw_nestedwstas.forEach(function(e) {
            let grp = wstamenu.append('optgroup')
              .attr('label',e.label);
            grp.selectAll('option')
              .data(d3.map(e.children.filter(function(d) { return d.ws_id !== rw_activewsta }), function(d) { return d.ws_id }).keys().sort(function(a,b) { return d3.ascending(a == 'Composite'? 0: +wstas.filter(function(d) { return d.ws_id == a })[0].ws_dist,b == 'Composite'? 0: +wstas.filter(function(d) { return d.ws_id == b })[0].ws_dist) }))
              .enter().append('option')
              .attr('value',function(d) { return d })
              .text(function(d) { return d });
          });
          wstamenu._groups[0][0].innerHTML = '<option></option>' + wstamenu._groups[0][0].innerHTML;
          $('#rw-stationmenu select').select2({
            dropdownAutoWidth:true,
            placeholder:source.ws_id + ': ' + source.ws_loc,
            width:'calc(100% + 2px)',
            formatResult:formatWstas,
            // sortResults:sortWstas,
          }).on('change',function(d) {
            rw_activewsta = d.val;
            d3.select('#rw-stationmenu').style('visibility','hidden');
            drawStationSelection(rw_wstas);
            initRecentWeather('ready');
          });
          //populate distance value 
          if (rw_activewsta !== 'Composite') {
            d3.select('#rw-stationmenu').append('span').style('float','right').style('font-size','10px').style('margin','2px 2px 0px 0px').text(' (' + d3.format('.1f')(source.ws_dist) + ' km away)')
          }
          
          function formatWstas(wsta) {
            if (wsta.text != '') {
              if (wsta.children) {
                return wsta.text;
              } else {
                let wstarecord = data.filter(function(d) { return d.ws_id == wsta.text })[0];
                if (wstarecord.data_compl < 50) {
                  return $(
                    (wstarecord.ws_id == 'Composite'? '': '<span style="font-size:9px;float:left;margin:0px 5px 0px 0px; ">' + ('[' + d3.format('.2f')(wstarecord.ws_dist) + 'km]  ') + '</span>') + 
                    '<span style="display:inline-block; width:172px; max-width:172px; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;">' + wsta.text + ': ' + wstarecord.ws_loc + '</span>' + 
                    '<span style="font-size:9px;float:right;">[' + Math.floor((wstarecord.data_compl > 1? 1: wstarecord.data_compl)*1000)/10 + '%]</span>')
                }
              }
            } 
          };
          // function sortWstas(wsta) {
          //   return wsta.sort(function(a,b) {
          //     return d3.ascending(a.text == 'Composite'? 0: +wstas.filter(function(d) { return d.ws_id == a.text })[0].ws_dist,b.text == 'Composite'? 0: +wstas.filter(function(d) { return d.ws_id == b.text })[0].ws_dist);
          //   });
          // };
        };
      }
    });
  };

  function initRwDateRangePicker() {
    //destroy daterange picker, if exists
    if ($('#rw-daterngselector').data('dateRangePicker')) {
      $('#rw-daterngselector').unbind('datepicker-change');
      $('#rw-daterngselector').data('dateRangePicker').destroy();
    }
    //reveal selector text
    d3.select('#rw-selectortxt1').style('visibility','visible');
    //init drp library
    $('#rw-daterngselector').dateRangePicker({
      inline:true,
      container: '#rw-daterngselector',
      alwaysOpen:true,
      singleMonth:true,
      endDate:new Date()
    });
    //issue default starting/ending points
    let ends = [new Date(new Date(rw_activespan.start).setDate(new Date(rw_activespan.start).getDate() + 1)),new Date(new Date(rw_activespan.end).setDate(new Date(rw_activespan.end).getDate() + 1))];
    $('#rw-daterngselector').data('dateRangePicker').setDateRange(ends[1],ends[0]);
    d3.selectAll('.drp_top-bar,.normal-top').style('display','none');
    d3.selectAll('.date-picker-wrapper').style('padding','0px');
    //bind on-change function
    $('#rw-daterngselector').bind('datepicker-change',function(event,obj) {
      //set activespan object vals
      rw_activespan.start = formatDate(new Date(obj.date1.setDate(obj.date1.getDate())));
      rw_activespan.end = formatDate(new Date(obj.date2.setDate(obj.date2.getDate())));
      rw_activespan.span = daysDiff(rw_activespan.start,rw_activespan.end) + 1;
      initRecentWeather('ready');
      initStationSelection();
      initRwDateRangePicker();
    });
    //update selected span element
    d3.select('#rw-daterngselector span').remove();
    d3.select('#rw-daterngselector').append('span').style('float','right').style('font-size','10px').style('margin','-1px 0px 0px 0px').text('(' + formatDate(rw_activespan.start) + ' - ' + formatDate(rw_activespan.end) + ')')
  };

  function initMeasuresOptions() {
    //reveal selector text
    d3.select('#rw-selectortxt2').style('visibility','visible');
    //clear existing options
    d3.select('#rw-measureoptions').html('');
    //re-init options
    let labels = ['TEMPERATURE (°F)','RELATIVE HUMIDITY (%)','WIND SPEED (mph)','SOLAR RADIATION (w/m^2)','RAIN FALL (inches/hr)']
    Object.keys($stateParams.schools.rwmeasures).forEach(function(d,i) {
      d3.select('#rw-measureoptions').append('div')
        .html('<label><input type="checkbox" id="' + 'measure-' + d + '">' + 
              '<text style="font-size:11px; font-weight:200; letter-spacing:1px; position:relative; top:-1px;">' + labels[i] + '</text></label>');
      if ($stateParams.schools.rwmeasures[d]) {
        $('#measure-' + d).prop('checked',true);
      }
      $('#measure-' + d).on('click',function() {
        let m = this.id.split('-')[1]
        $stateParams.schools.rwmeasures[m] = !$stateParams.schools.rwmeasures[m];
        initRecentWeather('ready');
      });
    });
  };

  function drawRwViz(status) {
    let rw_nesteddata = [{
      key:0,
      values:rw_activedata,
    }]
    //draw ranges,lines
    if (rw_measures.indexOf('temp') !== -1) {
      if (rw_activespan.span > rw_spanagglim) {
        rw_linesGroup.append('path')
          .attr('d',rw_tempRangeFunction(rw_activedata))
          .attr('fill','#d6212a')
          .attr('opacity',0.4);
      }
      rw_linesGroup.append('path')
        .attr('d',rw_tempLineFunction(rw_activedata))
        .attr('stroke','#d6212a')
        .attr('stroke-width',1)
        .attr('opacity',1)
        .attr('fill','none');
    }
    if (rw_measures.indexOf('relh') !== -1) {
      if (rw_activespan.span > rw_spanagglim) {
        rw_linesGroup.append('path')
          .attr('d',rw_relhRangeFunction(rw_activedata))
          .attr('fill','#6c884b')
          .attr('opacity',0.4);
      }
      rw_linesGroup.append('path')
        .attr('d',rw_relhLineFunction(rw_activedata))
        .attr('stroke','#6c884b')
        .attr('stroke-width',1)
        .attr('opacity',1)
        .attr('fill','none');
    }
    if (rw_measures.indexOf('wspd') !== -1) {
      if (rw_activespan.span > rw_spanagglim) {
        rw_linesGroup.append('path')
          .attr('d',rw_wspdRangeFunction(rw_activedata))
          .attr('fill','#002f80')
          .attr('opacity',0.4);
      }
      rw_linesGroup.append('path')
        .attr('d',rw_wspdLineFunction(rw_activedata))
        .attr('stroke','#002f80')
        .attr('stroke-width',1)
        .attr('opacity',1)
        .attr('fill','none');
    }
    if (rw_measures.indexOf('solr') !== -1) {
      if (rw_activespan.span > rw_spanagglim) {
        rw_linesGroup.append('path')
          .attr('d',rw_solrRangeFunction(rw_activedata))
          .attr('fill','#ffb92f')
          .attr('opacity',0.4);
      }
      rw_linesGroup.append('path')
        .attr('d',rw_solrLineFunction(rw_activedata))
        .attr('stroke','#ffb92f')
        .attr('stroke-width',1)
        .attr('opacity',1)
        .attr('fill','none');
    }
    if (rw_measures.indexOf('rain') !== -1) {
      if (rw_activespan.span > rw_spanagglim) {
        rw_linesGroup.append('path')
          .attr('d',rw_rainRangeFunction(rw_activedata))
          .attr('fill','#87c404')
          .attr('opacity',0.4);
      }
      rw_linesGroup.append('path')
        .attr('d',rw_rainLineFunction(rw_activedata))
        .attr('stroke','#87c404')
        .attr('stroke-width',1)
        .attr('opacity',1)
        .attr('fill','none');
    }

    //draw voronois
    let voronois = rw_voronoiGroup.selectAll('path')
      .data(rw_voronoiFunction.polygons(d3.merge(rw_nesteddata.map(function(d) {return d.values }))))
    voronois.enter()
      .append('path')
      .attr('d',function(d) { if (typeof d !== 'undefined') { return d.data? 'M' + d.join('L') + 'Z' : null }})
      .on('mouseover',mouseoverRwLine)
      .on('mouseout',mouseoutRwLine);
    
    //draw foci
    if (rw_measures.indexOf('temp') !== -1) {
      rw_focusTemp = rw_chart.append('g')
        .attr('class','focus')
        .attr('transform','translate(-100,-100)');
      rw_focusTemp.append('line')
        .attr('class','focus-line');
      rw_focusTemp.append('text')
        .attr('class','focus-label')
        .attr('id','rw-focusTemp-label')
        .attr('x',5).attr('y',-5);
      rw_focusTemp.append('text').attr('class','focus-time').attr('x',-10).attr('y',rw_rowh*(rw_measures.indexOf('temp') + 1) - 4);
    }
    if (rw_measures.indexOf('relh') !== -1) {
      rw_focusRelh = rw_chart.append('g')
        .attr('class','focus')
        .attr('transform','translate(-100,-100)');
      rw_focusRelh.append('line')
        .attr('class','focus-line');
      rw_focusRelh.append('text')
        .attr('class','focus-label')
        .attr('id','rw-focusRelh-label')
        .attr('x',5).attr('y',-5);
      rw_focusRelh.append('text').attr('class','focus-time').attr('x',-10).attr('y',rw_rowh*(rw_measures.indexOf('relh') + 1) - 4);
    }
    if (rw_measures.indexOf('wspd') !== -1) {
      rw_focusWspd = rw_chart.append('g')
        .attr('class','focus')
        .attr('transform','translate(-100,-100)');
      rw_focusWspd.append('line')
        .attr('class','focus-line');
      rw_focusWspd.append('text')
        .attr('class','focus-label')
        .attr('id','rw-focusWspd-label')
        .attr('x',5).attr('y',-5);
      rw_focusWspd.append('text').attr('class','focus-time').attr('x',-10).attr('y',rw_rowh*(rw_measures.indexOf('wspd') + 1) - 4);
    }
    if (rw_measures.indexOf('solr') !== -1) {
      rw_focusSolr = rw_chart.append('g')
        .attr('class','focus')
        .attr('transform','translate(-100,-100)');
      rw_focusSolr.append('line')
        .attr('class','focus-line');
      rw_focusSolr.append('text')
        .attr('class','focus-label')
        .attr('id','rw-focusSolr-label')
        .attr('x',5).attr('y',-5);
      rw_focusSolr.append('text').attr('class','focus-time').attr('x',-10).attr('y',rw_rowh*(rw_measures.indexOf('solr') + 1) - 4);
    }
    if (rw_measures.indexOf('rain') !== -1) {
      rw_focusRain = rw_chart.append('g')
        .attr('class','focus')
        .attr('transform','translate(-100,-100)');
      rw_focusRain.append('line')
        .attr('class','focus-line');
      rw_focusRain.append('text')
        .attr('class','focus-label')
        .attr('id','rw-focusRain-label')
        .attr('x',5).attr('y',-5);
      rw_focusRain.append('text').attr('class','focus-time').attr('x',-10).attr('y',rw_rowh*(rw_measures.indexOf('rain') + 1) - 4);
    }
    
    //legend, unit labels
    let els = [
      {color:'#d6212a',label:'Temperature (°F)',unit:'°F',chart:rw_measures.indexOf('temp')},
      {color:'#6c884b',label:'Relative Humidity (%)',unit:'%',chart:rw_measures.indexOf('relh')},
      {color:'#002f80',label:'Wind Speed (mph)',unit:'mph',chart:rw_measures.indexOf('wspd')},
      {color:'#ffb92f',label:'Solar Radiation (w/m^2)',unit:'w/m^2',chart:rw_measures.indexOf('solr')},
      {color:'#87c404',label:'Rain Fall (in/hr)',unit:'in/hr',chart:rw_measures.indexOf('rain')}
    ]
    for (var i = 0; i < els.length; i++) {
      if (els[i].chart !== -1 ) {
        //legend
        rw_chart.append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('transform', 'translate(' + (rw_svgw - rw_svgm.right - 20) + ',' + (8 + rw_rowh*els[i].chart) + ')')
          .attr('width', 20)
          .attr('height', 5)
          .style('fill',els[i].color);
        rw_chart.append('text')
          .attr('x', 0)
          .attr('y', 0)
          .attr('transform', 'translate(' + (rw_svgw - rw_svgm.right - 25) + ',' + (13 + rw_rowh*els[i].chart) + ')')
          .text(els[i].label)
          .style('text-anchor', 'end')
          .style('font-size','10px');
        rw_chart.append('text')
          .attr('x', 0)
          .attr('y', 0)
          .attr('transform', 'translate(' + (rw_svgm.left + 5) + ',' + (13 + rw_rowh*els[i].chart) + ')')
          .text(els[i].unit)
          .style('font-size','9px');
      }
    }
  };

  function mouseoverRwLine(d) {
    //hide x-axes ticks
    d3.selectAll('.rwc.x.axis').selectAll('.tick text').transition().delay(0).duration(500).attr('fill','#d3d3d3');
    //adjust foci locations, labels, heights
    if (rw_measures.indexOf('temp') !== -1 && d.data.temp !== null) {
      rw_focusTemp.attr('transform','translate(' + rw_xScale(new Date(d.data.dt)) + ',0)');
      rw_focusTemp.select('#rw-focusTemp-label')
        .text(d3.format(0.2)(d.data.temp) + ' °F')
        .attr('class','focus-label')
        .style('fill','#d6212a')
        .attr('y',rw_yScaleTemp(d.data.temp));
      rw_focusTemp.select('.focus-line')
        .attr('y1',rw_yScaleTemp(d.data.temp))
        .attr('y2',rw_yScaleTemp(rw_ymintemp));
    }

    if (rw_measures.indexOf('relh') !== -1 && d.data.relh !== null) {
      rw_focusRelh.attr('transform','translate(' + rw_xScale(new Date(d.data.dt)) + ',0)');
      rw_focusRelh.select('#rw-focusRelh-label')
        .text(d3.format(0.2)(d.data.relh) + '%')
        .attr('class','focus-label')
        .style('fill','#6c884b')
        .attr('y',rw_yScaleRelh(d.data.relh));
      rw_focusRelh.select('.focus-line')
        .attr('y1',rw_yScaleRelh(d.data.relh))
        .attr('y2',rw_yScaleRelh(rw_yminrelh));
    }
    if (rw_measures.indexOf('wspd') !== -1 && d.data.wspd !== null) {
      rw_focusWspd.attr('transform','translate(' + rw_xScale(new Date(d.data.dt)) + ',0)');
      rw_focusWspd.select('#rw-focusWspd-label')
        .text(d.data.wspd >= 0? d3.format(0.2)( d.data.wspd) + ' mph': '')
        .attr('class','focus-label')
        .style('fill','#002f80')
        .attr('y',rw_yScaleWspd(d.data.wspd));
      rw_focusWspd.select('.focus-line')
        .attr('y1',rw_yScaleWspd(d.data.wspd))
        .attr('y2',rw_yScaleWspd(0));
    }
    if (rw_measures.indexOf('solr') !== -1 && d.data.solr !== null) {
      rw_focusSolr.attr('transform','translate(' + rw_xScale(new Date(d.data.dt)) + ',0)');
      rw_focusSolr.select('#rw-focusSolr-label')
        .text(d.data.solr >= 0? d3.format(0.4)(d.data.solr) + ' w/m^2': '')
        .attr('class','focus-label')
        .style('fill','#ffb92f')
        .attr('y',rw_yScaleSolr(d.data.solr));
      rw_focusSolr.select('.focus-line')
        .attr('y1',rw_yScaleSolr(d.data.solr))
        .attr('y2',rw_yScaleSolr(0));
    }
    if (rw_measures.indexOf('rain') !== -1 && d.data.rain !== null) {
      rw_focusRain.attr('transform','translate(' + rw_xScale(new Date(d.data.dt)) + ',0)');
      rw_focusRain.select('#rw-focusRain-label')
        .text(d.data.rain >= 0? d3.format(0.2)(d.data.rain) + ' in/hr': '')
        .attr('class','focus-label')
        .style('fill','#87c404')
        .attr('y',rw_yScaleRain(d.data.rain));
      rw_focusRain.select('.focus-line')
        .attr('y1',rw_yScaleRain(d.data.rain))
        .attr('y2',rw_yScaleRain(0));
    }
    //adjust time tracker
    d3.selectAll('.focus-time')
      .attr('dx',-20)
      .attr('text-ancor','middle')
      .text(d.data.dt)
      .style('font-size','9px');
  };

  function mouseoutRwLine(d) {
    //reveal x-axis ticks
    d3.selectAll('.rwc.x.axis').selectAll('.tick text').transition().delay(0).duration(500).attr('fill','#000');
    //hide foci
    if (typeof rw_focusTemp !== 'undefined') { rw_focusTemp.attr('transform','translate(-300, -100)') }
    if (typeof rw_focusRelh !== 'undefined') { rw_focusRelh.attr('transform','translate(-300, -100)') }
    if (typeof rw_focusWspd !== 'undefined') { rw_focusWspd.attr('transform','translate(-300, -100)') }
    if (typeof rw_focusSolr !== 'undefined') { rw_focusSolr.attr('transform','translate(-300, -100)') }
    if (typeof rw_focusRain !== 'undefined') { rw_focusRain.attr('transform','translate(-300, -100)') }
    //adjust time tracker
    d3.select('.focus-time')
      .attr('x',-10);
  };

  function initRwFile() {
    //prep element
    d3.select('#rw-downloaddata').html('');
    d3.select('#rw-downloaddata').append('i')
      .style('float','right')
      .style('margin','0px 0px 0px 5px')
      .style('position','relative')
      .style('top','-2px')
      .style('font-size','20px')
      .style('color','#637e8d')
      .style('border','solid 1px #637e8d')
      .style('border-radius','5px')
      .attr('class','material-icons')
      .style('cursor','pointer')
      .text('file_download')
      .on('mouseover',function() {
        this.style.color = '#fbb157';
        this.style.border = 'solid 1px #fbb157';
      })
      .on('mouseout',function() {
        this.style.color = '#637e8d'
        this.style.border = 'solid 1px #637e8d';
      })
      .on('click',function() {
        //set notification text
        let dlnotification = 'Configuring File'
        let ellipses = '...'
        updateRwDlText(dlnotification,ellipses);
        rwdlint = setInterval(function() {
          if (ellipses.length < 3) {
            ellipses = ellipses + '.';
          } else {
            ellipses = '';
          }
          updateRwDlText(dlnotification,ellipses);
        },200);
        function updateRwDlText(note,ellipses) {
          d3.select('#dlstatus')
            .text(ellipses + ' ' + note)
        };
        //get data from redshift
        if (rw_activewsta == 'Composite') {
          d3.csv('/qSiteWeatherFile/?siteid=' + rw_activesite.site_id + '&start=' + rw_activespan.start + '&end=' + rw_activespan.end,function(data) {
            let csvdata = [];
            data.forEach (function(d) {
              //format successfully loaded data
              let row = {
                Ws_Id:d.ws_id,
                Ws_Location:d.ws_loc,
                Ws_Lat:+d.lat,
                Ws_Lng:+d.lng,
                Dist_to_Site:+d.ws_dist,
                Timestamp:d.timestamp,
                Temperature:d.temp,
                Dew_Point:d.temp,
                Rel_Humidity:d.relhum,
                Wind_Speed:d.windspd,
                Gust_Speed:d.gustspd,
                Wind_Direction:d.winddir,
                Rain_Rate:d.preciprate,
                Solar_Radiation:d.solrad
              };
              csvdata.push(row);
            });
            csvdata.sort(function(a,b) { return d3.ascending(a.Timestamp, b.Timestamp); });
            let meta = ( 
              'data provided by MKThink + RoundhouseOne,' +
              'data sourced from Proprietary and/or Wunderground Community Stations,' +
              'selected school: ' + rw_activesite.site_nm_long + ',' +
              'selected station(s): Composite ' + rw_wstas.filter(function(d) { return d.ws_id == rw_activewsta })[0].ws_loc + ',' +
              'selected date range: ' + formatDate(rw_activespan.start) + ' to ' + formatDate(rw_activespan.end) + ',' +
              'file exported on: ' + formatDateTime(new Date()) + ',').split(',')
            let filenm = rw_activesite.site_nm_shrt.replace(/\W+/g, '') + '_' + formatDateMatch(rw_activespan.start) + '-' + formatDateMatch(rw_activespan.end) + '.csv'
            let fields = (Object.keys(csvdata[0]));
            let units = ['','','','','(km)','(HST)','(degf)','(degf)','(pct)','(mph)','(mph)','(deg)','(in/hr)','(w/m^2)'];
            downloadRwFile(csvdata,meta,fields,units,filenm);
          });
        } else {
          d3.csv('/qStationWeatherFile/?wsid=' + rw_activewsta + '&start=' + rw_activespan.start + '&end=' + rw_activespan.end,function(data) {
            let csvdata = [];
            data.forEach (function(d) {
              //format successfully loaded data
              let row = {
                Ws_Id:d.ws_id,
                Ws_Location:d.ws_loc,
                Ws_Lat:+d.lat,
                Ws_Lng:+d.lng,
                Dist_to_Site:+rw_wstas.filter(function(d) { return d.ws_id == rw_activewsta })[0].ws_dist,
                Timestamp:d.timestamp,
                Temperature:d.temp,
                Dew_Point:d.temp,
                Rel_Humidity:d.relhum,
                Wind_Speed:d.windspd,
                Gust_Speed:d.gustspd,
                Wind_Direction:d.winddir,
                Rain_Rate:d.preciprate,
                Solar_Radiation:d.solrad
              };
              csvdata.push(row);
            });
            csvdata.sort(function(a,b) { return d3.ascending(a.Timestamp, b.Timestamp); });
            let meta = ( 
              'data provided by MKThink + RoundhouseOne,' +
              'data sourced from Proprietary Sensors and/or Wunderground Community Stations,' +
              'selected school: ' + rw_activesite.site_nm_long + ',' +
              'selected station(s): ' + rw_activewsta + ',' +
              'selected date range: ' + formatDate(rw_activespan.start) + ' to ' + formatDate(rw_activespan.end) + ',' +
              'file exported on: ' + formatDateTime(new Date()) + ',').split(',');
            let filenm = rw_activewsta + '_' + formatDateMatch(rw_activespan.start) + '-' + formatDateMatch(rw_activespan.end) + '.csv';
            let fields = (Object.keys(csvdata[0]));
            let units = ['','','','','(km)','(HST)','(degf)','(degf)','(pct)','(mph)','(mph)','(deg)','(in/hr)','(w/m^2)'];
            downloadRwFile(csvdata,meta,fields,units,filenm);
          });
        }
        function downloadRwFile(csvdata,meta,fields,units,filenm) {
          let replacer = function(key,value) { return value === null ? '' : value }
          //structure csv content
          let csv = csvdata.map(function(row) { // add data rows
            let line =  fields.map(function(field) { 
              return JSON.stringify(row[field],replacer);
            }).join(',');
            return  '\r\n' + line;
          });
          let csv3 = [meta.join('\r\n'),fields,units,csv].join('\r\n') //add metadata rows
          let file = new Blob([csv3], {type:'application/csv;charset=utf-8;'});
          //download file
          if (navigator.msSaveBlob) { //iE10
            navigator.msSaveBlob(file,filenm)
          } else { //other browser
            let a = document.createElement('a')
            let url = URL.createObjectURL(file)
            a.href = url
            a.style = 'visibility:hidden'
            a.download = filenm
            document.body.appendChild(a)
            a.click()
            setTimeout(function() {
              document.body.removeChild(a)
            },3000);
            window.clearInterval(rwdlint);
            d3.select('#dlstatus').text('Download Selected Data');
          }
        };
      });
      d3.select('#rw-downloaddata').append('span')
        .attr('id','dlstatus')
        .style('float','right')
        .style('position','relative')
        .style('top','4px')
        .style('font-size','10px')
        .style('font-style','italic')
        .style('font-weifht','200')
        .text('Download Selected Data');
  };

  /*resize function*/
  $(window).on('resize',function() {
    if ($('#schools-recentweather').length) {
      if (typeof rw_activesite !== 'undefined' && typeof rw_activedata !== 'undefined') {
        if (rw_activesite !== '' && rw_activedata.length > 0) {
          initRwViz('ready');
        } else {
          initRwViz('blank');
        }
      } 
      else {
        initRwViz('blank');
      }
    }
  });
});


