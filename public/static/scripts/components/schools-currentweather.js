app.directive('schoolcurrentweather', function() {
  return {
    template: [

      '<div id="schools-currentweather" class="holder left">',
        '<div class="s-title">CURRENT WEATHER</div>',
        '<div id="s-cw-content">',
        '<div id="s-cw-winddial"><div id="compass">',
          '<div id="dial"><div id="arrow"></div></div>',
        '</div></div>',
      '</div>'

    ].join(''),
    scope: {},
    controller: 'schoolcurrentweatherCtrl',
  }
})
app.controller('schoolcurrentweatherCtrl', function($rootScope, $scope, $state, $stateParams, siteList) {

  /*substantiate reference vars*/
  const spinopts = {lines:9, length:9, width:5, radius:14, color:'#525252', speed:1.9, trail:40, className:'spinner'}

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      if (sitelist && sitelist.length) {
        //init sequence
        sites = sitelist;
        sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
        if ($stateParams.siteid != '') {
          let activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
          initCurrentWeather(activesite);
        } else {
          initCurrentWeather('');
        }
      }
      else {
        //timeout re-call
        var retry = function() {
          setTimeout(function() {
            loadReqs();
          },100);
        }
        retry();
      }
    });
  };

  function initCurrentWeather(activesite) {
    //create holders for displayed data
    cwsvg = d3.select('#s-cw-content').append('svg')
      .attr('width',$('#schools-currentweather').width() - 5)
      .attr('height',$('#schools-currentweather').height() - 44);
    let l0 = cwsvg.append('text')
      .attr('x',5).attr('y',13);
    l0.append('tspan')
      .attr('id','cwlocation')
      .style('font-weight',400)
      .style('font-size','16px');
    l0.append('tspan')
      .attr('id','cwdistance')
      .style('font-weight',200)
      .style('letter-spacing','0px')
      .style('font-size','10px');
    cwsvg.append('text')
      .attr('id','cwdatetime')
      .attr('x',5).attr('y',28)
      .style('font-size','10px')
      .style('letter-spacing','0px')
      .style('font-weight',200);
    let l1 = cwsvg.append('text')
      .attr('x',5).attr('y',95)
      .style('font-weight',200);
    l1.append('tspan')
      .attr('id','cwtempval')
      .style('font-size','66px');
    l1.append('tspan')
      .attr('id','cwtempdegf')
      .attr('dy',-31)
      .style('font-size','20px');
    cwsvg.append('text') //dew point
      .attr('x',5).attr('y',128)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('DEW POINT :');
    cwsvg.append('text')
      .attr('id','cwdewpt')
      .attr('x',115).attr('y',128)
      .style('font-size','12px')
      .style('font-weight',400);
    cwsvg.append('text') //rel humidity
      .attr('x',5).attr('y',151)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('REL. HUMIDITY :');
    cwsvg.append('text')
      .attr('id','cwrelhum')
      .attr('x',115).attr('y',151)
      .style('font-size','12px')
      .style('font-weight',400);
    cwsvg.append('text') //precip rate
      .attr('x',5).attr('y',174)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('PRECIP. RATE :');
    cwsvg.append('text')
      .attr('id','cwprecrate')
      .attr('x',115).attr('y',174)
      .style('font-size','12px')
      .style('font-weight',400);
    cwsvg.append('text') //precip accum
      .attr('x',5).attr('y',197)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('PRECIP. ACCUM :');
    cwsvg.append('text')
      .attr('id','cwprecaccum')
      .attr('x',115).attr('y',197)
      .style('font-size','12px')
      .style('font-weight',400);
    cwsvg.append('text') //air pessure
      .attr('x',5).attr('y',220)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('AIR PRESSURE :');
    cwsvg.append('text')
      .attr('id','cwpressure')
      .attr('x',115).attr('y',220)
      .style('font-size','12px')
      .style('font-weight',400);
    cwsvg.append('text') //ultra violet
      .attr('x',210).attr('y',197)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('UV :');
    cwsvg.append('text')
      .attr('id','cwultraviolet0')
      .attr('x',280).attr('y',197)
      .style('font-size','12px')
      .style('font-weight',400);
    cwsvg.append('g')
      .attr('id','cwultraviolet1')
      .attr('x',300).attr('y',197);
    cwsvg.append('text') //sol radiation
      .attr('x',210).attr('y',220)
      .style('font-size','11px')
      .style('font-weight',200)
      .text('SOLAR :');
    cwsvg.append('text')
      .attr('id','cwsolarrad')
      .attr('x',280).attr('y',220)
      .style('font-size','12px')
      .style('font-weight',400);
    d3.select('#s-cw-winddial').append('div') //wspd value (wind dial)
      .attr('id','cwwindspd')
      .style('position','relative').style('top','-67px')
      .style('width','95px')
      .style('text-align','center')
      .style('font-size','24px')
      .style('font-weight',400);
    d3.select('#s-cw-winddial').append('div')
      .style('position','relative').style('top','-70px')
      .style('width','95px')
      .style('text-align','center')
      .style('font-size','11px')
      .style('font-weight',200)
      .style('visibility','hidden')
      .text('mph');
    d3.select('#s-cw-winddial').append('div') //wdir prefix (wind dial)
      .style('position','relative').style('top','-33px')
      .style('width','95px')
      .style('text-align','center')
      .style('font-size','11px')
      .style('font-weight',200)
      .style('visibility','hidden')
      .text('wind from');
    d3.select('#s-cw-winddial').append('div') //wdir value (wind dial)
      .attr('id','cwwinddir')
      .style('position','relative').style('top','-33px')
      .style('width','95px')
      .style('text-align','center')
      .style('font-size','24px')
      .style('font-weight',400);
    //initiate update interval
    if (activesite != '') {
      cw_spinner = new Spinner(spinopts).spin(document.getElementById('schools-currentweather'));
      updateCurrentWeather(activesite);
      cwcounter = 0;
      cwint = setInterval(function() {
        updateCurrentWeather(activesite);
        if (++cwcounter === 10) {
          window.clearInterval(cwint);
        }
      },100000);
    }
  };

  function updateCurrentWeather (activesite) {
    if (typeof cw_spinner !== 'undefined') { cw_spinner.stop(); }
    d3.csv('/qSiteWeather/?siteid=' + activesite.site_id,function(data) {
      //clear spinner, reveal elements
      cw_spinner.stop();
      $('#s-cw-content,#s-cw-winddial').css('display','block');
      if (data.length == 0) {
        showNoCurrentWeatherData();
      } else {
        d3.select('#cwlocation')
          .text(data[0].ws_loc)
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwdistance')
          .text(' (' + Math.floor(data[0].ws_dist * 100)/100 + ' km away)')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwdatetime')
          .text('Last Updated - ' + data[0].dt_local)
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwtempval')
          .text((+data[0].temp_f >= 0) && (data[0].temp_f != '')? data[0].temp_f: '--' )
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwtempdegf')
          .text('°F')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwdewpt')
          .text((+data[0].dewpt_f >= 0) && (data[0].dewpt_f != '')? data[0].dewpt_f + ' °F': '--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwrelhum')
          .text((+data[0].relhum_pct >= 0) && (data[0].relhum_pct !== '')? data[0].relhum_pct + '%': '--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwprecrate')
          .text((+data[0].precip_inchesperhr >= 0) && (data[0].precip_inchesperhr !== '')? data[0].precip_inchesperhr + ' in/hr': '--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwprecaccum')
          .text((+data[0].precip_inchesaccum >= 0) && (data[0].precip_inchesaccum !== '')? data[0].precip_inchesaccum + ' in': '--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwpressure')
          .text('--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwsolarrad')
          .text((+data[0].solrad_wpms >= 0) && (data[0].solrad_wpms !== '')? data[0].solrad_wpms + ' w/m^2': '--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#cwultraviolet0')
          .text('--')
          .transition().delay(0).duration(1000).attr('fill','#6C884B')
          .transition().delay(0).duration(3000).attr('fill','#000000');
        d3.select('#compass')
          .transition().delay(0).duration(1000).style('transform','rotate(' + data[0].winddir_deg + 'deg)')
        d3.select('#arrow')
          .style('border-top','21px solid #6C884B')
          .transition().delay(1000).duration(1000).style('border-top','21px solid rgb(0,0,0)')
        d3.selectAll('#s-cw-winddial div')
          .style('visibility','visible')
        //update values/units elements
        d3.select('#cwwindspd') //wspd
          .text(+data[0].windspd_mph)
          .transition().delay(0).duration(1000).style('color','#6C884B')
          .transition().delay(1000).duration(2000).style('color','#000000')
        d3.select('#cwwinddir') //wdir
          .text(data[0].winddir_str)
          .transition().delay(0).duration(1000).style('color','#6C884B')
          .transition().delay(1000).duration(2000).style('color','#000000')
      }
    });
  };

  function showNoCurrentWeatherData() {
    d3.select('#cwlocation')
      .style('font-size','10px')
      .style('font-weight',200)
      .text('NO ACTIVE WEATHER STATION REPORTING')
    if (typeof cwint !== 'undefined') { window.clearInterval(cwint) }
  };
})


