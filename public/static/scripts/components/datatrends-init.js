app.directive('datatrends', function() {
  return {
    template: [

        '<div id="dt-panel">',
          '<div id="dt-instructions"></div>',
          '<div id="dt-selections"></div>',
          '<div id="dt-addrow"></div>',
        '</div>',
        '<div id="dt-viz"></div>',
        '<div id="dt-statstable"></div>',

    ].join(''),
    scope: {},
    controller: 'datatrendsCtrl',
  }
});
app.controller('datatrendsCtrl', function($rootScope, $scope, $state, $stateParams, siteList, roomList) {

  /*substantiate reference vars*/
  const spinopts = {lines:9, length:9, width:5, radius:14, color:'#525252', speed:1.9, trail:40, className:'spinner'};
  const colors = [{label:'green1','hex':'#33cc99'},{label:'blue1','hex':'#6699ff'},{label:'orange1','hex':'#ee5549'},{label:'purple1','hex':'#9900ff'},{label:'green2','hex':'#bad294'},{label:'blue2','hex':'#a5d7e0'},{label:'orange2','hex':'#fbb157'},{label:'purple2','hex':'#e850e6'}];
  const allhrs = [d3.timeParse('%H:%M')('00:00'),d3.timeParse('%H:%M')('02:00'),d3.timeParse('%H:%M')('04:00'),d3.timeParse('%H:%M')('06:00'),d3.timeParse('%H:%M')('08:00'),d3.timeParse('%H:%M')('10:00'),d3.timeParse('%H:%M')('12:00'),d3.timeParse('%H:%M')('14:00'),d3.timeParse('%H:%M')('16:00'),d3.timeParse('%H:%M')('18:00'),d3.timeParse('%H:%M')('20:00'),d3.timeParse('%H:%M')('22:00')];
  /*substantiate data vars*/
  let dt_activespans = []; //active selection (group-level)
  let dt_activextnts = []; //available ranges (group-level)
  let dt_gcounter = 0; //manage array position (group-level)
  let dt_scounter = 0; //manage array position (subgroup-level)
  let dt_activesubs = [];
  let dt_activedata = [];
  let dt_activestat = [];
  /*substantiate settings vars*/
  let dt_axisrnge = {ymin:'',ymax:''};
  let dt_axislock = {ymin:'',ymax:''};
  let dt_statfilt = {sh:'yes',sd:'yes'};
  /*substantiate utility fields*/
  let dt_datacounter = 0;
  let dt_statcounter = 0;

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      roomList.getRooms(function(roomlist) {
        if (sitelist && sitelist.length && roomlist && roomlist.length) {
          //init sequence
          sites = sitelist
          sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) })
          rooms = roomlist
          rooms.sort(function(a,b) { return d3.ascending(a.room_nm,b.room_nm) })
          //init exploratory mode
          initExploratoryMode();
        } 
        else {
          //timeout re-call
          var retry = function() {
            setTimeout(function() {
              loadReqs();
            },100);
          };
          retry();
        }
      });
    });
  };

  function initExploratoryMode() {
    $('#dt-instructions').html(
      '<div class="title">Instructions :</div>' +
      '<div style="width:100%;"><div style="height:1px;width:150px;background-color:#d3d3d3;margin:-6px 0px 3px 0px;float:left;"></div></div>' +
      '<div class="instruction">1. Select a site</div>' +
      '<div class="instruction">2. Select a time period</div>' +
      '<div class="instruction">3. Select rooms to view</div>' +
      '<div class="instruction">4. Make additional selections to compare trends</div>' +
      '<div class="instruction">5. Download data using orange button in top-left</div>');
    //update height based on variable height of instructions
    $('#dt-selections').css('max-height',(750 - $('#dt-instructions').height() - 80) + 'px');
    //reset selection window for Exploratory searches
    $('#dt-selections').html('');
    $('#dt-addrow').html('');
    //clear statstable content
    d3.select('#dt-statstable table').remove();
    d3.select('#dt-statstable text').remove();
    //draw 'add new site' button
    d3.select('#dt-addrow').html('');
    d3.select('#dt-addrow').append('i')
      .style('float','right')
      .style('margin','0px 7.5px 0px 0px')
      .style('position','relative')
      .style('top','-2px')
      .style('font-size','20px')
      .style('color','#637e8d')
      .style('border','solid 1px #637e8d')
      .style('border-radius','5px')
      .attr('class','material-icons')
      .style('cursor','pointer')
      .text('add')
      .on('mouseover',function() {
        this.style.color = '#fbb157';
        this.style.border = 'solid 1px #fbb157';
      })
      .on('mouseout',function() {
        this.style.color = '#637e8d'
        this.style.border = 'solid 1px #637e8d';
      })
      .on('click',addEmptyDtGroup);
    d3.select('#dt-addrow').append('span')
      .attr('id','addstatus')
      .style('float','right')
      .style('margin','0px 7px 0px 0px')
      .style('position','relative')
      .style('top','4px')
      .style('font-size','10px')
      .style('font-style','italic')
      .style('font-weifht','200')
      .text('Add Site, Time Period');
    //init vizualization
    initDtViz('ready');
    initDtTable('blank',1);
    //add empty first group
    addEmptyDtGroup();
  };

  function initDtViz(phase) {
    //init margin/dimensions vars
    dt_svgm = {top: 0, right: 25, bottom: 15, left: 20};
    dt_svgw = $('#dt-viz').width();
    dt_svgh = $('#dt-viz').height() - 30;
    //update ymax, ymin if necessary
    if (dt_axislock.ymin == '' && dt_axislock.ymax == '') {
      if (dt_axisrnge.ymin == '' || dt_axisrnge.ymax == '' || dt_activedata.length == 0) { 
        dt_axisrnge.ymin = 60; 
        dt_axisrnge.ymax = 80;
      } else {
        //re-assess ymin/ymax values
        dt_axisrnge.ymin = d3.min(dt_activedata,function(d) { return d.temp_f });
        dt_axisrnge.ymax = d3.max(dt_activedata,function(d) { return d.temp_f });
      }
    } else {
      dt_axisrnge.ymin = dt_axislock.ymin; 
      dt_axisrnge.ymax = dt_axislock.ymax;
    }
    //define scales, axes, grids
    dt_xScale = d3.scaleTime();
    dt_xScale.domain([d3.timeParse('%H:%M')('00:00'),d3.timeParse('%H:%M')('23:59')]).range([dt_svgm.left, dt_svgw - dt_svgm.right]);
    dt_xAxis = d3.axisBottom();
    dt_xAxis.scale(dt_xScale).tickSize(0).tickValues(allhrs).tickFormat(d3.timeFormat('%H:00'));
    dt_yScale = d3.scaleLinear();
    dt_yScale.domain([Math.floor(dt_axisrnge.ymin / 2) * 2,Math.ceil(dt_axisrnge.ymax / 2) * 2]).range([dt_svgh - dt_svgm.top,dt_svgm.bottom]);
    dt_yAxis = d3.axisLeft()
    if (phase == 'blank') { 
      dt_yAxis.scale(dt_yScale).tickSize(0).tickFormat('');
    } else { 
      dt_yAxis.scale(dt_yScale).tickSize(0).tickFormat(function(a) { return a + '' });
    }
    dt_yGrid = d3.axisLeft();
    dt_yGrid.scale(dt_yScale).tickSize(-dt_svgw + dt_svgm.left + dt_svgm.right).tickFormat('');
    //define line function
    dt_lineFunction = d3.line()
      .curve(d3.curveCardinal)
      .defined(function(d) { return typeof d.temp_f !== 'undefined' })
      .x(function(d) { return dt_xScale(new Date(d.time)) })
      .y(function(d) { return dt_yScale(d.temp_f) });
    //define voronoi function
    dt_voronoiFunction = d3.voronoi()
      .x(function(d) { return dt_xScale(new Date(d.time)) })
      .y(function(d) { return dt_yScale(d.temp_f) })
      .extent([[dt_svgm.left,dt_svgm.top], [dt_svgw - dt_svgm.right,dt_svgh + dt_svgm.top]]);
    //remove existing viz elements
    d3.selectAll('#dt-viz svg,#dt-viz div,#dt-viz i,#dt-viz span').remove();
    //append new svg
    dt_chart = d3.select('#dt-viz').append('svg')
      .attr('width',dt_svgw  + dt_svgm.left + dt_svgm.right)
      .attr('height',dt_svgh + dt_svgm.bottom + dt_svgm.top);
    //key funciton
    dt_key = function(d) { return d.key }
    //draw school hour gray box
    dt_chart.append('g') 
      .attr('class','school-hours')
      .append('rect')
      .attr('width',Math.round((dt_svgw - dt_svgm.right - dt_svgm.left) * 6 / 24) + 'px')
      .attr('height',Math.round(dt_svgh - dt_svgm.top - dt_svgm.bottom) + 'px')
      .attr('x',dt_xScale(d3.timeParse('%H:%M')('08:00')))
      .attr('y',dt_svgm.bottom)
      .style('opacity',0.08)
      .style('background-color','#4D4D4D')
      .style('width',Math.round((dt_svgw - dt_svgm.right - dt_svgm.left) * 6 / 24) + 'px')
      .style('height',Math.round(dt_svgh - dt_svgm.top - dt_svgm.bottom) + 'px');
    //draw axes
    dt_chart.append('g')
      .attr('class','dt x axis')
      .attr('transform','translate(0,' + (dt_svgh - dt_svgm.top) + ')')
      .attr('width',dt_svgw + 'px')
      .call(dt_xAxis)
      .selectAll('.tick text')
      .attr('y','5')
      .attr('dx','10');
    dt_chart.append('g')
      .attr('class','dt y axis')
      .attr('height',(dt_svgh - dt_svgm.top) + 'px')
      .call(dt_yAxis)
      .selectAll('.tick text, .tick line, .domain')
      .attr('transform','translate(' + dt_svgm.left + ',0)');
    //draw gridlines
    dt_chart.append('g')
      .attr('class','dt y grid')
      .attr('height',(dt_svgh - dt_svgm.top) + 'px')
      .call(dt_yGrid)
      .selectAll('.tick text, .tick line, .domain')
      .attr('transform','translate(' + dt_svgm.left + ',0)');
    //create data elements
    dt_linesGroup = dt_chart.append('g')
      .attr('class','lines')
      .attr('transform','translate(0,' + dt_svgm.top + ')');
    dt_voronoiGroup = dt_chart.append('g') 
      .attr('class','voronoi');
    dt_focus = dt_chart.append('g')
      .attr('class','focus')
      .attr('transform','translate(-100,-100)');
    dt_focus.append('line')
      .attr('class','focus-line')
    dt_focus.append('text')
      .attr('class','focus-label')
      .attr('x',4)
    dt_focus.append('text')
      .attr('class','focus-time')
      .attr('x',-10)
    //add annotations
    dt_chart.append('text')
      .attr('x',0)
      .attr('y',0)
      .attr('transform','translate(' + (dt_svgm.left + 4) + ',' + (dt_svgm.bottom + 3) + ')')
      .text('°F')
      .style('font-size','9px');
    dt_chart.append('text')
      .attr('class','main-axis-label')
      .attr('x',dt_xScale(d3.timeParse('%H:%M')('11:00')))
      .attr('y', dt_svgh + dt_svgm.top - 4)
      .text('A school day typically runs from 8am to 2pm')
      .style('text-anchor','middle')
      .style('font-size','9px')
      .style('font-weight',300);
    //draw screencap button
    d3.select('#dt-viz').append('i')     
      .attr('id','dt-screencapbutton')
      .attr('class','material-icons button')
      .style('visibility','hidden')
      .style('position','absolute').style('top','10px').style('right','20px')
      .style('padding','5px')
      .style('font-size','20px')
      .style('border-radius','15px')
      .style('background-color','#637e8d')
      .style('color','#ffffff')
      .style('cursor','pointer')
      .text('print')
      .on('mouseover',function() { this.style.backgroundColor = '#025468' })
      .on('mouseout',function() { this.style.backgroundColor = '#637e8d' })
      .on('click',printDtScreencap);
    d3.select('#dt-screencapbutton').transition().delay(250).style('visibility','visible');
    //draw lock y button
    d3.select('#dt-viz').append('i')
      .attr('id','dt-lockybutton')
      .attr('class','material-icons button')
      .style('visibility','hidden')
      .style('position','absolute').style('top','-2px').style('left','15px')
      .style('font-size','11px')
      .style('color','#000000')
      .style('background-color','#f9f9f9')
      .style('cursor','pointer')
      .text(dt_axislock.ymin == '' & dt_axislock.ymax == ''? 'lock_open': 'lock')
      .on('click',
        function() {
          if (d3.select('#dt-viz span')._groups[0][0] == null) {
            //add input area for locking extents
            let lockform = d3.select('#dt-viz').append('span')
              .style('position','absolute').style('top','0px').style('left','40px')
              .style('display','block')
              .style('height','30px')
              .style('width','94px')
              .style('padding','5px')
              .style('border','solid 1px #525252')
              .style('border-radius','3px')
              .style('background-color','#fff')
              .style('letter-spacing','1px')
              .style('font-size','12px')
              .style('font-weight',200)
              .style('color','#525252');
            //max-entry
            let maxentry = lockform.append('div').style('width','55px').style('margin','2px').style('display','block').style('float','left');
            maxentry.append('span').style('float','left').style('font-size','9px').style('letter-spacing','1px').text('MAX');
            maxentry.append('input')
              .attr('type','text')
              .attr('value',dt_axislock.ymax)
              .attr('placeholder','(°F)')
              .style('width','20px')
              .style('height','9px')
              .style('font-size','9px')
              .style('color','#525252')
              .style('background-color','#f1f1f1')
              .style('border','none')
              .style('margin','0px 0px 0px 4px')
              .style('float','right')
              .style('padding','1px 3px')
              .on('click',function() { $(this).select() })
              .on('change',function() { 
                dt_axislock.ymax = this.value;
                if (dt_axislock.ymin !== '' && dt_axislock.ymax !== '') {
                  d3.select('#dt-locksavebtn')
                    .transition().delay(0).duration(1000)
                    .style('background-color','#6c884b')
                    .style('color','#ffffff');
                } else {
                  d3.select('#dt-locksavebtn')
                    .transition().delay(0).duration(1000)
                    .style('background-color','#a9a9a9')
                    .style('color','#d3d3d3');
                }
              });
            //min-entry
            let minentry = lockform.append('div').style('width','55px').style('margin','2px').style('display','block').style('float','left');
            minentry.append('span').style('float','left').style('font-size','9px').style('letter-spacing','1px').text('MIN');
            minentry.append('input')
              .attr('type','text')
              .attr('value',dt_axislock.ymin)
              .attr('placeholder','(°F)')
              .style('width','20px')
              .style('height','9px')
              .style('font-size','9px')
              .style('color','#525252')
              .style('background-color','#f1f1f1')
              .style('border','none')
              .style('margin','0px 0px 0px 4px')
              .style('float','right')
              .style('padding','1px 3px')
              .on('click',function() { $(this).select() })
              .on('change',function() { 
                dt_axislock.ymin = this.value;
                if (dt_axislock.ymin !== '' && dt_axislock.ymax !== '') {
                  d3.select('#dt-locksavebtn')
                    .transition().delay(0).duration(1000)
                    .style('background-color','#6c884b')
                    .style('color','#ffffff');
                } else {
                  d3.select('#dt-locksavebtn')
                    .transition().delay(0).duration(1000)
                    .style('background-color','#a9a9a9')
                    .style('color','#d3d3d3');
                }
              });
            //add save button
            let savebtn = lockform.append('span')
              .attr('id','dt-locksavebtn')
              .style('float','left')
              .style('position','relative')
              .style('top','-7px')
              .style('left','3px')
              .style('letter-spacing','1px')
              .style('border-radius','3px')
              .style('background-color',(dt_axislock.ymin !== '' && dt_axislock.ymax !== '')? '#6c884b': '#a9a9a9')
              .style('color',(dt_axislock.ymin !== '' && dt_axislock.ymax !== '')? '#ffffff': '#d3d3d3')
              .style('font-size','9px')
              .style('padding','2px')
              .text('SAVE')
              .on('mouseover',function() {
                if (dt_axislock.ymin !== '' && dt_axislock.ymax !== '') {
                  $(this).css({cursor:'pointer',color:'#f4eacb'});
                } else {
                  $(this).css('cursor',null);
                }
              })
              .on('mouseout',function() {
                if (dt_axislock.ymin !== '' && dt_axislock.ymax !== '') {
                  $(this).css('color','#ffffff');
                } else {
                  $(this).css('color','#d3d3d3');
                }
              })
              .on('click',function() {
                if (dt_axislock.ymin !== '' && dt_axislock.ymax !== '') {
                  d3.selectAll('#dt-viz span').remove();
                  d3.select('#dt-lockybutton').text('lock');
                  drawDtLines('init');
                }
              });
          }
          else {
            dt_axislock.ymin = '';
            dt_axislock.ymax = '';
            d3.selectAll('#dt-viz span').remove();
            d3.select('#dt-lockybutton').text('lock_open');
          }
        });
    d3.select('#dt-lockybutton').transition().delay(250).style('visibility','visible');
    //on resize, draw lines too
    if (phase == 'redraw') { drawDtLines('redraw'); }
  };

  function addEmptyDtGroup() {
    if ($('#dt-sitemenu').length == 0) {
      //establish group position, advance counter
      let g = dt_gcounter
      dt_gcounter++
      //establish default start/end dates
      dt_activespans[g] = {start:new Date(2018,0,1),end:new Date(2018,0,7)};
      //draw selector holder div
      d3.select('#dt-selections').append('div')
        .attr('id','g' + g)
        .attr('class','groupholder')
        .html('<p class="menu" id="dt-sitemenu"><select></select></p>');
      //populate selection list
      let sitemenu = d3.select('#g' + g + ' select');
      sitemenu.selectAll('option')
        .data(d3.map(sites, function(d) { return d.site_nm_long }).keys())
        .enter().append('option')
        .text(function(d) { return d });
      sitemenu._groups[0][0].innerHTML = '<option></option>' + sitemenu._groups[0][0].innerHTML;
      //init select2
      $('#g' + g + ' select').select2({
        placeholder:'Select Site',
        dropdownAutoWidth:true,
        width:288,
      }).on('change',function(){
        //on selection change...
        let hex = colors[g % colors.length].hex;
        let rgba = hex2rgba(hex,0.5);
        selectDtSite(g,sitemenu.property('value'),hex,rgba)
      });
      $('#dt-sitemenu').on('select2-open',function() {
        d3.selectAll('.select2-drop').style('max-width','288x').style('width','288px');
      });
      //collapse other active groups
      for (var i = 0; i < (dt_gcounter - 1); i++) {
        if (i !== g && $('#g' + i).length > 0) {
          let hex = colors[i % colors.length].hex;
          let rgba = hex2rgba(hex,0.5);
          collapseDtGroup(i,hex,rgba);
        }
      }
    };

    function collapseDtGroup(g,hex,rgba) {
      $('#g' + g + '-daterangepicker').css('display','none');
      $('#g' + g + ' .menu').css('display','none');
      $('#g' + g + '-collapsebutton i').css({'transform':'rotate(180deg)','transition':'transform 1s ease','padding':'4px 3px 2px 3px','pointer-events':'none'});
      $('#g' + g + '-nodatanotice').css('display','none');
      setTimeout(function() {
        $('#g' + g + '-collapsebutton').css({'pointer-events':'auto'});
      },500);
    };

    function expandDtGroup(g,hex,rgba) {
      $('#g' + g + '-daterangepicker').css('display','block');
      $('#g' + g + ' .menu').css('display','block');
      $('#g' + g + '-collapsebutton i').css({'transform':'rotate(360deg)','transition':'transform 1s ease','padding':'3px 3px 3px 3px','pointer-events':'none'});
      $('#g' + g + '-nodatanotice').css('display','block');
      setTimeout(function() {
        $('#g' + g + '-collapsebutton').css({'transform':'rotate(0deg)','transition':'transform 0s ease','pointer-events':'auto'});
      },500);
    };

    function clearDtSiteSelection(g) {
      //remove selector
      d3.select('#g' + g).remove();
      //clear data objects
      dt_activedata = dt_activedata.filter(function(d) { return d.g !== g });
      dt_activestat = dt_activestat.filter(function(d) { return d.g !== g });
      dt_activesubs = dt_activesubs.filter(function(d) { return d.g !== g });
      if (dt_activedata.length == 0) {
        addEmptyDtGroup();
      }
      //remove line path
      d3.selectAll('path.line.g' + g)
        .transition().duration(500)
        .style('opacity','0.001')
        .transition().delay(400).duration(100)
        .remove()
      //remove line label
      d3.selectAll('text.dt_pathlabel.g' + g) 
        .transition().delay(0).duration(500)
        .style('opacity','0.001')
        .transition().delay(400).duration(100)
        .remove()
      //redraw vizualization
      drawDtLines('redraw');
      //redraw statstable
      initDtTable('init');
    };

    function selectDtSite(g,gname,hex,rgba) {
      //remove selector
      d3.select('#g' + g + ' #dt-sitemenu').remove();
      //draw selection
      d3.select('#g' + g).append('div')
        .attr('class','site-labelholder')
        .style('z-index','9999');
      //attach download button
      d3.select('#g' + g + ' .site-labelholder').append('i')
        .attr('id','g' + g + '-downloadbutton')
        .attr('class','material-icons button')
        .style('visibility','hidden')
        .style('margin-left','0px')
        .style('margin-right','3px')
        .style('background-color','#fbb157')
        .text('file_download')
        .on('mouseover',function() { this.style.backgroundColor = 'rgba(251, 177, 87, 0.5)' })
        .on('mouseout',function() { this.style.backgroundColor = '#fbb157' })
        .on('click',function() { downloadDtCsv(g,gname) });
      //redraw site name
      d3.select('#g' + g + ' .site-labelholder').append('span')
        .attr('class','sitename')
        .style('background-color',hex)
        .text(gname)
      //attach close-group button
      d3.select('#g' + g + ' .site-labelholder').append('i')
        .attr('id','g' + g + '-clearbutton')
        .attr('class','material-icons button')
        .style('visibility','hidden')
        .style('background-color',hex)
        .text('close')
        .on('mouseover',function() { this.style.backgroundColor = rgba; })
        .on('mouseout',function() { this.style.backgroundColor = hex; })
        .on('click',function() { clearDtSiteSelection(g); });
      //attach collapse-view button
      d3.select('#g' + g + ' .site-labelholder').append('div')
        .attr('id','g' + g + '-collapsebutton')
        .style('background-color',hex)
        .style('height','24px')
        .style('width','24px')
        .style('margin-left','3px')
        .style('border-radius','5px')
        .style('display','inline-block')
        .style('cursor','pointer')
        .on('mouseover',function() {this.style.backgroundColor = rgba})
        .on('mouseout',function() {this.style.backgroundColor = hex})
        .on('click',function() {
          if ($('#g' + g + '-daterangepicker').css('display') == 'block') {
            $(this).addClass('closed');
            collapseDtGroup(g,hex,rgba);
          } else {
            $(this).removeClass('closed');
            expandDtGroup(g,hex,rgba);
          }
        })
        .append('i')
        .attr('class','material-icons button')
        .style('margin-left','0px')
        .style('visibility','hidden')
        .text('keyboard_arrow_down');
      //keep buttons hidden while icons render, reveal after split second
      d3.select('#g' + g + '-downloadbutton').transition().delay(100).style('visibility','visible');
      d3.select('#g' + g + '-collapsebutton i').transition().delay(100).style('visibility','visible');
      d3.select('#g' + g + '-clearbutton').transition().delay(100).style('visibility','visible');
      //establish counter for sub
      let s = dt_scounter;
      dt_scounter++;
      //push sub
      dt_activesubs.push({g:g,site_nm:gname,s:s,sub_nm:'Outdoor'});
      //begin spinner
      dt_spinner = new Spinner(spinopts).spin(document.getElementById('dt-statstable'));
      //load data, stats
      d3.select('#dt-selections').style('pointer-events','none');
      dt_datacounter = 1;
      initDtData('init',g,gname,s,'Outdoor');
      dt_statcounter = 1;
      initDtStats(g,gname,s,'Outdoor');
      //attach holder for daterangepicker
      d3.select('#g' + g).append('div')
        .attr('id','g' + g + '-daterangepicker')
        .attr('class',colors[g % colors.length].label)
        .style('display','block')
        .style('float','left')
        .style('width','100%')
        .style('margin-bottom','4px');
      //init daterangepicker
      dt_activextnts[g] = {
        start:new Date('2015-01-02'),
        end:new Date(),
      };
      initDtDateRangePicker(g,gname);
      //add empty first subgroup
      addEmptyDtSubgroup(g,gname);
    };
  };

  function addEmptyDtSubgroup(g,gname) {
    //remove existing subgroup selectors
    $('#g' + g + ' .subgroupholder:has(.menu)').remove();
    //generate list of rooms to include in selection list
    let site = sites.filter(function(d) { return d.site_nm_long == gname})[0];
    let srms = rooms.filter(function(d) { return d.site_id == site.site_id });
    if (srms.length > 0) {
      //establish subgroup position, and advance counter
      let s = dt_scounter;
      dt_scounter++;
      //draw selector holder div
      d3.select('#g' + g).append('div')
        .attr('id', 'sg' + s)
        .attr('class', 'subgroupholder')
        .html('<p class="menu" id="sg' + s + '-roommenu"><select></select></p>');
      //populate selection list
      let roommenu = d3.select('#sg' + s + ' select')
      let roomlist = getSingleColumn(dt_activesubs.filter(function(d) { return d.g == g }),'sub_nm');
      roommenu.selectAll('option')
        .data(d3.map(srms.filter(function(d) { return roomlist.indexOf(d.room_nm) == -1 }), function(d) { return d.room_nm }).keys())
        .enter().append('option')
        .text(function(d) { return d });
      //setup default blank first option
      roommenu._groups[0][0].innerHTML = '<option></option>' + roommenu._groups[0][0].innerHTML;
      //init select2
      $('#sg' + s + '-roommenu select').select2({
        placeholder : 'Select Room',
        dropdownAutoWidth : true,
        width: 288,
      }).on('change',function() {
        //on selection change...
        let hex = colors[g % colors.length].hex;
        let rgba = hex2rgba(hex,0.5);
        selectDtRoom(g,gname,s,roommenu.property('value'),hex,rgba);
      });
      $('#sg' + s +'-roommenu').on('select2-open',function() {
        d3.selectAll('.select2-drop').style('max-width','288x').style('width','288px');
      });
      if ($('#g' + g + '-collapsebutton').hasClass('closed')) {
        $('#g' + g + ' .menu').css('display','none');
      }
      if (dt_activesubs.filter(function(d) { return d.g == g }).length == 1) {
        d3.select('#g' + g + '-availablerangetext').style('display','none');
      }
    }
    else {
      d3.select('#g' + g).append('div')
        .attr('id','g' + g + '-nodatanotice')
        .style('margin-top','5px')
        .style('font-size','10px')
        .text('No classroom sensor data available.');
    }
    if ($('#g' + g + '-collapsebutton').hasClass('closed')) {
      $('#g' + g + ' .menu').css('display','none');
    }

    function selectDtRoom(g,gname,s,sname,hex,rgba) {
      //remove selector
      d3.select('#sg' + s + '-roommenu').remove();
      //draw selection
      d3.select('#sg' + s).append('div') //holder
        .attr('class','room-labelholder')
        .style('z-index','9999')
      d3.select('#sg' + s + ' .room-labelholder').append('span') //room name
        .attr('class','roomname')
        .style('color',hex)
        .style('border','1px solid ' + hex)
      d3.selectAll('#sg' + s + ' .roomname').append('span')
        .text(sname)
      d3.select('#sg' + s + ' .room-labelholder').append('i') //clear group button
        .attr('id','sg' + s + '-clearbutton')
        .attr('class','material-icons button')
        .style('color',hex)
        .style('border','1px solid ' + hex)
        .text('close')
        .on('mouseover',function() { this.style.backgroundColor = rgba; this.style.color = '#fff'; })
        .on('mouseout',function() { this.style.backgroundColor = '#fff'; this.style.color = hex; })
        .on('click',function() { 
          clearDtRoomSelection(g,gname,s);
        });
      //push sub
      dt_activesubs.push({g:g,site_nm:gname,s:s,sub_nm:sname});
      //begin spinner
      dt_spinner = new Spinner(spinopts).spin(document.getElementById('dt-statstable'));
      //init data, stats
      d3.select('#dt-selections').style('pointer-events','none');
      dt_datacounter = 1;
      initDtData('init',g,gname,s,sname);
      dt_statcounter = 1;
      initDtStats(g,gname,s,sname);
      //init daterangepicker
      let site = sites.filter(function(d) { return d.site_nm_long == dt_activesubs.filter(function(e) { return e.g == g })[0].site_nm })[0];
      let room = rooms.filter(function(d) { return d.site_id == site.site_id && d.room_nm == sname })[0];
      d3.csv('/qDataExtents/?siteid=' + site.site_id + '&roomid=' + room.room_id,function(data) {
        if (new Date(data[0].start) < dt_activextnts[g].start) { dt_activextnts[g].start = new Date(data[0].start)};
        if (new Date(data[0].end) > dt_activextnts[g].end) { dt_activextnts[g].end = new Date(data[0].end)};
        d3.select('#sg' + s + ' .roomname').append('span')
          .style('float','right')
          .style('font-size','10px')
          .style('margin','0px 10px 0px 0px')
          .text(formatDate(data[0].start) + ' to ' + formatDate(data[0].end))
        initDtDateRangePicker(g,gname);
      });
      //add new empty subgroup
      addEmptyDtSubgroup(g,gname);
    };

    function clearDtRoomSelection(g,gname,s) {
      //remove selector
      d3.select('#sg' + s).remove();
      //clear data objects
      dt_activedata = dt_activedata.filter(function(d) { return d.s !== s });
      dt_activestat = dt_activestat.filter(function(d) { return d.s !== s });
      dt_activesubs = dt_activesubs.filter(function(d) { return d.s !== s });
      //redraw options in empty group
      d3.selectAll('#g' + g + ' select2-container').remove();
      addEmptyDtSubgroup(g,gname);
      //remove line path
      d3.selectAll('path.line.sg' + s)
        .transition().duration(500)
        .style('opacity','0.001')
        .transition().delay(400).duration(100)
        .remove();
      //remove line label
      d3.selectAll('text.dt_pathlabel.sg' + s) 
        .transition().delay(0).duration(500)
        .style('opacity','0.001')
        .transition().delay(400).duration(100)
        .remove();
      //redraw vizualization
      drawDtLines('redraw');
      //redraw statstable
      initDtTable('init');
    };
  };

  function initDtDateRangePicker(g,gname) {
    //destroy daterange picker, if exists
    if ($('#g' + g + '-daterangepicker').data('dateRangePicker')) {
      $('#g' + g + '-daterangepicker').unbind('datepicker-change');
      $('#g' + g + '-daterangepicker').data('dateRangePicker').destroy();
      $('#g' + g + '-daterangepicker').html('');
    }
    $('#g' + g + '-daterangepicker')
      .dateRangePicker({
        inline:true,
        container: '#g' + g + '-daterangepicker', 
        alwaysOpen:true,
        singleMonth:true,
        startDate:typeof dt_activextnts[g] == 'undefined'? new Date(): new Date(dt_activextnts[g].start),
        endDate:typeof dt_activextnts[g] == 'undefined'? new Date(): new Date(dt_activextnts[g].end)});
    //set inital date range for picker
    $('#g' + g + '-daterangepicker')
      .data('dateRangePicker')
      .setDateRange(dt_activespans[g].start,dt_activespans[g].end);
    //draw available dates text
    d3.selectAll('.date-picker-wrapper').style('padding','5px 0px 5px 0px');
    d3.select('#g' + g + '-daterangepicker').append('div')
      .attr('id','g' + g + '-availablerangetext')
      .style('float','left')
      .style('position','relative').style('top','4px')
      .style('margin-left','171px')
      .style('font-size','10px')
      .style('font-weight',200)
      .style('letter-spacing','1px')
      .style('color','#a9a9a9')
      .text('Available Dates');
    if (dt_activesubs.filter(function(d) { return d.g == g }).length == 1) {
      d3.select('#g' + g + '-availablerangetext').style('display','none');
    }
    //configure change event
    $('#g' + g + '-daterangepicker')
      .bind('datepicker-change',function(event,obj) {
        //update active start/end object
        dt_activespans[g].start = new Date(obj.date1.setHours(0,0,0,0))
        dt_activespans[g].end = new Date(obj.date2.setHours(23,59,0,0))
        //reveal movement to other months
        d3.selectAll('#g' + g + '-daterangepicker .next').style('visibility',null);
        d3.selectAll('#g' + g + '-daterangepicker .prev').style('visibility',null);
        //clear data, stats
        dt_activedata = dt_activedata.filter(function(d) {return d.g != g });
        dt_activestat = dt_activestat.filter(function(d) {return d.g != g });
        //reset stats table
        initDtTable('blank',dt_activesubs.length);
        //reload data
        let affected = dt_activesubs.filter(function(d) { return d.g == g });
        dt_datacounter = affected.length;
        dt_statcounter = affected.length;
        //begin spinner
        dt_spinner = new Spinner(spinopts).spin(document.getElementById('dt-statstable'));
        affected.forEach(function(d) {
          initDtData('redraw',d.g,d.site_nm,d.s,d.sub_nm);
          initDtStats(d.g,d.site_nm,d.s,d.sub_nm);
        });
      });
    //if group is collapsed, re-collapse with updated drp
    if ($('#g' + g + '-collapsebutton').hasClass('closed')) {
      let hex = colors[g % colors.length].hex
      let rgba = hex2rgba(hex,0.5);
      collapseDtGroup(g,hex,rgba);
    }
  };

  function initDtData(phase,g,gname,s,sname) {
    let site = sites.filter(function(d) { return d.site_nm_long == dt_activesubs.filter(function(e) { return e.g == g })[0].site_nm })[0];
    d3.csv('/qSpanProfileData/?siteid=' + site.site_id + '&roomnm=' + sname + '&start=' + formatDate(dt_activespans[g].start) + '&end=' + formatDate(dt_activespans[g].end),function(data) {
      data.forEach(function(d) {
        let row = {
          g:g,
          site_nm:gname,
          s:s,
          sub_nm:sname,
          time_str:d.time_str,
          time:d.time,
          temp_f:+d.temp_f,
        };
        dt_activedata.push(row);
      });
      dt_datacounter--;
      if (dt_datacounter == 0) {
        dt_activedata.sort(function(a,b) { return d3.ascending(a.time,b.time) });
        drawDtLines(phase);
        if (dt_statcounter == 0) {
          //re-enable pointer-events
          $('#dt-selections').css('pointer-events','auto');
        }
      }
    });
  };

  function initDtStats(g,gname,s,sname) {
    let site = sites.filter(function(d) { return d.site_nm_long == dt_activesubs.filter(function(e) { return e.g == g })[0].site_nm })[0];
    d3.csv('/qSpanProfileStats/?siteid=' + site.site_id + '&roomnm=' + sname + '&start=' + formatDate(dt_activespans[g].start) + '&end=' + formatDate(dt_activespans[g].end) + '&schlhrs=' + dt_statfilt.sh + '&schldays=' + dt_statfilt.sd,function(data) {
      dt_activestat.push({
        g:g,
        site_nm:gname,
        s:s,
        sub_nm:sname,
        compl:+d3.format('.4f')(data[0].compl),
        temp_min:+data[0].temp_min,
        temp_avg:+data[0].temp_avg,
        temp_max:+data[0].temp_max,
        temp_n:+data[0].temp_n,
        temp_o85:+data[0].temp_o85,
        temp_o90:+data[0].temp_o90,
      });
      dt_statcounter--;
      if (dt_statcounter == 0) {
        //stop spinner
        dt_spinner.stop();
        initDtTable('init');
        if (dt_datacounter == 0) {
          //re-enable pointer-events
          $('#dt-selections').css('pointer-events','auto');
        }
      }
    });
  };

  function downloadDtCsv(g,gname) {
    let site = sites.filter(function(d) { return d.site_nm_long == gname})[0];
    let srms = rooms.filter(function(d) { return d.site_id == site.site_id }).filter(function(d) { return getSingleColumn(dt_activesubs.filter(function(e) { return e.g == g }),'sub_nm').indexOf(d.room_nm) !== -1 });
    let csvdata = [];
    showLoading();
    d3.csv('/qGroupFile/?siteid=' + site.site_id + '&srmids=' + [...new Set(srms.map(rm => rm.room_id))].join('|') + '&start=' + formatDate(dt_activespans[g].start) + '&end=' + formatDate(dt_activespans[g].end),function(data) { 
      data.forEach(function(d) {
        //format successfully loaded data
        let row = {
          Site_Selected:gname,
          Monitoring_Site:d.site == 'X'? gname: d.site,
          Dist_to_Site:+d.dist,
          Location:d.location,
          Timestamp:d.timestamp,
          Temperature:+d.temp < 0? '-': +d.temp,
          Dew_Point:+d.dewpt < 0? '-': +d.dewpt,
          Rel_Humidity:+d.relh < 0? '-': +d.relh,
          Wind_Speed:+d.wspd < 0? '-': +d.wspd,
          Gust_Speed:+d.gspd < 0? '-': +d.gspd,
          Wind_Direction:+d.wdir < 0? '-': +d.wdir,
          Rain_Rate:+d.rain < 0? '-': +d.rain,
          Solar_Radiation:+d.solr < 0? '-': +d.solr
        };
        csvdata.push(row);
      });
      csvdata.sort(function(a,b) { return d3.ascending(a.Timestamp, b.Timestamp); });
      let meta = ( 
        'data provided by MKThink + RoundhouseOne,' +
        'data sourced from Proprietary Sensors and/or Wunderground Community Stations,' +
        'selected school: ' + gname + ',' +
        'selected location(s): ' + [...new Set(srms.map(rm => rm.room_id))].join(' | ') + ',' +
        'selected date range: ' + formatDate(dt_activespans[g].start) + ' to ' + formatDate(dt_activespans[g].end) + ',' +
        'file exported on: ' + formatDateTime(new Date()) + ',').split(',');
      let filenm = site.site_nm_shrt.split(' ').join('') + '_' + formatDateMatch(dt_activespans[g].start) + '-' + formatDateMatch(dt_activespans[g].end) + '.csv';
      let fields = (Object.keys(csvdata[0]));
      let units = ['','','','','(HST)','(degf)','(degf)','(pct)','(mph)','(mph)','(deg)','(in/hr)','(w/m^2)'];
      downloadDtFile(csvdata,meta,fields,units,filenm);
    });

    function showLoading() {
      let siteobj = d3.select('#g' + g + ' .sitename')
      if (siteobj.html().length - site.site_nm_long.length > 8) { siteobj.text(site.site_nm_long + ' '); }
      else { siteobj.text(siteobj.html() + '.'); }
      if (csvdata.length == 0) {
        setTimeout(function() {
          showLoading();
        },100);
      } else {
        siteobj.text(site.site_nm_long);
        d3.selectAll('#g' + g + ' .sitename,#g' + g + '-clearbutton,#g' + g + '-collapsebutton')
          .transition().delay(0).duration(500)
          .style('background-color','#fbb157')
          .transition().delay(0).duration(1500)
          .style('background-color',colors[g % colors.length].hex);
      }
    };

    function downloadDtFile(csvdata,meta,fields,units,filenm) {
      let replacer = function(key,value) { return value === null ? '' : value }
      //structure csv content
      let csv = csvdata.map(function(row) { 
        let line =  fields.map(function(field) { 
          return JSON.stringify(row[field],replacer);
        }).join(',');
        return  '\r\n' + line;
      });
      let csv3 = [meta.join('\r\n'),fields,units,csv].join('\r\n');
      let file = new Blob([csv3], {type:'application/csv;charset=utf-8;'});
      //download file
      if (navigator.msSaveBlob) { //iE10
        navigator.msSaveBlob(file,filenm);
      } else { //other browser
        let a = document.createElement('a');
        let url = URL.createObjectURL(file);
        a.href = url;
        a.style = 'visibility:hidden';
        a.download = filenm;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
          document.body.removeChild(a);
        },3000);
      }
    };
  };

  function drawDtLines(phase) {
    //shared transition for updates
    let t = d3.transition().duration(750);
    //update ymax, ymin if necessary
    if (dt_axislock.ymin == '' && dt_axislock.ymax == '') {
      if (dt_axisrnge.ymin == '' || dt_axisrnge.ymax == '' || dt_activedata.length == 0) { 
        dt_axisrnge.ymin = 60; 
        dt_axisrnge.ymax = 80;
      } else {
        //re-assess ymin/ymax values
        dt_axisrnge.ymin = d3.min(dt_activedata,function(d) { return d.temp_f });
        dt_axisrnge.ymax = d3.max(dt_activedata,function(d) { return d.temp_f });
      }
    } else {
      dt_axisrnge.ymin = dt_axislock.ymin; 
      dt_axisrnge.ymax = dt_axislock.ymax;
    }
    //adjust scales
    dt_yScale.domain([Math.floor(dt_axisrnge.ymin / 2) * 2,Math.ceil(dt_axisrnge.ymax / 2) * 2]).range([dt_svgh - dt_svgm.top,dt_svgm.bottom]);
    dt_yAxis.scale(dt_yScale).tickSize(0).tickFormat(function(a) {return a + '' });
    dt_yGrid.scale(dt_yScale).tickSize(-dt_svgw + dt_svgm.left + dt_svgm.right).tickFormat('');
    dt_chart.selectAll('.y.axis')
      .transition(t).call(dt_yAxis)
      .selectAll('.tick text, .tick line, .domain')
      .attr('transform','translate(' + dt_svgm.left + ',0)');
    dt_chart.selectAll('.y.grid')
      .transition(t).call(dt_yGrid)
      .selectAll('.tick text, .tick line, .domain')
      .attr('transform','translate(' + dt_svgm.left + ',0)');
    //nest active data
    let linedata = d3.nest()
      .key(function(d) { return d.s + '_' + d.g + '_' + d.sub_nm })
      .entries(dt_activedata);
    //reset
    //dt_linesGroup.selectAll('path').remove();
    let lines = dt_linesGroup.selectAll('path')
      .data(linedata,dt_key);
    dt_voronoiGroup.selectAll('path').remove();
    let voronois = dt_voronoiGroup.selectAll('path')
      .data(dt_voronoiFunction.polygons(d3.merge(linedata.map(function(d) { return d.values }))));
    //enter
    let newlines = lines.enter()
      .append('path')
      .style('opacity','0.001')
      .attr('class',function(d) { return 'line g' + d.key.split('_')[1] + ' sg' +  d.key.split('_')[0] })
      .attr('d',function(d) { return dt_lineFunction(d.values) })
      .style('stroke',function(d) { return colors[d.key.split('_')[1]*1 % colors.length].hex })
      .style('stroke-width','2px')
      .style('stroke-dasharray',function(d) { return d.key.split('_')[2] == 'Outdoor'?'5,5': null })
      .transition().duration(function(d) { return phase == 'redraw'? 0: 1500 })
      .style('opacity','0.9');
    voronois.enter()
      .append('path')
      .attr('d',function(d) { return d? 'M' + d.join('L') + 'Z': null })
      .on('mouseover',mouseoverDtLine)
      .on('mouseout',mouseoutDtLine);
    //merge lines
    lines.merge(newlines)
      .transition().duration(1500)
      .attr('d',function(d) { return dt_lineFunction(d.values) })
      .style('opacity', null);
    //remove old lines
    d3.selectAll('.line')._groups[0].forEach(function(d) {
      let g_index = d3.select(d).attr('class').split(' ')[1].replace('g','')
      let s_index = d3.select(d).attr('class').split(' ')[2].replace('sg','')
      if (dt_activedata.filter(function(e) { return e.g == g_index * 1 && e.s == s_index * 1 }).length == 0) {
        d3.selectAll('.line.g' + g_index + '.sg' + s_index)
          .transition().duration(1000)
          .style('opacity','0.001')
          .remove()
      }
    })
    //annotate paths
    d3.selectAll('.dt_pathlabel').remove();
    linedata.forEach(function(d) {
      if (d3.selectAll('.s' + d.key.split('_')[0] + '.dt_pathlabel')._groups[0].length > 0) {
        d3.selectAll('.s' + d.key.split('_')[0] + '.dt_pathlabel')
          .transition().duration(1500)
          .attr('transform','translate(' + (dt_xScale(new Date(d.values[d.values.length - 1].time)) + 5) + ',' + (dt_yScale(d.values[d.values.length - 1].temp_f) + dt_svgm.top + 3) + ')')
          .attr('class','s' + d.key.split('_')[0] + ' dt_pathlabel')
          .text(d.key.split('_')[2])
          .style('font-size','9px')
          .style('fill',function() { return colors[d.key.split('_')[1] % colors.length].hex })
          .style('opacity','0.75')
      } else {
        dt_chart.append('text')
          .attr('transform','translate(' + (dt_xScale(new Date(d.values[d.values.length - 1].time)) + 5) + ',' + (dt_yScale(d.values[d.values.length - 1].temp_f) + dt_svgm.top + 3) + ')')
          .attr('class','s' + d.key.split('_')[0] + ' dt_pathlabel')
          .text(d.key.split('_')[2])
          .style('font-size','9px')
          .style('fill',function() { return colors[d.key.split('_')[1] % colors.length].hex })
          .style('opacity',0)
          .transition(0).duration(function(d) { return phase == 'redraw'? 0: 1500 })
          .style('opacity','0.75')
      } 
    });
    
    function mouseoverDtLine(d) {
      //highlight selectedline
      d3.selectAll('.line').style('opacity',0.25);
      d3.selectAll('.line.sg' + d.data.s)
        .classed('line-hover',true)
        .style('opacity',1.0)
      d3.selectAll('.dt_pathlabel').style('opacity',0.25);
      if (new Date(d.data.time).getHours() < 23) {
        d3.selectAll('.s' + d.data.s + '.dt_pathlabel').style('opacity',1.0);
      }
      //hide x-axis ticks
      d3.select('.x.axis').selectAll('.tick').attr('display','none');
      //adjust focus
      dt_focus
        .attr('transform','translate(' + dt_xScale(new Date(d.data.time)) + ',' + dt_svgm.top + ')');
      dt_focus.select('.focus-label')
        .text(d3.format('.2f')(d.data.temp_f) + '°F')
        .attr('class','focus-label')
        .style('fill',colors[d.data.g % colors.length].hex)
        .attr('y',function() { return dt_yScale(d.data.temp_f) });
      dt_focus.select('.focus-line')
        .attr('y1',function() { return dt_yScale(d.data.temp_f) })
        .attr('y2',dt_svgh + dt_svgm.top);
      dt_focus.select('.focus-time')
        .attr('y',dt_svgh + dt_svgm.top + 10)
        .text(formatTime(new Date(d.data.time)))
        .style('font-size','9.5px');
      //highlight table contents
      d3.selectAll('tr.dt .s' + d.data.s)
        .transition().delay(0).duration(100)
        .style('background-color',colors[d.data.g % colors.length].hex)
        .style('color','#ffffff');
      d3.selectAll('tr.dt .s' + d.data.s)
        .transition().delay(0).duration(100)
        .style('background-color',colors[d.data.g % colors.length].hex)
        .style('color','#ffffff');
      d3.selectAll('tr.dt .s' + d.data.s + ' span')
        .transition().delay(0).duration(100)
        .style('background-color','#ffffff');
    };

    function mouseoutDtLine(d) {
      //un-highlight selected line
      d3.selectAll('.line').style('opacity',null);
      d3.selectAll('.dt_pathlabel').style('opacity',null)
      d3.selectAll('.line.sg' + d.data.s)
        .classed('line-hover',false)
        .style('opacity',null);
      //reveal x-axis ticks
      d3.select('.x.axis').selectAll('.tick').attr('display',null);
      //hide focus
      dt_focus.attr('transform','translate(-300,-300)');
      //un-highlight table contents
      d3.selectAll('tr.dt .s' + d.data.s)
        .transition().delay(0).duration(100)
        .style('background-color',null)
        .style('color',null);
      d3.selectAll('tr.dt .s' + d.data.s + ' span')
        .transition().delay(0).duration(100)
        .style('background-color',colors[d.data.g % colors.length].hex);
    };
  };

  function initDtTable(phase,n) {
    //clear existing table elements
    d3.selectAll('#dt-statstable table,#dt-statstable div,#dt-statstable text').remove();
    //draw heading
    let head = d3.select('#dt-statstable').append('div')
      .style('margin','0px 0px 4px 21px')
      .style('font-weight','200')
    head.append('span')
      .style('float','left')
      .style('width','178px')
      .style('font-size','13px')
      .text('STATISTICAL SUMMARY');
    //configure stat filters
    head.append('div')
      .style('float','left')
      .style('width','220px')
      .style('letter-spacing','1px')
      .style('font-size','11px')
      .html('<input type="checkbox" id="sh_cb"><label for="sh_cb">School-Hours Only (8am to 2pm)</label>');
    if (dt_statfilt.sh == 'yes') { $('#sh_cb').prop('checked',true); }
    $('#sh_cb').on('click',function() {
      if ($('#sh_cb').prop('checked') == true) { dt_statfilt.sh = 'yes'; }
      else { dt_statfilt.sh = 'no'; }
      //clear existing table
      initDtTable('blank',dt_activesubs.length);
      //begin spinner
      dt_spinner = new Spinner(spinopts).spin(document.getElementById('dt-statstable'));
      //re-init stats
      dt_activestat = [];
      dt_statcounter = dt_activesubs.length;
      dt_activesubs.forEach(function(d) {
        initDtStats(d.g,d.site_nm,d.s,d.sub_nm);
      });
    })
    head.append('div')
      .style('float','left')
      .style('width','220px')
      .style('letter-spacing','1px')
      .style('font-size','11px')
      .html('<input type="checkbox" id="wd_cb"><label for="wd_cb">Weekdays Only</label>');
    if (dt_statfilt.sd == 'yes') { $('#wd_cb').prop('checked',true); }
    //add listener for click
    $('#wd_cb').on('click',function() {
      //update settings var
      if ($('#wd_cb').prop('checked') == true) { dt_statfilt.sd = 'yes'; }
      else { dt_statfilt.sd = 'no'; }
      //clear existing table
      initDtTable('blank',dt_activesubs.length);
      //begin spinner
      dt_spinner = new Spinner(spinopts).spin(document.getElementById('dt-statstable'));
      //re-init stats
      dt_activestat = [];
      dt_statcounter = dt_activesubs.length;
      dt_activesubs.forEach(function(d) {
        initDtStats(d.g,d.site_nm,d.s,d.sub_nm);
      });
    });
    //configure table contents   
    let contents = [{label:'SCHOOL:'},{label:'SENSOR LOCATION:'},{label:'DATE RANGE:'},{label:'DATA COMPLETE:'},{label:'MIN TEMPERATURE (°F):'},{label:'AVG TEMPERATURE (°F):'},{label:'AVG INDOOR-OUTDOOR ∆ (°F):'},{label:'MAX TEMPERATURE (°F):'},{label:'% OF TIME OVER 85°F:'},{label:'% OF TIME OVER 90°F:'}];
    if (phase == 'blank') {
      for (let i = 0; i < n; i++) {
        contents.forEach(function(d,j) {
          d['value' + i] = ' ';
          if (j <= 1) { d['class' + i] = 'sX'; }
        });
      }
    } else if (dt_activestat.length == 0) {
      contents.forEach(function(d,j) {
        d['value0'] = ' ';
        if (j <= 1) { d['class0'] = 'sX'; }
      });
    } else {
      dt_activestat.forEach(function(d,i) {
        let hex = colors[d.g % colors.length].hex;
        //configure legend
        let f1,f3,f6 = ['','','']
        if (d.sub_nm == 'Outdoor') {
          f1 = 
            '<span style="width:3px; height:3px; margin:0px 2px 0px 0px; position:relative; top:-2px; display:inline-block; background-color:' + hex + ';"></span>' +
            '<span style="width:3px; height:3px; margin:0px 2px 0px 0px; position:relative; top:-2px; display:inline-block; background-color:' + hex + ';"></span>' +
            '<span style="width:3px; height:3px; margin:0px 2px 0px 0px; position:relative; top:-2px; display:inline-block; background-color:' + hex + ';"></span>' +
            '<span style="width:3px; height:3px; margin:0px 3px 0px 0px; position:relative; top:-2px; display:inline-block; background-color:' + hex + ';"></span><b>' + d.sub_nm + '</b></span><div style="display:inline-block;font-size:15px;line-height:9px;background-color:rgba(255,255,255,0);"></div>';
          f6 = 'n/a'
        } else {
          f1 = 
            '<span style="width:15px; height:3px; margin: 0px 3px 0px 0px; position:relative; top:-2px; display:inline-block; background-color:' + hex + ';"></span><b>' + d.sub_nm + '</b>';
          f6 = d3.format('.2f')(d.temp_avg - dt_activestat.filter(function(e) { return e.g == d.g && e.sub_nm == 'Outdoor' })[0].temp_avg);
        }
        //configure data completion
        if (d.compl > 99) { f3 = '<span>' + dec2pct(d.compl) + '</span><span style="margin-left:4px;color:#228b22;"><i>Excellent</i></span>' }
        else if (d.compl > 90) { f3 = '<span>' + dec2pct(d.compl) + '</span><span style="margin-left:4px;color:#9acd32;"><i>Acceptable</i></span>' }
        else if (d.compl > 0) { f3 = '<span>' + dec2pct(d.compl) + '</span><span style="margin-left:4px;color:#ff0000;"><i>Significant Data Missing</i></span>' }
        else { f3 = '<span>' + dec2pct(d.compl) + '</span><span style="margin-left:4px;color:#ff0000;"><i>No Data</i></span>' }
        //append to 'contents' object
        contents[0]['value' + i] = d.site_nm;
        contents[0]['class' + i] = 's' + d.s;
        contents[1]['value' + i] = f1;
        contents[1]['class' + i] = 's' + d.s;
        contents[2]['value' + i] = '<span style="letter-spacing:0px;">' + formatDate(dt_activespans[d.g].start) + ' to ' + formatDate(dt_activespans[d.g].end) + '</span>';
        contents[3]['value' + i] = f3;
        contents[4]['value' + i] = d.temp_n == 0? '-': d3.format('.2f')(d.temp_min);
        contents[5]['value' + i] = d.temp_n == 0? '-': d3.format('.2f')(d.temp_avg);
        contents[6]['value' + i] = d.temp_n == 0? '-': f6;
        contents[7]['value' + i] = d.temp_n == 0? '-': d3.format('.2f')(d.temp_max);
        contents[8]['value' + i] = d.temp_n == 0? '-': dec2pct(d.temp_o85/d.temp_n);
        contents[9]['value' + i] = d.temp_n == 0? '-': dec2pct(d.temp_o90/d.temp_n);
      });
    }
    //draw statstable
    let table = d3.select('#dt-statstable')
      .append('table')
      .style('margin-left','18px')
      .style('width','calc(100% - 42px');
    let tr = table.selectAll('tr')
      .data(contents).enter()
      .append('tr')
      .attr('class','dt');
    tr.append('td').html(function(row) { return row.label })
      .style('width','170px');
    for (i = 0; i < Object.keys(contents[2]).length - 1; i++) {
      tr.append('td')
        .html(function(row) {
          return row['value' + i] 
        })
        .attr('class',function(row,j) {
          if (j <= 1) {
            return row['class' + i]
          }
        });

    }
  };

  function printDtScreencap() {
    //standardize element widths/height
    $('#content-wrapper').width(1430);
    $('#content-wrapper').height(850);
    //redraw elements
    d3.selectAll('#dt-viz svg').remove();
    d3.selectAll('#dt-viz div').remove();
    initDtViz('redraw');
    setTimeout(function() {
      //remove buttons
      d3.selectAll('#dt-screencapbutton,#dt-lockybutton').remove();
      //map chart's computedstyles to elements directly
      d3.selectAll('#dt-viz path, #dt-viz line, #dt-viz g, #dt-viz text').each(function() {
          var element = this;
          var computedStyle = getComputedStyle(element, null);
          for (var i = 0; i < computedStyle.length; i++) {
            var property = computedStyle.item(i);
            var value = computedStyle.getPropertyValue(property);
            element.style[property] = value;
          }
        });
      //change page title
      document.title = 'datatrends-' + formatDateTimeMatchWithSeconds(new Date());
      //hide extraneous page elements
      d3.select('#dt-panel').style('display','none');
      d3.select('#header').style('display','none');
      d3.select('#nav-controls').style('display','none');
      window.print();
      setTimeout(function(){
         //change page title back
         document.title = "Thermal Comfort"
         //reveal other elements
         d3.select('#dt-panel').style('display','inline-block');
         d3.select('#header').style('display','block');
         d3.select('#nav-controls').style('display','block');
         //revert content-wrapper dimensions
         resizeContentWrapper();
         //redraw viz w/ original classes/sizes
         d3.selectAll('#dt-viz svg').remove();
         d3.selectAll('#dt-viz div').remove();
         initDtViz('redraw');
      },1000);
    },250);
  };

  /*helper functions*/
  function getSingleColumn(matrix,col){
    let column = [];
    for (let i = 0; i < matrix.length; i++){
      column.push(matrix[i][col]);
    }
    return column;
  };

  /*resizing listener*/
  $(window).on('resize',function() {
    d3.selectAll('#dt-viz svg').remove();
    d3.selectAll('#dt-viz div').remove();
    d3.selectAll('#dt-screencapbutton').remove();
    d3.selectAll('#dt-lockybutton').remove();
    initDtViz('redraw');
  });

  /*header page nav listeners*/
  $('#home-nav').on('click',navToHome);
  function navToHome() {
    $state.go('home',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        // rwws: $stateParams.schools.rwws,
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#schools-nav').on('click',navToSchools);
  function navToSchools() {
    $state.go('schools',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        // rwws: $stateParams.schools.rwws,
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#keepcool-nav').on('click',navToKeepCool);
  function navToKeepCool() {
    $state.go('keepcool',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        // rwws: $stateParams.schools.rwws,
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
});