app.directive('schoolsiteselection', function() {
  return {
    template: [
    
      '<div id="schools-siteselection">',
        '<span style="width:190px; float:left; font-size:16px; margin:2px 0px 0px 0px;">FIND YOUR SCHOOL:</span>',
        '<p id="schools-sitemenu"><select></select></p>',
      '</div>',
      
    ].join(''),
    scope: {},
    controller: 'schoolsiteselectionCtrl',
  }
})
app.controller('schoolsiteselectionCtrl', function($rootScope, $scope, $state, $stateParams, siteList) {

  /*check content-wrapper height*/
  resizeContentWrapper();
  /*update nav-bar styling*/
  d3.selectAll(".nav-button").classed("active-nav", false);
  d3.selectAll(".nav-button").classed("alt-nav", true);
  d3.select("#schools-nav").classed("alt-nav", false);
  d3.select("#schools-nav").classed("active-nav", true);

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      if (sitelist && sitelist.length) {
        //init sequence
        sites = sitelist;
        sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
        if ($stateParams.siteid != '') {
          let activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
          initSiteSelector(activesite);
        } else {
          initSiteSelector('');
        }
      }
      else {
        //timeout re-call
        var retry = function() {
          setTimeout(function() {
            loadReqs();
          },100);
        }
        retry();
      }
    });
  };

  function initSiteSelector(activesite) {
    //initialize selection list
    const sitemenu = d3.select("#schools-sitemenu select");
    sitemenu._groups[0][0].innerHTML = '<option></option>' + sitemenu._groups[0][0].innerHTML;
    $('#schools-sitemenu select').select2({
      placeholder: $stateParams.siteid == ''?'Select Site':sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0].site_nm_long, 
      dropdownAutoWidth:true,
      width:'100%',
    }).on('change',changeSite);
    $('#home-sitemenu').on('select2-open',function() {
      d3.selectAll('.select2-drop')
        .style('max-width',$('#schools-sitemenu').width() + 'px')
        .style('width',$('#schools-sitemenu').width() + 'px');
    });
    //populate selection list
    sitemenu.selectAll('option')
      .data(d3.map(sites.filter(function(d) { return d.site_id != $stateParams.siteid }), function (d) { return d.site_nm_long }).keys())
      .enter().append('option')
      .text(function(d) { return d });

    function changeSite() {
      //clear curr conditions update loop
      if (typeof cwint !== 'undefined') { window.clearInterval(cwint) }
      //init state change
      const site = sites.filter(function(d) { return d.site_nm_long == sitemenu.property('value') })[0];
      navToProfile(site.site_id);
    };
  };

  //NAV FUNCS
  $('#home-navdtbutton').on('click',navToDataTrends);
  $('#home-navkcbutton').on('click',navToKeepCool);

  //go button listener
  function navToProfile(id) {
    $state.go('schools',{ 
      siteid: id,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        // rwws: $stateParams.schools.rwws,
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };

  //header page nav listeners
  $('#home-nav').on('click',navToHome);
  function navToHome() {
    $state.go('home',{
      siteid: '',
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#datatrends-nav').on('click',navToDataTrends);
  function navToDataTrends() {
    $state.go('datatrends',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#keepcool-nav').on('click',navToKeepCool);
  function navToKeepCool() {
    $state.go('keepcool',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
});


