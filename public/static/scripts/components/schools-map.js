app.directive('schoolmap', function() {
  return {
    template: [

      '<div id="schools-map" class="holder right"></div>',

    ].join(''),
    scope: {},
    controller: 'schoolmapCtrl',
  }
})
app.controller('schoolmapCtrl', function($rootScope, $scope, $state, $stateParams, siteList) {
  
  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      if (sitelist && sitelist.length) {
        //init sequence
        sites = sitelist;
        sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
        if ($stateParams.siteid != '') {
          let activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
          initMap(activesite);
        } else {
          initMap('');
        }
      }
      else {
        //timeout re-call
        var retry = function() {
          setTimeout(function() {
            loadReqs();
          },100);
        }
        retry();
      }
    });
  };

  function initMap (activesite) {
    //load style
    d3.json('./static/scripts/components/resources/gmap.json',function(gmapstyle) {
      //init gmap object
      map = new google.maps.Map(document.getElementById('schools-map'),{
        center: activesite == ''? new google.maps.LatLng(20.613834,-157.473497): new google.maps.LatLng(activesite.lat,activesite.lng),
        zoom:activesite == ''? 7: 17,
        gestureHandling: 'greedy',
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        options: {
          streetViewControl:false,
          mapTypeControl:false,
          fullscreenControl:false,
        },
        styles:gmapstyle,
      });
      //draw map objects
      if (activesite !== '') {
        var marker = new google.maps.Marker({
          position:new google.maps.LatLng(activesite.lat, activesite.lng),
          map:map,
        });
      }
    });
  };
});