app.directive('schooloverview', function() {
  return {
    template: [

      '<div id="schools-overview" class="holder left">',
        '<div class="s-title">SCHOOL OVERVIEW</div>',
      '</div>',
      
    ].join(''),
    scope: {},
    controller: 'schooloverviewCtrl',
  }
})
app.controller('schooloverviewCtrl', function($rootScope, $scope, $state, $stateParams, siteList) {

  /*load required data, init page content*/
  loadReqs();
  function loadReqs() {
    siteList.getSites(function(sitelist) {
      if (sitelist && sitelist.length) {
        //init sequence
        sites = sitelist;
        sites.sort(function(a,b) { return d3.ascending(a.site_nm_long,b.site_nm_long) });
        if ($stateParams.siteid != '') {
          let activesite = sites.filter(function(d) { return d.site_id == $stateParams.siteid })[0];
          initOverview(activesite);
        } else {
          initOverview('');
        } 
      }
      else {
        //timeout re-call
        var retry = function() {
          setTimeout(function() {
            loadReqs();
          },100);
        }
        retry();
      }
    });
  };

  function initOverview(activesite) {
    //create table element
    var table = d3.select('#schools-overview').append('table');
    if (activesite != '') { 
      var inputs = [
        {'cat':'DISTRICT','value':'<b>' + activesite.district + '</b>'},
        {'cat':'COMPLEX','value':'<b>' + activesite.complex + '</b>'},
        {'cat':'CLIMATE TYPE','value':'<b>' + activesite.climate + '</b>'},
        {'cat':'SITE ACREAGE','value':'<b>-</b>'},
        {'cat':'# OF STRUCTURES','value':'<b>-</b>'},
        {'cat':'# OF PORTABLES','value':'<b>-</b>'},
        {'cat':'TOTAL GROSS SF','value':'<b>-</b>'},
        {'cat':'BUILDING TYPOLOGIES','value':'<b>-</b>'},
        {'cat':'LAND COVERAGE','value':'<b>-</b>'},
        {'cat':'ROOF COLOR(S)','value':'<b>-</b>'}]
    } else {
      var inputs = [
        {'cat':'DISTRICT','value':'<b>-</b>'},
        {'cat':'COMPLEX','value':'<b>-</b>'},
        {'cat':'CLIMATE TYPE','value':'<b>-</b>'},
        {'cat':'SITE ACREAGE','value':'<b>-</b>'},
        {'cat':'# OF STRUCTURES','value':'<b>-</b>'},
        {'cat':'# OF PORTABLES','value':'<b>-</b>'},
        {'cat':'TOTAL GROSS SF','value':'<b>-</b>'},
        {'cat':'BUILDING TYPOLOGIES','value':'<b>-</b>'},
        {'cat':'LAND COVERAGE','value':'<b>-</b>'},
        {'cat':'ROOF COLOR(S)','value':'<b>-</b>'}]
    }
    var tr = table.selectAll('tr')
        .data(inputs)
        .enter()
      .append('tr');
    tr.append('td').html(function(inputs) { return inputs.cat });
    tr.append('td').html(function(inputs) { return inputs.value });
  }
});