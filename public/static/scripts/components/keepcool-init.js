app.directive('keepcool', function() {
  return {
    template: [

      //modal for selected content
      '<div id="kc-modal" style="display:none;"></div>',

      //keepcool intro
      '<div id="kc-intro"></div>',

      //ways to keep cool!
      '<div id="kc-ways"></div>',

      //frequently asked questions
      '<div id="kc-faqs">',
        '<div id="kc-faqs-t"></div>', //TITLE

        '<br>',

        '<div class="q">What is the Thermal Comfort portal?</div>', //Q1
        '<div class="a">The Thermal Comfort portal is a tool for school communities to access information on the area temperature and environmental conditions that is used to make decisions related to heat abatement.</div>', //A1

        '<br>',

        '<div class="q">How and when did this project start?</div>', //Q2
        '<div class="a">In 2013 the Hawaiʻi State Department of Education (HIDOE) and the Hawaii Natural Energy Institute (HNEI) partnered to collect environmental data on public school campuses. The information is used to guide heat abatement projects to improve the learning environment for students. This website was built by the data analytics firm RoundhouseOne in collaboration with MKThink.</div>', //A2

        '<br>',

        '<div class="q">How does this data help the Hawaiʻi State Department of Education and the community?</div>', //Q3
        '<div class="a">HIDOE uses this information in several ways:</div>', //A3
          '<br>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Access to local weather data can help schools with short-term decisions related to weather.</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• A combination of outdoor and indoor temperature readings help HIDOE identify areas that have consistent heat issues for targeted thermal comfort improvements.</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Detailed information from classrooms helps architects and planners design heat abatement solutions to address the specific issues for particular buildings.</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Teachers and students may use the data for class projects.</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Community members looking for information on local micro-climates can access and study the data collected at school campuses.</div>',

        '<br>',

        '<div class="q">How many schools does the data represent? How many classrooms?</div>', //Q4
        '<div class="a">As of Jan 11, 2018 there are active weather stations at 37 schools and classrooms sensors in 62 schools.</div>', //A4

        '<br>',

        '<div class="q">How were these schools chosen?</div>', //Q5
        '<div class="a">A representative sample of schools in different climate zones and building types were selected for monitoring. Click <a href="http://www.mkthinkstrategy.info/hidoe_thermalcomfort/Sensor_Deployment_Plan.pdf" target="_blank">here</a> for more information about the selection criteria and climate zones.</div>', //A5',
          '<br>',
        '<div class="a">Some of the types of school building that are monitored include:</div>',
          '<br>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Concrete/masonry linear buildings</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Concrete/masonry courtyard buildings</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Wood-frame permanent buildings</div>',
          '<div class="a" style="font-style:italic; text-indent:-17px; margin-left:40px;">• Various portable classrooms</div>',
        
        '<br>',

        '<div class="q">How do I search for a school? What if I cannot find the school that I am looking for?</div>', //Q6
        '<div class="a">To search for a school, click on the “Schools” tab at the top of the page. Click “Select Site,” and type the name of the school in the search box. You may also scroll through the dropdown menu and select a school from the database.</div>', //A6
        '<br>',
        '<div class="a">If a school does not have a weather station, data from the closest school will be displayed, usually within 5 miles or less.</div>',
        '<br>',
        '<div class="a">If a school has temperature sensors in its classrooms, the information will be displayed below the weather station data. If a school does not have indoor sensors, data from nearby schools that may have similar conditions can be viewed.</div>',
        
        '<br>',

        '<div class="q">How can i find out a school\'s <a href="http://www.mkthinkstrategy.info/hidoe_thermalcomfort/Sensor_Deployment_Plan.pdf" target="_blank">climate zone</a>?</div>', //Q7
        '<div class="a">After you search and select the desired school, the “Climate Type” will be listed under the “School Overview” profile. </div>', //A7

        '<br>',

        '<div class="q">Will these weather-sensing monitors be installed in more schools?</div>', //Q8
        '<div class="a">Additional weather stations are being installed with the goal of having stations installed in HIDOE’s 15 Complex Areas. In some cases, weather stations may be installed at specific schools when it is determined that microclimates caused by landforms, vegetation or urban development are creating different thermal conditions.</div>', //A8

        '<br>',

        '<div class="q">How are the outdoor weather conditions and indoor classroom temperatures being monitored?</div>', //Q9
          '<div class="a">Outdoor weather conditions are monitored using solar-powered weather stations mounted on school rooftops. These weather stations transmit data to a receiver in the school office that is connected to the Internet. The data is uploaded to Weather Underground, a public weather website, where it is accessible to the public directly or through the HIDOE Thermal Comfort website.</div>', //A9
          '<br>',
          '<div class="a">Indoor classroom temperatures are monitored by the use of data loggers that record the temperature and humidity every 30 minutes. Due to the challenge of transmitting data over long distances and through thick concrete walls, it was not possible to use sensors that transmit data wirelessly. Accordingly, the data is stored on each device and collected on a quarterly basis. Once data is collected, it is published on the HIDOE Thermal Comfort website.</div>',

          '<br>',
          '<br>',
        '<div class="a" style="color:#a9a9a9;">For additional queries, please call 808-784-5000.</div>', //A10
          
          '<br>',

      '</div>',
      
    ].join(''),
    scope: {},
    controller: 'keepcoolCtrl',
  }
})
app.controller('keepcoolCtrl', function($rootScope, $scope, $state, $stateParams, siteList) {

  /*check content-wrapper height*/
  resizeContentWrapper();

  /*declare + structure reference vars*/
  kc_sections = [ //holds titles of strategy sections
    {'title':   'PASSIVE INDOOR STRATEGIES',
     'hexcol':  '#33cc99'},
    {'title':   'ACTIVE INDOOR STRATEGIES',
     'hexcol':  '#bad294'},
    {'title':   'OUTDOOR STRATEGIES',
     'hexcol':  '#a5d7e0'},
  ];

  kc_strategies = [{//holds strategy card content
      'ss':       0,
      's':        0,
      'title':    'REFLECTIVE ROOFS + WALLS',
      'text':     'Light-colored coatings reduce heat absorbed from sunlight, reducing peak temperatures on bright days and significantly improving the efficiency and effectiveness of other solutions applied in concert.',
      'imgsrc':   './static/images/white-roof-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       0,
      's':        1,
      'title':    'NIGHTTIME FLUSHING',
      'text':     'Circulating cool air through rooms at night means that rooms are cool at the start of the day instead of trapping hot air from the previous day.',
      'imgsrc':   './static/images/thermal-flushing-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       0,
      's':        2,
      'title':    'REDUCE PAVING',
      'text':     'Reduce the presence of paved surfaces that absorb sunlight and radiate heat into adjacent rooms.',
      'imgsrc':   './static/images/reduce-paving-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       0,
      's':        3,
      'title':    'INCREASE SHADING',
      'text':     'Blocking the sunlight with trees or shades keeps adjacent rooms from heating up and creates a more comfortable campus.',
      'imgsrc':   './static/images/increase-shading-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       1,
      's':        0,
      'title':    'CEILING/WALL FANS',
      'text':     'The air movement from ceiling fans improves comfort in both naturally ventilated and air conditioned rooms. Large diameter fans make a big difference in cafeterias and gymnasiums.',
      'imgsrc':   './static/images/ceilingfans-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       1,
      's':        1,
      'title':    'PV AIR CONDITIONING',
      'text':     'PV Air Conditioning: Photovoltaic air conditioners use solar energy to sustainably generate electricity for cool air.',
      'imgsrc':   './static/images/pvac-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       1,
      's':        2,
      'title':    'CHILLED WATER STATIONS',
      'text':     'Providing students with chilled water via combination fountains/water bottle fillers is an effective way to improve comfort.',
      'imgsrc':   './static/images/chilledwater-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       1,
      's':        3,
      'title':    'EXHAUST FANS',
      'text':     'To assist nighttime flushing, exhaust fans remove hot air from the room and bring in cool fresh air to ensure the room is comfortable at the start of the day.',
      'imgsrc':   './static/images/exhaustfans-265x265.png',
      'photos':   [],
      'resrcs':   [],
      },
      {
      'ss':       2,
      's':        0,
      'title':    'FURNITURE',
      'text':     'click for more information',
      'imgsrc':   './static/images/furniture-canopytable-190x160.png',
      'kitcats':  [{  //CAT 0
                      'n':      0,
                      'kits':   [{  //KIT 0
                                    'n':      0,
                                    'title':  'PICNIC TABLE',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Recycled Plastic Step-Over Picnic Table',
                                                  'imgsrc': './static/images/furniture-picnictable-190x160.png',
                                                  'mm':     'Belson Outdoor (model:RDP6)',
                                                  'detail': 'seats 4-6 students',
                                                  'price':  '$800.00',
                                                  'vlink':  'https://www.google.com/#tbm=shop&q=Recycled+Plastic+Step+Over+Picnic+Table'
                                              }]
                                },
                                {   //KIT 1
                                    'n':      1,
                                    'title':  'CAFE TABLE + CHAIRS',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Magis Air Chair (x4)',
                                                  'imgsrc': './static/images/furniture-airchair-190x160.png',
                                                  'mm':     'Magis Outdoor/Indoor',
                                                  'detail': 'stacks for storage',
                                                  'price':  '$700.00',
                                                  'vlink':  'https://www.google.com/search?output=search&tbm=shop&q=Magis+Air+chair&oq=Magis+Air+chair&gs_l=products-cc.3..0.2519.4726.0.4814.15.7.0.8.8.0.66.387.7.7.0....0...1ac.1.64.products-cc..0.15.395.4MfXRA2qs74'
                                              },
                                              {   //ITEM 1
                                                  'n':      1,
                                                  'title':  'Magis Air Table',
                                                  'imgsrc': './static/images/furniture-airtable-190x160.png',
                                                  'mm':     'Magis Outdoor/Indoor',
                                                  'detail': 'stacks for storage',
                                                  'price':  '$349.00',
                                                  'vlink':  'https://www.google.com/#q=magis+air+table+square&*'
                                              }]
                                },
                                {  //KIT 2
                                    'n':      2,
                                    'title':  'TREE BENCH',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Plastic Tree Hugger Bench with Back',
                                                  'imgsrc': './static/images/furniture-treebench-190x160.png',
                                                  'mm':     'Barco Products (model:BN-85)',
                                                  'detail': 'fits around 4\' diameter tree, seats up to 12 students, buy with mounting kit',
                                                  'price':  '$1,300.00',
                                                  'vlink':  'https://www.google.com/#q=recycled+plastic+tree+hugger+bench&*'
                                              }]
                                },
                                {   //KIT 3
                                    'n':      3,
                                    'title':  'DOUBLE-SIDED BENCH',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Bollard Style Double-sided Bench, In-ground Mount',
                                                  'imgsrc': './static/images/furniture-dblsidedbench-190x160x.png',
                                                  'mm':     'Belson Outdoors (model:962S-PR6)',
                                                  'detail': '72"L x 46"W x 35-5/8"H',
                                                  'price':  '$908.00',
                                                  'vlink':  'https://www.google.com/#q=Bollard+Style+Double+Sided+Bench,+In-ground+Mount+Belson+Outdoors,+Model+962S-PR6&*'
                                              }]
                                },
                                {  //KIT 0
                                    'n':      4,
                                    'title':  'CANOPY PICNIC TABLE',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Canopy Picnic Table w/ Diamod Expanded Metal',
                                                  'imgsrc': './static/images/furniture-canopytable-190x160.png',
                                                  'mm':     'Ultra Play Systems (model:UTP-230-V6)',
                                                  'detail': 'seats 4-6 students',
                                                  'price':  '$4,120.00',
                                                  'vlink':  'https://www.google.com/search?output=search&tbm=shop&q=Canopy+Picnic+Table%2C+Diamond+Expanded+Metal&oq=Canopy+Picnic+Table%2C+Diamond+Expanded+Metal&gs_l=products-cc.3...1320.1320.0.2013.1.1.0.0.0.0.51.51.1.1.0....0...1ac.2.64.products-cc..0.0.0.orPayTNFV3Q'
                                              }]
                                }]
                  }]
      },
      {
      'ss':      2,
      's':       1,
      'title':   'SHADE STRUCTURES',
      'text':    'click for more information',
      'imgsrc':  './static/images/furniture-freestaindingshadesail-190x160.png',
      'kitcats': [{   //CAT 0
                      'n':      0,
                      'kits':   [{  //KIT 0
                                    'n':      0,
                                    'title':  'HANGING SHADE SAIL',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Ready-to-Hang Shade Sails',
                                                  'imgsrc': './static/images/furniture-hangingshadesale-190x160x.png',
                                                  'mm':     'Coolaroo (model:449315)',
                                                  'detail': '16\'L x 16\'W',
                                                  'price':  '$65.00 each',
                                                  'vlink':  'https://www.google.com/search?output=search&tbm=shop&q=Ready-to-Hang+Shade+Sails+Coolaroo%2C+449315&oq=Ready-to-Hang+Shade+Sails+Coolaroo%2C+449315&gs_l=products-cc.3...8354.8354.0.9225.1.1.0.0.0.0.50.50.1.1.0....0...1ac.2.64.products-cc..0.0.0.HQsySBpRhVU'
                                              }]
                                },
                                {   //KIT 1
                                    'n':      1,
                                    'title':  'STANDING SHADE SAIL',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Freestanding Shade Sail',
                                                  'imgsrc': './static/images/furniture-freestaindingshadesail-190x160.png',
                                                  'mm':     'Shades Asia',
                                                  'detail': 'custom size options',
                                                  'price':  '$2,200.00-$2,900.00',
                                                  'vlink':  'https://www.google.com/#q=Freestanding+Shade+Sail+Shades+Asia&*'
                                              }]
                                },
                                {   //KIT 2
                                    'n':      2,
                                    'title':  'PERGOLA',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'White Aluminum Freestanding Pergola',
                                                  'imgsrc': './static/images/furniture-pergola-190x160.png',
                                                  'mm':     'Americana Building Projects (model:PDHFS1220)',
                                                  'detail': '144"W x 240"L x 112.5"H',
                                                  'price':  '$4663.00',
                                                  'vlink':  'https://www.google.com/search?output=search&tbm=shop&q=White+Aluminum+Freestanding+Pergola+Americana+Building+Products%2C+PDHFS1220&oq=White+Aluminum+Freestanding+Pergola+Americana+Building+Products%2C+PDHFS1220&gs_l=products-cc.3...6005.6005.0.6840.1.1.0.0.0.0.48.48.1.1.0....0...1ac.2.64.products-cc..0.0.0.v9CYybmiTTk'
                                              }]
                                },
                                {   //KIT 3
                                    'n':      3,
                                    'title':  'DOUBLE-SIDED BENCH w/ MINI-SHADE ATTACHMENT',
                                    'items':  [{  //ITEM 0
                                                  'n':      0,
                                                  'title':  'Bollard Style Double-sided Bench, In-ground Mount',
                                                  'imgsrc': './static/images/furniture-dblsidedbench-190x160x.png',
                                                  'mm':     'Belson Outdoors (model:962S-PR6)',
                                                  'detail': '72"L x 46"W x 35-5/8"H',
                                                  'price':  '$908.00',
                                                  'vlink':  'https://www.google.com/#q=Bollard+Style+Double+Sided+Bench,+In-ground+Mount+Belson+Outdoors,+Model+962S-PR6&*'
                                              },
                                              {   //ITEM 1
                                                  'n':      1,
                                                  'title':  'Mini-Shade Structure Attachment',
                                                  'imgsrc': './static/images/furniture-minishadeattachment-190x160x.png',
                                                  'mm':     'Belson Outdoors (model:STS765BA)',
                                                  'detail': '87"L x 84"W',
                                                  'price':  '$968.00',
                                                  'vlink':  'https://www.google.com/#q=Bollard+Style+Double+Sided+Bench,+In-ground+Mount+Belson+Outdoors,+Model+962S-PR6&*'
                                              }]
                                }]
                  }]
      },
      {
      'ss':       2,
      's':        2,
      'title':    'TREES + VEGETATION',
      'text':     'click for more information',
      'imgsrc':   './static/images/trees-and-vegetation-265x265.png',
      'items':    [{  //ITEM 0
                      n:       0,
                      imgsrc:  './static/images/plant-monkeypod-220x293.png',
                      comnm:   'Monkeypod Tree',
                      botnm:   'Samanea Saman',
                      exphgt:  '40\'-50\'',
                      expcpy:  '100\'+'
                  },
                  {   //ITEM 1
                      n:       1,
                      imgsrc:  './static/images/plant-halatree-220x293.png',
                      comnm:   'Hala Tree',
                      botnm:   'Pandanus Tectorius',
                      exphgt:  '35\'',
                      expcpy:  '15\'-35\''
                  },
                  {   //ITEM 2
                      n:       2,
                      imgsrc:  './static/images/plant-pinktecoma-220x293.png',
                      comnm:   'Pink Tacoma',
                      botnm:   'Tabebuia Heterophylla',
                      exphgt:  '30\'-40\'',
                      expcpy:  '20\'-25\''
                  },
                  {   //ITEM 3
                      n:       3,
                      imgsrc:  './static/images/plant-kamani-220x293.png',
                      comnm:   'Kamani Tree',
                      botnm:   'Colophyllum Inophyllum',
                      exphgt:  '30\'-40\'',
                      expcpy:  '30\'-40\''
                  },
                  {   //ITEM 4
                      n:       4,
                      imgsrc:  './static/images/plant-goldenshower-220x293.png',
                      comnm:   'Golden Shower Tree',
                      botnm:   'Cassia Fistula',
                      exphgt:  '25\'-35\'',
                      expcpy:  '30\'-35\''
                  },
                  {   //ITEM 5
                      n:       5,
                      imgsrc:  './static/images/plant-narra-220x293.png',
                      comnm:   'Narra Tree',
                      botnm:   'Pterocarpus Indicus',
                      exphgt:  '30\'-40\'',
                      expcpy:  '30\'-40\''
                  },
                  {   //ITEM 6
                      n:       6,
                      imgsrc:  './static/images/plant-lonomea-220x293.png',
                      comnm:   'Lonomea Tree',
                      botnm:   'Sapindus Oahuensis',
                      exphgt:  '30\'-40\'',
                      expcpy:  '20\'-30\''
                  },
                  {   //ITEM 7
                      n:       7,
                      imgsrc:  './static/images/plant-silvertrumpet-220x293.png',
                      comnm:   'Silver Trumpet Tree',
                      botnm:   'Tabebuia Aurea',
                      exphgt:  '25\'',
                      expcpy:  '15\'-20\''
                  },
                  {   //ITEM 8
                      n:       8,
                      imgsrc:  './static/images/plant-hongkongorchid-220x293.png',
                      comnm:   'Hong Kong Orchid',
                      botnm:   'Bauhinia Blakeana',
                      exphgt:  '25\'-30\'',
                      expcpy:  '30\''
                  },
                  {   //ITEM 9
                      n:       9,
                      imgsrc:  './static/images/plant-rainbowshower-220x293.png',
                      comnm:   'Rainbow Shower Tree',
                      botnm:   'Cassia Nealiae',
                      exphgt:  '25\'-30\'',
                      expcpy:  '30\'-35\''
                  },
                  {   //ITEM 10
                      n:       10,
                      imgsrc:  './static/images/plant-koutree-220x293.png',
                      comnm:   'Kou Tree',
                      botnm:   'Cordia Subcordata',
                      exphgt:  '30\'-40\'',
                      expcpy:  '25\'-30\''
                  },
                  {   //ITEM 11
                      n:       11,
                      imgsrc:  './static/images/plant-puakenikeni-220x293.png',
                      comnm:   'Puakenikeni',
                      botnm:   'Fagraea Barterana',
                      exphgt:  '25\'',
                      expcpy:  '25\''
                  },
                  {   //ITEM 12
                      n:       12,
                      imgsrc:  './static/images/plant-loulu-220x293.png',
                      comnm:   'Loulu Tree',
                      botnm:   'Pritchardia Spp.',
                      exphgt:  '20\'-35\'',
                      expcpy:  '10\'-15\''
                  },
                  {   //ITEM 13
                      n:       13,
                      imgsrc:  './static/images/plant-areca-220x293.png',
                      comnm:   'Areca Tree',
                      botnm:   'Dypsis Luescens',
                      exphgt:  '25\'-30\'',
                      expcpy:  '15\'-20\''
                  },
                  {   //ITEM 14
                      n:       14,
                      imgsrc:  './static/images/plant-milotree-220x293.png',
                      comnm:   'Milo Tree',
                      botnm:   'Thespesia Populnea',
                      exphgt:  '30\'-40\'',
                      expcpy:  '30\'-40\''
                  }]
      }
  ];

  /*update nav-bar styling*/
  d3.selectAll(".nav-button").classed("active-nav", false);
  d3.selectAll(".nav-button").classed("alt-nav", true);
  d3.select("#keepcool-nav").classed("alt-nav", false);
  d3.select("#keepcool-nav").classed("active-nav", true);

  //write 'what is thermal comfort?' title
  d3.select('#kc-intro').append('div')
    .style('font-size','20px')
    .style('font-weight',400)
    .style('letter-spacing','1px')
    .style('text-align','center')
    .style('color','#025468')
    .text('WHAT IS THERMAL COMFORT?');

  //write 'what is thermal comfort?' description
  d3.select('#kc-intro').append('div')
    .style('margin-top','10px')
    .style('font-size','14px')
    .style('font-weight',200)
    .style('letter-spacing','1px')
    .style('line-height','24px')
    .style('text-align','center')
    .style('color','#525252')
    .text(
      'Thermal comfort is the condition of mind which expresses satisfaction with the thermal environment. ' +
      'Thermal comfort depends on air temperature, humidity, radiant temperature, air velocity, metabolic rates, and clothing. ' +
      'To maximize the thermal comfort of students and teachers in schools, the Hawai\ʻi State Department of Education is working to abate heat and give school occupants more control over their environment.'
      );

  //write 'ways to keep cool' title
  d3.select('#kc-ways').append('div')
    .style('margin-bottom','15px')
    .style('font-size','20px')
    .style('font-weight',400)
    .style('letter-spacing','1px')
    .style('text-align','center')
    .style('color','#025468')
    .text('COOL STRATEGIES');

  //write 'FAQ' title
  d3.select('#kc-faqs-t').append('div')
    .style('margin-bottom','15px')
    .style('font-size','20px')
    .style('font-weight',400)
    .style('letter-spacing','1px')
    .style('text-align','center')
    .style('color','#025468')
    .text('FREQUENTLY ASKED QUESTIONS');

  //draw ways
  initKcWaysLayout();

  function initKcWaysLayout () {
  console.log('initKcWaysLayout()'); //log function nav

    //write section0 title holder
    d3.select('#kc-ways').append('div')
      .attr('id','ss0t')
      .attr('class','ss-tholder');

    //write section0 holder: passsive indoor strategies
    d3.select('#kc-ways').append('div')
      .attr('id','ss0')
      .attr('class','strategy-section')
      .style('height','360px');

    //write section0 strategy holders
    d3.select('#ss0').append('div')
      .attr('id','ss0s0')
      .attr('class','ss-strat');
    d3.select('#ss0').append('div')
      .attr('id','ss0s1')
      .attr('class','ss-strat');
    d3.select('#ss0').append('div')
      .attr('id','ss0s2')
      .attr('class','ss-strat');
    d3.select('#ss0').append('div')
      .attr('id','ss0s3')
      .attr('class','ss-strat');
    
    //write section1 title holder
    d3.select('#kc-ways').append('div')
      .attr('id','ss1t')
      .attr('class','ss-tholder');

    //write section1 holder: active indoor strategies
    d3.select('#kc-ways').append('div')
      .attr('id','ss1')
      .attr('class','strategy-section')
      .style('height','380px');

    //write section1 strategy holders
    d3.select('#ss1').append('div')
      .attr('id','ss1s0')
      .attr('class','ss-strat');
    d3.select('#ss1').append('div')
      .attr('id','ss1s1')
      .attr('class','ss-strat');
    d3.select('#ss1').append('div')
      .attr('id','ss1s2')
      .attr('class','ss-strat');
    d3.select('#ss1').append('div')
      .attr('id','ss1s3')
      .attr('class','ss-strat');

    //write section2 title holder
    d3.select('#kc-ways').append('div')
      .attr('id','ss2t')
      .attr('class','ss-tholder');

    //write section2 holder: outdoor strategies
    d3.select('#kc-ways').append('div')
      .attr('id','ss2')
      .attr('class','strategy-section')
      .style('height','265px');

    //write section2 strategy holders
    d3.select('#ss2').append('div')
      .attr('id','ss2s0')
      .attr('class','ss-strat');
    d3.select('#ss2').append('div')
      .attr('id','ss2s1')
      .attr('class','ss-strat');
    d3.select('#ss2').append('div')
      .attr('id','ss2s2')
      .attr('class','ss-strat');

    //loop, and draw description titles
    for (var i = 0; i < kc_sections.length; i++) {

      //gen formatting variables
      var hexcol = kc_sections[i].hexcol;
      var rgbacol = 'rgba(' + parseInt(hexcol.slice(-6,-4),16)
        + ',' + parseInt(hexcol.slice(-4,-2),16)
        + ',' + parseInt(hexcol.slice(-2),16)
        +',0.5)';

      //write section title
      d3.select('#ss' + i + 't').append('div')
        .attr('class','title')
        .style('color',hexcol)
        .text(kc_sections[i].title);

      //loop, and draw strategy cards
      for (var j = 0; j < kc_strategies.length; j++) {

        if (kc_strategies[j].ss == i) {
          //write strategy card
          d3.select('#ss' + i + 's' + kc_strategies[j].s)
            .style('position','relative')
            .style('border','solid 1px '+ hexcol)
            .style('border-radius','3px')
            .html(
              '<div class="title" style="background-color:' + hexcol + ';">' + kc_strategies[j].title + '</div>' +
              '<img src="' + kc_strategies[j].imgsrc + '">'
            ).on('mouseover',function() {
              var ss = (this.id).substr(-3)[0] * 1; //extract section id# from id field
              var s = (this.id).substr(-1) * 1; //extract strategy id# from id field

              if (ss == 2) {
                var hexcol = kc_sections[ss].hexcol; //gen formatting vars
                var rgbacol = 'rgba(' + parseInt(hexcol.slice(-6,-4),16) //gen formatting var
                  + ',' + parseInt(hexcol.slice(-4,-2),16)
                  + ',' + parseInt(hexcol.slice(-2),16)
                  +',0.5)';

                d3.select('#ss' + ss + 's' + s).style('border','solid 1px ' + rgbacol); //hexcol --> rgbacol
                d3.selectAll('#ss' + ss + 's' + s + ' .title').style('background-color',rgbacol); //hexcol --> rgbacol
              }
            }).on('mouseout',function() {
              var ss = (this.id).substr(-3)[0] * 1; //extract section id# from id field
              var s = (this.id).substr(-1) * 1; //extract strategy id# from id field

              if (ss == 2) {
                var hexcol = kc_sections[ss].hexcol; //gen formatting var

                d3.select('#ss' + ss + 's' + s).style('border','solid 1px ' + hexcol); //rgbacol --> hexcol
                d3.selectAll('#ss' + ss + 's' + s + ' .title').style('background-color',hexcol); //rgbacol --> hexcol
              }
            }).on('click',function() {
              var ss = (this.id).substr(-3)[0] * 1; //extract section id# from id field
              var s = (this.id).substr(-1) * 1; //extract strategy id# from id field

              if (ss == 2) {
                var hexcol = kc_sections[ss].hexcol; //gen formatting var
                var rgbacol = 'rgba(' + parseInt(hexcol.slice(-6,-4),16) //gen formatting var
                  + ',' + parseInt(hexcol.slice(-4,-2),16)
                  + ',' + parseInt(hexcol.slice(-2),16)
                  +',0.5)';

                initKcModal(ss,s,hexcol,rgbacol); //init modal
              }
            });

          //write strategy description
          d3.select('#ss' + i + 's' + kc_strategies[j].s).append('div')
            .attr('class','text')
            .text(kc_strategies[j].text);

          //modify text placement for category 3 strategies
          if (kc_strategies[j].text == 'click for more information') {
            d3.select('#ss' + i + 's' + kc_strategies[j].s + ' .text')
              .style('position','absolute')
              .style('bottom','1px');
          }

          //modify image placement for category 3 strategies
          if ((i == 2 && kc_strategies[j].s == 0) || (i == 2 && kc_strategies[j].s == 1)) {
            d3.select('#ss' + i + 's' + kc_strategies[j].s + ' img')
              .style('margin-top','18px');
          }
        }
      }
    }
  }

  function initKcModal(ss,s,hexcol,rgbacol) {
  console.log('initKcModal()') //log function nav

    //reveal modal
    d3.select('#kc-modal').style('display','inline-block');

    //append content holder
    d3.select('#kc-modal').append('div')
      .attr('id','kc-content')
      .style('display','inline-block')
      .style('width','100%')
      .style('height','100%');

    //pull coords of selected strat
    var coords = document.getElementById('ss' + ss + 's' + s).getBoundingClientRect();

    //place content at location of selected strategy
    d3.select('#kc-content')
      .style('display','inline-block')
      .style('position','relative')
      .style('top',(coords.top - 69) + 'px')
      .style('left',(coords.left - $(window).width()*.01 - 1) +  'px')
      .style('width','247px')
      .style('height','237px')
      .style('background-color','#ffffff')
      .style('border','solid 1px ' + hexcol)
      .style('border-radius','3px');

    //create html elements inside kc-content
    d3.select('#kc-content').append('div') //strategy title
      .style('padding','5px 6px 5px 6px')
      .style('color','#ffffff')
      .style('font-size','14px')
      .style('font-weight',200)
      .style('letter-spacing','2px')
      .style('background-color',hexcol)
      .text(kc_strategies[4*ss + s].title);

    d3.select('#kc-content').append('div') //strategy label  
      .style('position','absolute').style('top','4px').style('left','-100px')
      .style('font-size','16px')
      .style('color',hexcol)
      .text('STRATEGY :')

    d3.select('#kc-content').append('i') //close button  
      .attr('id','closebutton')
      .attr('class','material-icons button')
      .style('position','absolute').style('top','-3px').style('right','-50px')
      .style('font-size','36px')
      .style('color',hexcol)
      .style('cursor','pointer')
      .text('close')
      .on('mouseover',
        function() {
          this.style.color = '#ffffff';
        })
      .on('mouseout',
        function() {
          this.style.color = hexcol;
        })
      .on('click',
        function() {
          d3.select('#kc-modal').html('');
          d3.select('#kc-modal').style('display','none');
        });

    //extend content over 1000ms
    d3.select('#kc-content')
      .transition().delay(0).duration(500)
      .style('transform','translate(' + -(coords.left - ($('#content-wrapper').width()) * 0.2 - 10) + 'px,' + -(coords.top - 80) + 'px)')
      .style('width',($('#content-wrapper').width() * 0.6 - 2) + 'px')
      .style('height',($('#content-wrapper').height() - 24) + 'px');

    d3.select('#kc-content')
      .transition().delay(500).duration(0)
      .style('left',null)
      .style('top','12px')
      .style('transform',null)
      .style('margin','0% calc(20% - 1px)')
      .style('width','60%')
      .style('height','calc(100% - 26px)');

    //set modal content
    setKcModalContent(ss,s,hexcol,rgbacol); 
  }

  function setKcModalContent(ss,s,hexcol,rgbacol) {
  console.log('setKcModalContent()'); //log function nav
    
    //add outer wrapper to content-holder
    d3.select('#kc-content').append('div')
      .attr('id','outer-wrapper')
      .style('margin','10px')
      .style('height','calc(100% - 45px)')
      .style('overflow-y','auto');

    //add inner wrapper to content-holder
    d3.select('#outer-wrapper').append('div')
      .attr('id','inner-wrapper');

    setTimeout(function() { //delay content for modal content to grow bigger than modal image - 250ms should do it...
      if (ss == 0 || ss == 1) { //set modal content for static resources
        followKcModalTemplate1(ss,s,hexcol,rgbacol);
      } else if (ss == 2 && s !== 2) {
        followKcModalTemplate2(ss,s,hexcol,rgbacol);
      } else {
        followKcModalTemplate3(ss,s,hexcol,rgbacol);
      }
    },1000);
  }

  function followKcModalTemplate1(ss,s,hexcol,rgbacol) {
  console.log('followKcModalTemplate1()'); //log function nav

    d3.select('#inner-wrapper').append('div') //Diagram image
      .style('position','relative').style('top','-15px')
      .style('margin','0px 20px')
      .style('float','right')
      .style('max-height','245px')
      .style('max-width','245px')
      .html('<img src="' + kc_strategies[4*ss + s].imgsrc + '">');

    d3.select('#inner-wrapper').append('div') //'Description' banner
      .style('margin','20px 0px 0px 0px')
      .style('width','calc(100% - 305px')
      .style('padding','0px 0px 5px 5px')
      .style('font-size','14px')
      .style('color','#637e8d')
      .style('letter-spacing','1px')
      .style('font-weight',400)
      .text('Description');

    d3.select('#inner-wrapper').append('div') //'Description' text
      .style('margin','5px 0px 0px 0px')
      .style('width','calc(100% - 305px')
      .style('height','200px')
      .style('padding','0px 0px 5px 5px')
      .style('font-size','13px')
      .style('color','#525252')
      .style('letter-spacing','1px')
      .style('font-weight',200)
      .style('line-height','16px')
      .style('font-style','italic')
      .text(kc_strategies[4*ss + s].text);

    d3.select('#inner-wrapper').append('div') //'Project Photos' banner
      .style('padding','0px 0px 5px 5px')
      .style('font-size','14px')
      .style('color','#637e8d')
      .style('letter-spacing','1px')
      .style('font-weight',400)
      .text('Project Photos');

    d3.select('#inner-wrapper').append('div') //'Project Photos' wrapper
      .attr('id','photos-wrapper')
      .style('width','100%')
      .style('overflow-x','auto');

    d3.select('#photos-wrapper').append('div') //'Project Photos' scroller
      .attr('id','photos-holder')
      .style('margin','5px 0px 12px 0px')
      .style('height','120px');

    d3.select('#inner-wrapper').append('div') //big picture wrapper
      .attr('id','bp-wrapper')
      .style('display','none');

    d3.select('#bp-wrapper').append('div') //big picture holder
      .attr('id','bp-holder')
      .style('max-width','400px')
      .style('padding','10px calc((100% - 400px)/2) 8px calc((100% - 400px)/2)')
      .style('background-color','#d3d3d3')
      .style('border-radius','3px')
      .style('overflow','hidden');

    for (var i = 0; i < kc_strategies[4*ss + s].photos.length; i++ ) { //load project photos
      d3.select('#photos-holder').append('div')
        .attr('id','p' + i)
        .style('display','inline-block')
        .style('margin','5px 5px 5px 5px')
        .style('height','110px')
        .style('min-width','110px')
        .style('background-color','#d3d3d3')
        .style('border','1px solid #a9a9a9')
        .style('border-radius','3px')
        .style('overflow','hidden')
        .style('cursor','pointer')
        .html(
          '<img src="' + kc_strategies[4*ss + s].photos[i] + '">'
        ).on('mouseover',function() {
          d3.select('#' + this.id).style('border-color','#fbb157');
        }).on('mouseout',function() {
          d3.select('#' + this.id).style('border-color','#a9a9a9');
        }).on('click',function() {
          clickKcThumbnail(ss,s,(this.id).substr(-1) * 1);
        });
    }

    var phw = 0; //holds value that photo-holder width will be set to
    setTimeout(function () { //delay content for project photos to load, and div widths to update - 250ms should do it...
      for (var i = 0; i < kc_strategies[4*ss + s].photos.length; i++ ) { //calc project photos width
        phw = phw + $('#p' + i).width() + 13; //add loaded photo width to pw total
      }
      $('#photos-holder').width(phw); //holds photo-holder width
    },250);

    d3.select('#inner-wrapper').append('div') //'Learn More' banner
      .style('margin','20px 0px 0px 0px')
      .style('padding','0px 0px 5px 5px')
      .style('font-size','14px')
      .style('color','#637e8d')
      .style('letter-spacing','1px')
      .style('font-weight',400)
      .text('Learn More');

    for (var i = 0; i < kc_strategies[4*ss + s].resrcs.length; i++ ) { //load additional resource links
      d3.select('#inner-wrapper').append('div')
        .style('margin','5px 0px 0px 0px')
        .style('padding','0px 0px 5px 5px')
        .style('font-size','13px')
        .style('color','#525252')
        .style('letter-spacing','1px')
        .style('font-weight',200)
        .style('line-height','16px')
        .style('font-style','italic')
        .html('<a>' + kc_strategies[4*ss + s].resrcs[i] + '</a>');
    }
  }

  function followKcModalTemplate2(ss,s,hexcol,rgbacol) {
  console.log('followKcModalTemplate2()'); //log function nav

    for (var i = 0; i < kc_strategies[4*ss + s].kitcats.length; i++) {
      
      var kitcat = kc_strategies[4*ss + s].kitcats[i];
      d3.select('#inner-wrapper').append('div') //append category holder
        .attr('id','kitcat' + i)
        .style('height',kitcat.kits.length < 3? '400px':'665px')
        .style('width','100%');

      for (var j = 0; j < kc_strategies[4*ss + s].kitcats[i].kits.length; j++) {
        
        var kit = kc_strategies[4*ss + s].kitcats[i].kits[j];
        d3.select('#kitcat' + i).append('div') //append kit holder
          .attr('id','c' + i + 'k' + j)
          .attr('class','kit')
          .style('width','calc(' + 100/3 + '% * ' + kit.items.length + ' - 12px)')
          .style('border','1px solid #a9a9a9');
        d3.select('#c' + i + 'k' + j).append('div') //append kit title
          .attr('class','kit-title')
          .style('background-color','#a9a9a9')
          .text('Kit #' + (kit.n + 1) + ' : ' + kit.title);

        for (var k = 0; k < kc_strategies[4*ss + s].kitcats[i].kits[j].items.length; k++) {

          var item = kc_strategies[4*ss + s].kitcats[i].kits[j].items[k];
          var n = kc_strategies[4*ss + s].kitcats[i].kits[j].items.length;

          d3.select('#c' + i + 'k' + j).append('div') //append item holder
            .attr('id','c' + i + 'k' + j + 'i' + k)
            .style('position','relative')
            .style('display','inline-block')
            .style('float','left')
            .style('width','192px')
            .style('height','300px');

          if (k !== n - 1) {
            d3.select('#c' + i + 'k' + j + 'i' + k) //apply full margin to item holder if it is the last object in the array
              .style('margin','10px ' + 'calc((' + 100/n + '% - 193px) / 2)');
            d3.select('#c' + i + 'k' + j).append('div') //append item divider
              .style('display','inline-block')
              .style('float','left')
              .style('width','1px')
              .style('height','278px')
              .style('margin-top','10px')
              .style('background-color','#a9a9a9');
          } else {
            d3.select('#c' + i + 'k' + j + 'i' + k) //apply a margin 2px shorter if it not the last object in the array
              .style('margin','10px ' + 'calc((' + 100/n + '% - 192px) / 2)');
          }

          d3.select('#c' + i + 'k' + j + 'i' + k).append('div') //append item image
            .style('display','block')
            .style('width','192px')
            .style('max-width','192px')
            .html(
              '<img style="border:1px solid #a9a9a9; position:relative; " src="' + item.imgsrc + '">'
            );

          d3.select('#c' + i + 'k' + j + 'i' + k).append('div') //append item title
            .style('padding-top','5px')
            .style('color','#025468')
            .style('font-size','11px')
            .style('font-weight',600)
            .style('letter-spacing','1px')
            .text(item.title);

          // d3.select('#c' + i + 'k' + j + 'i' + k).append('div') //append item make/model
          //   .style('padding-top','3px')
          //   .style('color','#025468')
          //   .style('font-size','11px')
          //   .style('font-weight',200)
          //   .style('letter-spacing','1px')
          //   .style('font-style','italic')
          //   .text(item.mm);

          d3.select('#c' + i + 'k' + j + 'i' + k).append('div') //append item details
            .style('padding-top','5px')
            .style('color','#025468')
            .style('font-size','11px')
            .style('letter-spacing','1px')
            .html('<span style="font-weight:200">' + item.detail + '</span>');

          // d3.select('#c' + i + 'k' + j + 'i' + k).append('div') //append item cost
          //   .style('padding-top','5px')
          //   .style('color','#025468')
          //   .style('font-size','11px')
          //   .style('letter-spacing','1px')
          //   .html('<span style="font-weight:200; margin-right:2px;">cost: </span><span style="font-weight:600">' + item.price + '</span><span style="margin-left:2px; font-style:italic; font-weight:200;">(est.) </span>');

          // d3.select('#c' + i + 'k' + j + 'i' + k).append('div') //append vendor link
          //   .attr('id',item.vlink)
          //   .style('position','absolute').style('bottom','30px')
          //   .style('background-color','#a9a9a9')
          //   .style('width','calc(100% - 10px)')
          //   .style('padding','5px')
          //   .style('text-align','center')
          //   .style('color','#ffffff')
          //   .style('font-size','11px')
          //   .style('border-radius','4px')
          //   .style('letter-spacing','1px')
          //   .style('cursor','pointer')
          //   .text('FIND VENDOR')
          //   .on('mouseover',function() {
          //     this.style.backgroundColor = '#d3d3d3';
          //   }).on('mouseout',function() {
          //     this.style.backgroundColor = '#a9a9a9';
          //   }).on('click',function() {
          //     var win = window.open(this.id + ', \'_blank\'');
          //     win.focus();
          //   });
        }
      }
    }
  }

  function followKcModalTemplate3(ss,s,hexcol,rgbacol) {
  console.log('followKcModalTemplate3()'); //log function nav

    d3.select('#inner-wrapper').append('div') //append instructions
      .attr('id','instructions')
      .style('width','100%')
      .style('margin','5px ' + (($('#inner-wrapper').width() - 660) / 6 - 1) + 'px 25px ' + (($('#inner-wrapper').width() - 660) / 6 - 1) + 'px');

    d3.select('#instructions').append('div') //append instructions
      .style('padding-top','3px')
      .style('margin-right','20px')
      .style('width','400px')
      .style('font-size','14px')
      .style('color','#025468')
      .text('View the State of Hawai\ʻi\'s list of certified nurseries, where shade trees such as those displayed below may be purchased:');

    d3.select('#instructions').append('span') //append vendor link
      .style('background-color','#a9a9a9')
      .style('display','inline-block')
      .style('width','200px')
      .style('padding','5px')
      .style('margin-top','10px')
      .style('text-align','center')
      .style('color','#ffffff')
      .style('font-size','11px')
      .style('border-radius','4px')
      .style('letter-spacing','1px')
      .style('cursor','pointer')
      .text('FIND VENDOR')
      .on('mouseover',function() {
        this.style.backgroundColor = '#d3d3d3';
      }).on('mouseout',function() {
        this.style.backgroundColor = '#a9a9a9';
      }).on('click',function() {
        var win = window.open('http://hdoa.hawaii.gov/pi/pq/certified-nurseries/','_blank');
        win.focus();
      });

    for (var i = 0; i < kc_strategies[4*ss + s].items.length; i++) {
      
      var item = kc_strategies[4*ss + s].items[i];
      d3.select('#inner-wrapper').append('div') //append item image
        .attr('id','holder-' + i)
        .attr('class','treeimgholder')
        .style('position','relative')
        .style('display','inline-block')
        .style('float','left')
        .style('width','220px')
        .style('max-width','220px')
        .style('height','400px')
        .style('margin','10px ' + (($('#inner-wrapper').width() - 660) / 6 - 1) + 'px')
        .html(
          '<img style="border:1px solid #a9a9a9;" src="' + item.imgsrc + '">' 
        );

      d3.select('#holder-' + i).append('div') //append tree common name
        .style('padding-top','5px')
        .style('color','#025468')
        .style('font-size','11px')
        .style('font-weight',600)
        .style('letter-spacing','1px')
        .text(item.comnm);

      d3.select('#holder-' + i).append('div') //append tree biological name
        .style('padding-top','5px')
        .style('color','#025468')
        .style('font-size','11px')
        .style('font-weight',200)
        .style('letter-spacing','1px')
        .style('font-style','italic')
        .text(item.botnm);

      d3.select('#holder-' + i).append('div') //append expected height
        .style('padding-top','10px')
        .style('color','#025468')
        .style('font-size','11px')
        .style('letter-spacing','1px')
        .html('<span style="font-weight:200; margin-right:4px;">expected height: </span><span style="font-weight:600;">' + item.exphgt + '</span>');

      d3.select('#holder-' + i).append('div') //append expected canopy
        .style('padding-top','5px')
        .style('color','#025468')
        .style('font-size','11px')
        .style('letter-spacing','1px')
        .html('<span style="font-weight:200; margin-right:4px;">expected canopy: </span><span style="font-weight:600;">' + item.expcpy + '</span>');
    }
  }
  
  function clickKcThumbnail(ss,s,p) {
  console.log('clickKcThumbnail()'); //nav function nav

    d3.selectAll('#bp-wrapper i').remove();
    d3.select('#bp-wrapper').style('display','block')

    d3.select('#bp-holder') // place big image
      .html(
        '<img style="border:1px solid #ffffff; position:relative;" src="' + kc_strategies[4*ss + s].photos[p] + '">'
      );

    d3.selectAll('#bp-wrapper').append('i') //hide big picture button 
      .attr('class','material-icons button')
      .style('position','relative').style('top',-($('#bp-holder').height() + 38) + 'px')
      .style('margin-left','calc(100% - (100% - 420px)/2)')
      .style('font-size','35px')
      .style('color','#ffffff')
      .style('cursor','pointer')
      .text('close')
      .on('mouseover',
        function() {
          this.style.color = '#a9a9a9';
        })
      .on('mouseout',
        function() {
          this.style.color = '#ffffff';
        })
      .on('click',
        function() {
          d3.select('#bp-wrapper').style('display','none');
        });
  
    d3.selectAll('#bp-wrapper').append('i') //next big picture button 
      .attr('id','nextbpbutton')
      .attr('class','material-icons button')
      .style('position','relative').style('top',-($('#bp-holder').height() / 2 + 25) + 'px')
      .style('margin-left', '-48px')
      .style('font-size','60px')
      .style('color','#ffffff')
      .style('cursor','pointer')
      .text('navigate_next')
      .on('mouseover',
        function() {
          this.style.color = '#a9a9a9';
        })
      .on('mouseout',
        function() {
          this.style.color = '#ffffff';
        })
      .on('click',
        function() {
          clickKcThumbnail(ss,s,(p + 1));
        });

    if (typeof kc_strategies[4*ss + s].photos[p + 1] == 'undefined') {
      d3.select('#nextbpbutton').style('visibility','hidden');
    }

    d3.selectAll('#bp-wrapper').append('i') //previous big picture button 
      .attr('id','prevbpbutton')
      .attr('class','material-icons button')
      .style('position','relative').style('top',-($('#bp-holder').height() / 2 + 25) + 'px')
      .style('margin-left', '-518px')
      .style('font-size','60px')
      .style('color','#ffffff')
      .style('cursor','pointer')
      .text('navigate_before')
      .on('mouseover',
        function() {
          this.style.color = '#a9a9a9';
        })
      .on('mouseout',
        function() {
          this.style.color = '#ffffff';
        })
      .on('click',
        function() {
          clickKcThumbnail(ss,s,(p - 1));
        });

    if (p == 0) {
      d3.select('#prevbpbutton').style('visibility','hidden');
    }
  }

  //resizing listener
  $(window).on('resize',function() {
    d3.selectAll('.treeimgholder').style('margin','10px ' + (($('#inner-wrapper').width() - 660) / 6 - 1) + 'px');
    d3.select('#instructions').style('margin','5px ' + (($('#inner-wrapper').width() - 660) / 6 - 1) + 'px 25px ' + (($('#inner-wrapper').width() - 660) / 6 - 1) + 'px');

  });

  //photoclicklistener
  $('#photos-holder div').on('click',clickKcThumbnail);

  /*header page nav listeners*/
  $('#home-nav').on('click',navToHome);
  function navToHome() {
    $state.go('home',{
      siteid: '',
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#schools-nav').on('click',navToSchools);
  function navToSchools() {
    $state.go('schools',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
  $('#datatrends-nav').on('click',navToDataTrends);
  function navToDataTrends() {
    $state.go('keepcool',{
      siteid: $stateParams.siteid,
      homemap: {
        weather:$stateParams.homemap.weather,
        airqual:$stateParams.homemap.airqual,
        airqmeasure:$stateParams.homemap.airqmeasure,
        sites:$stateParams.homemap.sites,
      },
      schools: {
        rwmeasures: {
          temp:$stateParams.schools.rwmeasures.temp,
          relh:$stateParams.schools.rwmeasures.relh,
          wspd:$stateParams.schools.rwmeasures.wspd,
          solr:$stateParams.schools.rwmeasures.solr,
          rain:$stateParams.schools.rwmeasures.rain,
        },
      },
    });
  };
});



