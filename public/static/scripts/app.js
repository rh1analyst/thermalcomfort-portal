app.requires.push(
  'ui.router'
);

app.controller('MainCtrl', function($scope,siteList,roomList,stationList) {

  /*siteList controller*/
  if (typeof siteList.getSites() === 'undefined') {
    d3.csv("/qSites/?",function(sites) {
      siteList.setSites(sites);
      $scope.siteList = sites;
    });
  }

  /*roomList controller*/
  if (typeof roomList.getRooms() === 'undefined') {
    d3.csv("/qRooms/?",function(rooms) {
      roomList.setRooms(rooms);
      $scope.roomList = rooms;
    });
  }

  /*roomList controller*/
  if (typeof stationList.getStations() === 'undefined') {
    d3.csv("/qStations/?",function(stations) {
      stationList.setStations(stations);
      $scope.stationList = stations;
    });
  }
});

// routes
app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/home");
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: './static/templates/home.html',
      params: {
        siteid: '',
        homemap: {
          weather:true,
          airqual:true,
          airqmeasure:'Sulfur Dioxide',
          sites:true,
        },
        schools: {
          // year: '',
          // month: '',
          // rwws: '',
          rwmeasures: {
            temp:true,
            relh:true,
            wspd:true,
            solr:true,
            rain:false,
          }
        },
      },
    })
    .state('schools', {
      url: '/schools/:siteid',
      templateUrl: './static/templates/schools.html',
      params: {
        siteid: '',
        homemap: {
          weather:true,
          airqual:true,
          airqmeasure:'Sulfur Dioxide',
          sites:true,
        },
        schools: {
          // year: '',
          // month: '',
          // rwws: '',
          rwmeasures: {
            temp:true,
            relh:true,
            wspd:true,
            solr:true,
            rain:false,
          }
        },
      },
    })
    .state('datatrends', {
      url: '/datatrends',
      templateUrl: './static/templates/datatrends.html',
      params: {
        siteid: '',
        homemap: {
          weather:true,
          airqual:true,
          airqmeasure:'Sulfur Dioxide',
          sites:true,
        },
        schools: {
          // year: '',
          // month: '',
          // rwws: '',
          rwmeasures: {
            temp:true,
            relh:true,
            wspd:true,
            solr:true,
            rain:false,
          }
        },
      },
    })
    .state('keepcool', {
      url: '/keepcool',
      templateUrl: './static/templates/keepcool.html',
      params: {
        siteid: '',
        homemap: {
          weather:true,
          airqual:true,
          airqmeasure:'Sulfur Dioxide',
          sites:true,
        },
        schools: {
          // year: '',
          // month: '',
          // rwws: '',
          rwmeasures: {
            temp:true,
            relh:true,
            wspd:true,
            solr:true,
            rain:false,
          }
        },
      },
    })
});


