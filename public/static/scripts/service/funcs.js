//listen to page load, adjust content-wrapper height
$(window).on('load',resizeContentWrapper);
//listen to window height changes, adjust content-wrapper height
$(window).on('resize',resizeContentWrapper);

function resizeContentWrapper () {
	var winHeight = $(window).height();
	$('#content-wrapper').height(winHeight - 120);
  var winWidth = $(window).width();
  $('#content-wrapper').width(winWidth - 10);
}

function formatDateTime(d) {
  var d = new Date(d)
  var mo = '' + (d.getMonth() + 1)
  var dd = '' + d.getDate()
  var yy = d.getFullYear()
  var hh = '' + d.getHours()
  var mn = '' + d.getMinutes()

  if (mo.length < 2) mo = '0' + mo
  if (dd.length < 2) dd = '0' + dd
  if (hh.length < 2) hh = '0' + hh
  if (mn.length < 2) mn = '0' + mn

  return [yy, mo, dd].join('/') + ' ' + [hh, mn].join(':')
}

function dec2pct(dec) {
  return Math.floor(dec*1000)/1000 + '%'
}

function hex2rgba(hex,opa) {
  return 'rgba('+ parseInt(hex.slice(-6,-4),16)
          + ',' + parseInt(hex.slice(-4,-2),16)
          + ',' + parseInt(hex.slice(-2),16)
          + ',' + opa +')'
}

function formatDate(d) {
  var d = new Date(d),
      mo = '' + (d.getMonth() + 1),
      dd = '' + d.getDate(),
      yyyy = d.getFullYear();

  if (mo.length < 2) mo = '0' + mo;
  if (dd.length < 2) dd = '0' + dd;

  return [yyyy, mo, dd].join('-');
}

function formatDateDash(d) {
  var d = new Date(d),
      mo = '' + (d.getMonth() + 1),
      dd = '' + d.getDate(),
      yyyy = d.getFullYear();

  if (mo.length < 2) mo = '0' + mo;
  if (dd.length < 2) dd = '0' + dd;

  return [yyyy, mo, dd].join('/');
}

function formatTime(d) {
  var t = new Date(d),
      hh = '' + d.getHours(),
      mn = '' + d.getMinutes();

  if (hh.length < 2) hh = '0' + hh;
  if (mn.length < 2) mn = '0' + mn;

  return [hh, mn].join(':');
}

function formatDateTime(d) {
  var d = new Date(d);
  return [formatDate(d),formatTime(d)].join(' ')
}


function formatDateTimeMatch(d) {
  var d = new Date(d),
      hh = '' + d.getHours(),
      mn = '' + d.getMinutes();
      mo = '' + (d.getMonth() + 1),
      dd = '' + d.getDate(),
      yyyy = d.getFullYear();

  if (hh.length < 2) hh = '0' + hh;
  if (mn.length < 2) mn = '0' + mn;
  if (mo.length < 2) mo = '0' + mo;
  if (dd.length < 2) dd = '0' + dd;

  return [yyyy, mo, dd, hh, mn].join('');
}

function formatDateTimeMatchWithSeconds(d) {
  var d = new Date(d),
      ss = '' + d.getSeconds();
      mn = '' + d.getMinutes();
      hh = '' + d.getHours(),
      mo = '' + (d.getMonth() + 1),
      dd = '' + d.getDate(),
      yyyy = d.getFullYear();

  if (ss.length < 2) ss = '0' + ss;
  if (mn.length < 2) mn = '0' + mn;
  if (hh.length < 2) hh = '0' + hh;
  if (mo.length < 2) mo = '0' + mo;
  if (dd.length < 2) day = '0' + dd;

  return [yyyy, mo, dd, hh, mn, ss].join('');
}

function formatDateMatch(d) {
  var d = new Date(d),
      mo = '' + (d.getMonth() + 1),
      dd = '' + d.getDate(),
      yyyy = d.getFullYear();

  if (mo.length < 2) mo = '0' + mo;
  if (dd.length < 2) dd = '0' + dd;

  return [yyyy, mo, dd].join('');
}

function daysDiff(d1,d2) {
  return Math.round(Math.abs((new Date(d1).getTime() - new Date(d2).getTime())/(24*60*60*1000)))
}

function roundTo(num, interval) {
    return Math.round(num / interval) * interval;
}

function distance(lat1, lon1, lat2, lon2, unit) {
  var radlat1 = Math.PI * lat1/180
  var radlat2 = Math.PI * lat2/180
  var theta = lon1-lon2
  var radtheta = Math.PI * theta/180
  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist)
  dist = dist * 180/Math.PI
  dist = dist * 60 * 1.1515
  if (unit=="K") { dist = dist * 1.609344 }
  if (unit=="N") { dist = dist * 0.8684 }
  return dist
}