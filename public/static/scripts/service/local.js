app.service('siteList', function() {
  this.data = {}
  this.get = function(key) {
    return this.data[key]
  }
  this.set = function(key, value) {
    this.data[key] = value
  }
  this.setSites = function(sites) {
    this.set('sites',sites)
  }
  this.getSites = function(cb) {
    if (cb) {
      cb(this.get('sites'))
    }
    return this.get('sites')
  }
})

app.service('roomList', function() {

  this.data = {};

  this.get = function(key) {
    return this.data[key];
  };

  this.set = function(key, value) {
    this.data[key] = value;
  };

  this.setRooms = function(rooms) {
    this.set('rooms', rooms);
  }

  this.getRooms = function(cb) {
    if (cb) {
      cb(this.get('rooms'));
    }
    return this.get('rooms');
  }
})

app.service('stationList', function() {

  this.data = {};

  this.get = function(key) {
    return this.data[key];
  };

  this.set = function(key, value) {
    this.data[key] = value;
  };

  this.setStations = function(stations) {
    this.set('stations', stations);
  }

  this.getStations = function(cb) {
    if (cb) {
      cb(this.get('stations'));
    }
    return this.get('stations');
  }
})