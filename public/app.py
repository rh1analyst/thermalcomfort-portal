from flask import Flask, jsonify
from flask import render_template
from flask import request, redirect
from flask import Response
from flask_wtf import Form

import rh1mod

app = Flask(__name__) #create a variable to store an instance of the Flask class

@app.route("/")
def index():
    return(render_template("index.html"))

#SIMPLE RECORDS PULL
@app.route("/qSites/")
def qSites():
    return rh1mod.qSites()

@app.route("/qRooms/")
def qRooms():
    return rh1mod.qRooms()

@app.route("/qStations/")
def qStations():
    return rh1mod.qStations()

#CURRENT STATION CONDITIONS (ALL)
@app.route("/qAirQualStations/")
def qAirQualStations():
    return rh1mod.qAirQualStations(request.args)

#CURRENT SITE CONDITIONS (ONE)
@app.route("/qSiteWeather/")
def qSiteWeather():
    return rh1mod.qSiteWeather(request.args)

#RECENTWEATHER (ONE)
@app.route("/qSiteWeatherSpan/")
def qSiteWeatherSpan():
    return rh1mod.qSiteWeatherSpan(request.args)

@app.route("/qStationWeatherSpan/")
def qStationWeatherSpan():
    return rh1mod.qStationWeatherSpan(request.args)

##LIST NEARBY STATIONS LIST##
@app.route("/qNearbyWeatherStations/")
def qNearbyWeatherStations():
    return rh1mod.qNearbyWeatherStations(request.args)

##EXPORT DATA##
@app.route("/qSiteWeatherFile/")
def qSiteWeatherFile():
    return rh1mod.qSiteWeatherFile(request.args)

@app.route("/qStationWeatherFile/")
def qStationWeatherFile():
    return rh1mod.qStationWeatherFile(request.args)

##CLASSROOM PROFILES##
@app.route("/qClassroomProfileData/")
def qClassroomProfileData():
    return rh1mod.qClassroomProfileData(request.args)

@app.route("/qClassroomProfileStats/")
def qClassroomProfileStats():
    return rh1mod.qClassroomProfileStats(request.args)

@app.route("/qRoomFile/")
def qRoomFile():
    return rh1mod.qRoomFile(request.args)

##DATATRENDS##
@app.route("/qSpanProfileData/")
def qSpanProfileData():
    return rh1mod.qSpanProfileData(request.args)

@app.route("/qSpanProfileStats/")
def qSpanProfileStats():
    return rh1mod.qSpanProfileStats(request.args)

@app.route("/qDataExtents/")
def qDataExtents():
    return rh1mod.qDataExtents(request.args)

@app.route("/qGroupFile/")
def qGroupFile():
    return rh1mod.qGroupFile(request.args)

##AQSTATION INFO
@app.route("/qClosestAqStation/")
def qClosestAqStation():
    return rh1mod.qClosestAqStation(request.args)

@app.route("/qAqStationData/")
def qAqStationData():
    return rh1mod.qAqStationData(request.args)

#######
# @app.route("/qOutdoorMonthProfile/")
# def qOutdoorMonthProfile():
#     return rh1mod.qOutdoorMonthProfile(request.args)

# @app.route("/qRoomMonthStats/")
# def qRoomMonthStats():
#     return rh1mod.qRoomMonthStats(request.args)

# @app.route("/qOutdoorMonthStats/")
# def qOutdoorMonthStats():
#     return rh1mod.qOutdoorMonthStats(request.args)

# #DATATRENDS
# @app.route("/qRoomSpanProfile/")
# def qRoomSpanProfile():
#     return rh1mod.qRoomSpanProfile(request.args)

# @app.route("/qRoomSpanStats/")
# def qRoomSpanStats():
#     return rh1mod.qRoomSpanStats(request.args)

# @app.route("/qOutdoorSpanProfile/")
# def qOutdoorSpanProfile():
#     return rh1mod.qOutdoorSpanProfile(request.args)

# @app.route("/qOutdoorSpanStats/")
# def qOutdoorSpanStats():
#     return rh1mod.qOutdoorSpanStats(request.args)






# @app.route("/qRoomData/")
# def qRoomData():
#     return rh1mod.qRoomData(request.args)

# @app.route("/qWeatherData/")
# def qWeatherData():
#     return rh1mod.qWeatherData(request.args)

# @app.route("/qWuDayData0/")
# def qWuDayData0():
# 	return rh1mod.qWuDayData0(request.args)

# @app.route("/qWuDayData1/")
# def qWuDayData1():
# 	return rh1mod.qWuDayData1(request.args)

# @app.route("/qWeatherData2/")
# def qWeatherData2():
# 	return rh1mod.qWeatherData2(request.args)

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5006,debug=True,threaded=True)