import numpy as np
import datetime as dt
import psycopg2 as pg
import pandas as pd

import requests
import csv
import io
import sys
import os
import time
import pytz
import math
import geopy.distance


##CONNECTION STRING COMPONENTS##
rs = ['rh1-data.cgptfmcyizib.us-west-2.redshift.amazonaws.com','admin',5439,'4AdaPT118899-DAPt4','devimport']
wu = ['e5d6ceeb92759dce','a44c9c62154dd40b','852099ae1c4737ff','9d8af6f0a16b64dd','e3580ed250dbc895','16d134a486638563']

##HELPERS##
def daterange(start_date,end_date):
    for n in range(int((end_date - start_date).days) + 1):
        date = start_date + dt.timedelta(n)
        yield date

def weekdayct(start_date,end_date):
    t = 0
    for n in range(int((end_date - start_date).days) + 1):
        date = start_date + dt.timedelta(n)
        if (date.weekday() != 0) & (date.weekday() != 6): 
            t = t + 1 
    return t

def calcdist(lat1,lat2,lng1,lng2):
    lat1 = math.radians(float(lat1))
    lat2 = math.radians(float(lat2))
    lng1 = math.radians(float(lng1))
    lng2 = math.radians(float(lng2))
    x = (lng2 - lng1)*math.cos((lat1 + lat2)/2.0)
    y = (lat2 - lat1)
    d = math.sqrt(x * x + y * y) * 6371.0
    return d

def isnegative(s):
    try:
        float(s)
        if (float(s) < -360):
            return True
        else:
            return False
    except:
        return False

def wurequest0(wsid,date,x):
    yyyy,mm,dd = date.split('-')
    time.sleep(3)
    url = 'http://api.wunderground.com/api/' + wu[x] + '/history_' + yyyy + mm + dd + '/q/' + wsid + '.json'
    req = requests.get(url=url)
    try:
        r = json.loads(req.content.decode('utf-8'))
        if r.get('history'):
            r = r['history']['observations']
            res = []
            for row in r:
                res.append([
                    dt.datetime(int(row['date']['year']),int(row['date']['mon']),int(row['date']['mday']),int(row['date']['hour']),int(row['date']['min'])),
                    row['dewpti'],
                    row['tempi'],
                    row['wdird'] if row['wdird'] != 'N/A' else -999.0,
                    row['wdire'] if row['wdire'] != 'N/A' else '',
                    row['wspdi'] if row['wspdi'] != 'N/A' else -999.0,
                    row['wgusti'] if row['wgusti'] != 'N/A' else -999.0,
                    row['hum'] if row['hum'] != 'N/A' else -999.0,
                    row['precipi'] if row['precipi'] != 'N/A' else -999.0,
                    -999.0,
                    -999.0])
            res = pd.DataFrame(res,columns=['dt_local','dewpt_f','temp_f','winddir_deg','winddir_str','windspd_mph','gustspd_mph','relhum_pct','precip_inchesperhr','precip_inchesaccum','solrad_wpms'])
            res = res[['dt_local','dewpt_f','relhum_pct','temp_f','windspd_mph','gustspd_mph','winddir_deg','winddir_str','precip_inchesperhr','precip_inchesaccum','solrad_wpms']]
            return res.tail(n=1)
        else: 
            if x == len(wu)-1: x = 0
            else: x +=1
            time.sleep(4)
            return wurequest0(wsid,date,x)
    except:
        print('bounce error')
        time.sleep(5)
        return wurequest0(wsid,date,x)

def wurequest1(wsid,date):
    yyyy,mm,dd = date.split('-')
    url = 'https://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=' + wsid + '&day=' + dd + "&month="+ mm + "&year=" + yyyy + '&graphspan=day&format=1'
    res = requests.get(url).content
    res = pd.read_csv(io.StringIO(res.decode('utf-8')),index_col=False)
    res = res.dropna(0,thresh=5)
    res = res.reindex(columns=('Time','DewpointF','Humidity','TemperatureF','WindSpeedMPH','WindSpeedGustMPH','WindDirectionDegrees','WindDirection','HourlyPrecipIn','dailyrainin','SolarRadiationWatts/m^2'))
    res.columns = ['dt_local','dewpt_f','relhum_pct','temp_f','windspd_mph','gustspd_mph','winddir_deg','winddir_str','precip_inchesperhr','precip_inchesaccum','solrad_wpms']
    return res.tail(n=1)

##LOCAL SERVICE##
def qSites():
    #fetch site list
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    quer = """SELECT * FROM hidoe_sites"""
    curs.execute(quer)
    data = curs.fetchall()
    rs_conn.close()
    #format, return data
    sites = pd.DataFrame(data,columns=['site_id','site_nm_long','site_nm_shrt','lat','lng','island','district','complex','climate'])
    return sites.to_csv(sep=',',index=False)

def qRooms():
    #fetch rooms list
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    quer = """SELECT site_id, room_id, room_nm, type FROM hidoe_rooms"""
    curs.execute(quer)
    data = curs.fetchall()
    rs_conn.close()
    #format, return data
    rooms = pd.DataFrame(data,columns=['site_id','room_id','room_nm','type'])
    return rooms.to_csv(sep=',',index=False)

def qStations():
    #fetch rooms list
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    quer = """SELECT id, location FROM hidoe_stations2"""
    curs.execute(quer)
    data = curs.fetchall()
    rs_conn.close()
    #format, return data
    stations = pd.DataFrame(data,columns=['ws_id','ws_loc'])
    stations['ws_dist'] = 'XX'
    stations['compl'] = 'XX'
    return stations.to_csv(sep=',',index=False)


##CURRENT STATION CONDITIONS (ALL)##
def qAirQualStations(args):
    #process args
    param = str(args.get('param'))
    #thresholds
    thresholds = [
        {'descr':'GOOD','hexcol':'#00E400','minv':0,'maxv':50},
        {'descr':'MODERATE','hexcol':'#FFFF00','minv':51,'maxv':100},
        {'descr':'UNHEALTHY FOR SENSITIVE GROUPS','hexcol':'#FF7E00','minv':101,'maxv':150},
        {'descr':'UNHEALTHY','hexcol':'#FF0000','minv':151,'maxv':200},
        {'descr':'VERY UNHEALTHY','hexcol':'#99004C','minv':201,'maxv':300},
        {'descr':'HAZARDOUS','hexcol':'#7E0023','minv':301,'maxv':99999999},
        {'descr':'NO DATA','hexcol':'#A9A9A9','minv':-999,'maxv':-1}]
    if param == 'Sulfur Dioxide':
        fields = ['so2_ppb','so2_aqi']
        obj = {
            'descr0':'Sulfur Dioxide (SO',
            'descr1':'2',
            'descr2':')',
            'units0':'ppb',
            'units1':'',
            'method':'3-hour peak'}
    elif param == 'Particulate Matter 2.5':
        fields = ['pm2pt5_ugperm3','pm2pt5_aqi']
        obj = {
            'descr0':'Particulate Matter 2.5 (PM',
            'descr1':'2.5',
            'descr2':')',
            'units0':'μg/m',
            'units1':'3',
            'method':'3-hour peak'}
    elif param == 'Particulate Matter 10':
        fields = ['pm10_ugperm3','pm10_aqi']
        obj = {
            'descr0':'Particulate Matter 10 (PM',
            'descr1':'10',
            'descr2':')',
            'units0':'μg/m',
            'units1':'3',
            'method':'3-hour peak'}
    elif param == 'Carbon Monoxide':
        fields = ['co_ppm','co_aqi']
        obj = {
            'descr0':'Carbon Monoxide (CO)',
            'descr1':'',
            'descr2':'',
            'units0':'ppm',
            'units1':'',
            'method':'3-hour peak'}
    elif param == 'Ozone':
        fields = ['o3_ppb','o3_aqi']
        obj = {
            'descr0':'Ozone (O',
            'descr1':'3',
            'descr2':')',
            'units0':'ppb',
            'units1':'',
            'method':'3-hour peak'}
    else:
        print('error....... invalid parameter')

    #fetch list of aq stations
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    quer = """SELECT id,lat,lng,location FROM hidoe_stations2 WHERE type = 'airqual'"""
    curs.execute(quer)
    stations = pd.DataFrame(curs.fetchall(),columns=['id','lat','lng','location'])
    #fetch last batch of aqis and measurements for requested param
    quer = """SELECT MAX(dt_local) FROM hidoe_airquality"""
    curs.execute(quer)
    last = (curs.fetchall()[0][0] - dt.timedelta(minutes = 180))
    #...aqis
    quer = """SELECT DISTINCT(id), CAST(MAX({}) as int) FROM hidoe_airquality WHERE dt_local > '{}' GROUP BY 1 ORDER BY 1 DESC"""
    curs.execute(quer.format(fields[1],last))
    aqis = pd.DataFrame(curs.fetchall(),columns=['id','aqi'])
    #...measures
    quer = """SELECT id, CAST({} as int), {} FROM hidoe_airquality WHERE dt_local > '{}'  ORDER BY 3 DESC"""
    curs.execute(quer.format(fields[1],fields[0],last))
    msrs = pd.DataFrame(curs.fetchall(),columns=['id','aqi','value'])
    rs_conn.close()

    #format, return data
    data = pd.merge(aqis,msrs,on=['id','aqi'],how='left')
    data.drop_duplicates(subset=['id'],keep='first',inplace=True)
    data = pd.merge(stations,data,on=['id'],how='left')
    response = []
    for i,d in data.iterrows():
        r = []
        if (str(d.aqi) == 'nan') | (d.aqi < 0):
            r = [i,d['id'],d['lat'],d['lng'],d['location'],last + dt.timedelta(minutes=180),'NO DATA','NO DATA',obj['units0'],obj['units1'],obj['descr0'],obj['descr1'],obj['descr2'],thresholds[6]['hexcol'],'']
        else:
            j = 0
            while r == []:
                if (int(d['aqi']) >= thresholds[j]['minv']) & (int(d['aqi']) <= thresholds[j]['maxv']):
                    r = [i,d['id'],d['lat'],d['lng'],d['location'],last + dt.timedelta(minutes=180),d['value'],d['aqi'],obj['units0'],obj['units1'],obj['descr0'],obj['descr1'],obj['descr2'],thresholds[j]['hexcol'],obj['method']]
                j = j+1
        response.append(r)
    return pd.DataFrame(response,columns=['num','asta_id','lat','lng','location','dt_local','value','aqi','units0','units1','descr0','descr1','descr2','hexcol','method']).to_csv(sep=',',index=False)

##CURRENT STATION CONDITIONS (ONE)##
def qSiteWeather(args):
    #extract args
    siteid = str(args.get('siteid'))
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch last date uploaded to redshift
    quer = """SELECT MAX(dt_local) FROM hidoe_siteweather2 WHERE site_id = '{}'"""
    curs.execute(quer.format(siteid))
    resp = curs.fetchone()[0]
    #configure return content
    if resp != None:
        #assess whether elapsed time since last update is less than 90 minutes
        last = pytz.timezone('US/Hawaii').localize(resp)
        last = time.mktime(resp.timetuple())
        nowt = dt.datetime.now(pytz.timezone('US/Hawaii'))
        nowt = time.mktime(nowt.timetuple())
        # print((nowt - last)/60)
        if int(nowt - last)/60 < 36000:
            #fetch wsid from last record
            quer = """ 
                   SELECT ws_id, ws_loc, ws_dist, dt_local, temp_f, relhum_pct, windspd_mph, winddir_str, winddir_deg, dewpt_f, solrad_wpms, precip_inchesperhr, precip_inchesaccum
                   FROM hidoe_siteweather2 
                   WHERE site_id = '{}' and dt_local = '{}'"""
            curs.execute(quer.format(siteid,resp))
            data = curs.fetchall()
            wsta = data[0]
            #fetch full record from weather underground
            if len(wsta[0]) == 4:
                data = wurequest0(wsta[0],dt.datetime.now().strftime('%Y-%m-%d'),0)
            else:
                data = wurequest1(wsta[0],dt.datetime.now().strftime('%Y-%m-%d'))
            data['ws_id'] = wsta[0]
            data['ws_loc'] = wsta[1]
            data['ws_dist'] = wsta[2]
            df = data[['ws_id','ws_loc','ws_dist','dt_local','temp_f','relhum_pct','windspd_mph','winddir_str','winddir_deg','dewpt_f','solrad_wpms','precip_inchesperhr','precip_inchesaccum']]
            #close rs connection
            rs_conn.close()
            return df.to_csv(sep=',',index=False)
        else:
            #close rs connection
            rs_conn.close()
            return 'None'
    else:
        #close rs connection
        rs_conn.close()
        return 'None'

##RECENT WEATHER SPAN (ONE)##
def qSiteWeatherSpan(args):
    #extract args
    siteid = str(args.get('siteid'))
    s = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    e = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    span = int(args.get('span'))
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch site weather data (aggregate based on span length)
    if span <= 5: 
        select = 'SELECT dt_local as dt, temp_f as temp, relhum_pct as relh, windspd_mph as wspd, solrad_wpms as solr, precip_inchesperhr as rain'
        group = ''
    elif span <= 60:
        select = 'SELECT date_trunc(\'hour\',dt_local) as dt, MIN(temp_f) as temp_min, AVG(temp_f) as temp, MAX(temp_f) as temp_max, MIN(relhum_pct) as relh_min, AVG(relhum_pct) as relh, MAX(relhum_pct) as relh_max, MIN(windspd_mph) as wspd_min, AVG(windspd_mph) as wspd, MAX(windspd_mph) as wspd_max, MIN(solrad_wpms) as solr_min, AVG(solrad_wpms) as solr, MAX(solrad_wpms) as solr_max, MIN(precip_inchesperhr) as rain_min, AVG(precip_inchesperhr) as rain, MAX(precip_inchesperhr) as rain_max'
        group = 'GROUP BY 1'
    else: 
        select = 'SELECT date_trunc(\'day\',dt_local) as dt, MIN(temp_f) as temp_min, AVG(temp_f) as temp, MAX(temp_f) as temp_max, MIN(relhum_pct) as relh_min, AVG(relhum_pct) as relh, MAX(relhum_pct) as relh_max, MIN(windspd_mph) as wspd_min, AVG(windspd_mph) as wspd, MAX(windspd_mph) as wspd_max, MIN(solrad_wpms) as solr_min, AVG(solrad_wpms) as solr, MAX(solrad_wpms) as solr_max, MIN(precip_inchesperhr) as rain_min, AVG(precip_inchesperhr) as rain, MAX(precip_inchesperhr) as rain_max'
        group = 'GROUP BY 1'
    quer = """
        {}
        FROM hidoe_siteweather2
        WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}'
        {}
        ORDER BY 1 DESC
        """
    curs.execute(quer.format(select,siteid,s,e,group))
    resp = curs.fetchall()
    if span < 5:
        data = pd.DataFrame(resp,columns=['dt','temp','relh','wspd','solr','rain'])
    else:
        data = pd.DataFrame(resp,columns=['dt','temp_min','temp','temp_max','relh_min','relh','relh_max','wspd_min','wspd','wspd_max','solr_min','solr','solr_max','rain_min','rain','rain_max'])
    #close rs connection
    rs_conn.close()
    data.drop_duplicates(subset=['dt'],keep='first',inplace=True)
    return data.to_csv(sep=',',index=False)

def qStationWeatherSpan(args):
    #extract args
    wsid = str(args.get('wsid'))
    s = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    e = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    span = int(args.get('span'))
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch site weather data (aggregate based on span length)
    if span <= 5: 
        select = 'SELECT dt_local as dt, temp_f as temp, relhum_pct as relh, windspd_mph as wspd, solrad_wpms as solr, precip_inchesperhr as rain'
        group = ''
    elif span <= 60:
        select = 'SELECT date_trunc(\'hour\',dt_local) as dt, MIN(temp_f) as temp_min, AVG(temp_f) as temp, MAX(temp_f) as temp_max, MIN(relhum_pct) as relh_min, AVG(relhum_pct) as relh, MAX(relhum_pct) as relh_max, MIN(windspd_mph) as wspd_min, AVG(windspd_mph) as wspd, MAX(windspd_mph) as wspd_max, MIN(solrad_wpms) as solr_min, AVG(solrad_wpms) as solr, MAX(solrad_wpms) as solr_max, MIN(precip_inchesperhr) as rain_min, AVG(precip_inchesperhr) as rain, MAX(precip_inchesperhr) as rain_max'
        group = 'GROUP BY 1'
    else: 
        select = 'SELECT date_trunc(\'day\',dt_local) as dt, MIN(temp_f) as temp_min, AVG(temp_f) as temp, MAX(temp_f) as temp_max, MIN(relhum_pct) as relh_min, AVG(relhum_pct) as relh, MAX(relhum_pct) as relh_max, MIN(windspd_mph) as wspd_min, AVG(windspd_mph) as wspd, MAX(windspd_mph) as wspd_max, MIN(solrad_wpms) as solr_min, AVG(solrad_wpms) as solr, MAX(solrad_wpms) as solr_max, MIN(precip_inchesperhr) as rain_min, AVG(precip_inchesperhr) as rain, MAX(precip_inchesperhr) as rain_max'
        group = 'GROUP BY 1'
    quer = """
        {}
        FROM hidoe_wsweather2
        WHERE ws_id = '{}' and dt_local >= '{}' and dt_local <= '{}'
        {}
        ORDER BY 1 DESC
        """
    curs.execute(quer.format(select,wsid,s,e,group))
    resp = curs.fetchall()
    if span < 5:
        data = pd.DataFrame(resp,columns=['dt','temp','relh','wspd','solr','rain'])
    else:
        data = pd.DataFrame(resp,columns=['dt','temp_min','temp','temp_max','relh_min','relh','relh_max','wspd_min','wspd','wspd_max','solr_min','solr','solr_max','rain_min','rain','rain_max'])
    #close rs connection
    rs_conn.close()
    data.drop_duplicates(subset=['dt'],keep='first',inplace=True)
    return data.to_csv(sep=',',index=False)

##LIST NEARBY WEATHER STATIONS##
def qNearbyWeatherStations(args):
    #extract args
    siteid = str(args.get('siteid'))
    island = str(args.get('island'))
    lat = str(args.get('lat'))
    lng = str(args.get('lng'))
    start = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    end = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    nowt = dt.datetime.now(pytz.timezone('US/Hawaii'))
    xhrs = (24 - nowt.hour) * 2
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch island's weatherstations
    quer = """SELECT id,location,lat,lng
              FROM hidoe_stations2
              WHERE island = '{}' and type = 'weather'"""
    curs.execute(quer.format(island))
    istas = curs.fetchall()
    #determine if station is in range
    nstas = []
    for ista in istas:
        #build station object with distance relative to current site
        dist = geopy.distance.vincenty((lat,lng),(ista[2],ista[3])).km
        if siteid == '402':
            if dist < 15:
                station = [ista[0],ista[1],dist]
                nstas.append(station)
        else:
            if dist < 10:
                station = [ista[0],ista[1],dist]
                nstas.append(station)

    #fetch chunked data from 'hidoe_siteweather' assess completion
    quer = """SELECT DISTINCT ws_id, date_trunc('hour',dt_local), EXTRACT(minute from dt_local)::int/60
              FROM hidoe_siteweather2
              WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
              GROUP BY 1,2,3"""
    curs.execute(quer.format(siteid,start,end))
    comp = pd.DataFrame(curs.fetchall(),columns=('ws_id','h','m'))
    srcs = pd.unique(comp['ws_id'])
    pctg = float(len(comp.index))/float(((end-start).days + 1.0)*24.0 - xhrs)
    #fetch chunked data from 'hidoe_wsweather' assess completion
    quer = """SELECT DISTINCT ws_id, date_trunc('hour',dt_local), EXTRACT(minute from dt_local)::int/60
              FROM hidoe_wsweather2
              WHERE dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
              GROUP BY 1,2,3"""
    curs.execute(quer.format(start,end))
    indv = pd.DataFrame(curs.fetchall(),columns=['ws_id','dt_local','min_offset'])
    indv = indv.groupby(['ws_id']).size().reset_index(name='counts')
    indv['data_compl'] = (indv['counts']/(float((end-start).days + 1.0)*24.0 - xhrs)).astype(float)
    rs_conn.close()
    #merge responses
    resp = pd.merge(pd.DataFrame(nstas,columns=['ws_id','ws_loc','ws_dist']),indv,on=['ws_id'],how='left')
    resp['sources'] = ''
    print(srcs)
    resp = pd.concat([resp,pd.DataFrame([['Composite','(' + str(len(srcs)) + ' Locations)','','',pctg,'|'.join(srcs)]],columns=('ws_id','ws_loc','ws_dist','counts','data_compl','sources'))])
    resp = resp[['ws_id','ws_loc','ws_dist','data_compl','sources']]
    return resp.to_csv(sep=',',index=False)
# def qNearbyWeatherStations(args):
#     #extract args
#     siteid = str(args.get('siteid'))
#     start = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
#     end = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
#     nowt = dt.datetime.now(pytz.timezone('US/Hawaii'))
#     xhrs = (24 - nowt.hour) * 2
#     #connect to redshift
#     rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
#     curs = rs_conn.cursor()
#     #fetch all weatherstations contributing to composite dataset
#     quer = """SELECT DISTINCT ws_id,ws_loc,ws_dist 
#               FROM hidoe_siteweather2
#               WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
#               ORDER BY 2"""
#     curs.execute(quer.format(siteid,start,end))
#     wlst = curs.fetchall()
#     #fetch chunked data from 'hidoe_siteweather' assess completion
#     quer = """SELECT DISTINCT date_trunc('hour',dt_local), EXTRACT(minute from dt_local)::int/30
#               FROM hidoe_siteweather2
#               WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
#               GROUP BY 1,2"""
#     curs.execute(quer.format(siteid,start,end))
#     comp = float(len(curs.fetchall()))/float(((end-start).days + 1.0)*48.0 - xhrs)
#     #fetch chunked data from 'hidoe_siteweather' assess completion
#     quer = """SELECT DISTINCT ws_id, date_trunc('hour',dt_local), EXTRACT(minute from dt_local)::int/30
#               FROM hidoe_wsweather2
#               WHERE dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
#               GROUP BY 1,2,3"""
#     curs.execute(quer.format(start,end))
#     indv = pd.DataFrame(curs.fetchall(),columns=['ws_id','dt_local','min_offset'])
#     indv = indv.groupby(['ws_id']).size().reset_index(name='counts')
#     indv['data_compl'] = (indv['counts']/(float((end-start).days + 1.0)*48.0 - xhrs)).astype(float)
#     rs_conn.close()
#     #merge responses
#     resp = pd.merge(pd.DataFrame(wlst,columns=['ws_id','ws_loc','ws_dist']),indv,on=['ws_id'],how='left')
#     resp = pd.concat([resp,pd.DataFrame([['Composite','(' + str(resp.shape[0]) + ' Locations)','','',comp]],columns=['ws_id','ws_loc','ws_dist','counts','data_compl'])])
#     resp = resp[['ws_id','ws_loc','ws_dist','data_compl']]
#     return resp.to_csv(sep=',',index=False)

##DOWNLOAD RECENT WEATHER##
def qSiteWeatherFile(args):
    #extract args
    siteid = str(args.get('siteid'))
    s = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    e = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch site weather data
    quer = """
        SELECT ws_id, ws_loc, ws_dist, dt_local, temp_f, dewpt_f, relhum_pct, windspd_mph, gustspd_mph, winddir_deg, precip_inchesperhr, precip_inchesaccum, solrad_wpms
        FROM hidoe_siteweather2
        WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
        """
    curs.execute(quer.format(siteid,s,e))
    resp = pd.DataFrame(curs.fetchall(),columns=['ws_id','ws_loc','ws_dist','timestamp','temp','dewpt','relhum','windspd','gustspd','winddir','preciprate','preciptotal','solrad'])
    #fetch station lat-lng
    quer = """SELECT id,lat,lng FROM hidoe_stations2"""
    curs.execute(quer)
    stas = pd.DataFrame(curs.fetchall(),columns=['ws_id','lat','lng'])
    #close rs connection
    rs_conn.close()
    #return data
    data = pd.merge(resp,stas,on=['ws_id'],how='left')
    data.drop_duplicates(subset=['timestamp'],keep='first',inplace=True)
    return data.to_csv(sep=',',index=False)

def qStationWeatherFile(args):
    #extract args
    wsid = str(args.get('wsid'))
    s = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    e = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch station weather data
    quer = """
        SELECT ws_id, ws_loc, dt_local, temp_f, dewpt_f, relhum_pct, windspd_mph, gustspd_mph, winddir_deg, precip_inchesperhr, precip_inchesaccum, solrad_wpms
        FROM hidoe_wsweather2
        WHERE ws_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and temp_f > 0
        """
    curs.execute(quer.format(wsid,s,e))
    resp = pd.DataFrame(curs.fetchall(),columns=['ws_id','ws_loc','timestamp','temp','dewpt','relhum','windspd','gustspd','winddir','preciprate','preciptotal','solrad'])
    #fetch station lat-lng
    quer = """SELECT id,lat,lng FROM hidoe_stations2"""
    curs.execute(quer)
    stas = pd.DataFrame(curs.fetchall(),columns=['ws_id','lat','lng'])
    #close rs connection
    rs_conn.close()
    #return data
    data = pd.merge(resp,stas,on=['ws_id'],how='left')
    data.drop_duplicates(subset=['timestamp'],keep='first',inplace=True)
    return data.to_csv(sep=',',index=False)

##CLASSROOM PROFILES##
def qClassroomProfileData(args):
    #extract args
    siteid = str(args.get('siteid'))
    srmids = str(args.get('srmids')).split('|')
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch room profile data from 'hidoe_interiors'
    quer = """SELECT DISTINCT room_id, date_trunc('month',dt_local), EXTRACT(hour from dt_local), EXTRACT(minute from dt_local)::int/60, AVG(temp_f), COUNT(temp_f)
              FROM hidoe_interiors
              WHERE site_id = '{}'
              GROUP BY 1,2,3,4"""
    curs.execute(quer.format(siteid))
    crdata = pd.DataFrame(curs.fetchall(),columns=('room_id','ym','h','m','temp_f','counts'))
    #fetch outdoor profile data from 'hidoe_siteweather'
    quer = """SELECT DISTINCT date_trunc('month',dt_local), EXTRACT(hour from dt_local), EXTRACT(minute from dt_local)::int/60, AVG(temp_f), COUNT(temp_f)
              FROM hidoe_siteweather2
              WHERE site_id = '{}'
              GROUP BY 1,2,3"""
    curs.execute(quer.format(siteid))
    wsdata = pd.DataFrame(curs.fetchall(),columns=('ym','h','m','temp_f','counts'))
    wsdata['room_id'] = 'Outdoor'
    #concatenate cr and ws profile data
    xdata = pd.concat([crdata,wsdata])
    xdata['year_i'] = pd.to_datetime(xdata['ym']).dt.year
    xdata['month_i'] = pd.to_datetime(xdata['ym']).dt.month - 1
    xdata['time_i'] = (xdata['h'] + xdata['m']/2)*2
    xdata['time_str'] = xdata['h'].map(str) + ':' + ((xdata['m']*3).map(int)).map(str) + '0'
    #fetch start, end dates from 'hidoe_interiors'
    quer = """SELECT MIN(dt_local), MAX(dt_local) from hidoe_interiors WHERE site_id = '{}'"""
    curs.execute(quer.format(siteid))
    ends = curs.fetchall()[0]
    rs_conn.close()
    #substantiate final dataframe
    fdata = pd.DataFrame(columns=['room_id','year_i','month_i','time_i','temp_f'])
    if ends[0] != None:
        #loop months between start and end
        s = 12 * ends[0].year + ends[0].month 
        e = 12 * ends[1].year + ends[1].month
        for ym in range(s,e):
            y,m = divmod(ym,12)
            ymdata = pd.DataFrame(columns=['room_id','year_i','month_i','time_i','temp_f'])
            #loop rooms in set
            for room in srmids:
                #assess whether sufficient data exists to summarize month
                vdata = xdata.loc[(xdata['year_i'] == y) & (xdata['month_i'] == m) & (xdata['counts'] > 10) & (xdata['room_id'] == room)]
                if len(vdata) == 24 or len(vdata) == 48:
                    ymdata = pd.concat([ymdata,vdata])
            #test if this month has any cr profiles
            if len(ymdata.index) > 0:
                vdata = xdata.loc[(xdata['year_i'] == y) & (xdata['month_i'] == m) & (xdata['counts'] > 10) & (xdata['room_id'] == 'Outdoor')]
                if len(vdata) == 24 or len(vdata) == 48:
                    ymdata = pd.concat([ymdata,vdata])
            fdata = pd.concat([fdata,ymdata])
    #return formatted data
    if len(fdata.index) > 0:
        fdata = fdata[['room_id','time_str','year_i','month_i','time_i','temp_f']]
        return fdata.to_csv(sep=',',index=False)
    else: 
        return 'None'

def qClassroomProfileStats(args):
    #extract args
    siteid = str(args.get('siteid'))
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch room profile data from 'hidoe_interiors'
    quer = """SELECT room_id, date_trunc('month',dt_local), MIN(temp_f), AVG(temp_f), MAX(temp_f), STDDEV_POP(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
              FROM hidoe_interiors
              WHERE site_id = '{}' and EXTRACT(DOW from dt_local) != 0 and EXTRACT(DOW from dt_local) != 6 and EXTRACT(HOUR from dt_local) >= 8 and EXTRACT(HOUR from dt_local) < 14
              GROUP BY 1,2"""
    curs.execute(quer.format(siteid))
    #format response
    crstats = pd.DataFrame(curs.fetchall(),columns=('room_id','ym','temp_min','temp_avg','temp_max','temp_std','temp_n','temp_o85','temp_o90'))
    #fetch outdoor profile data from 'hidoe_siteweather'
    quer = """SELECT date_trunc('month',dt_local), MIN(temp_f), AVG(temp_f), MAX(temp_f), STDDEV_POP(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
              FROM hidoe_siteweather2
              WHERE site_id = '{}' and EXTRACT(DOW from dt_local) != 0 and EXTRACT(DOW from dt_local) != 6 and EXTRACT(HOUR from dt_local) >= 8 and EXTRACT(HOUR from dt_local) < 14
              GROUP BY 1"""
    curs.execute(quer.format(siteid))
    wsstats = pd.DataFrame(curs.fetchall(),columns=('ym','temp_min','temp_avg','temp_max','temp_std','temp_n','temp_o85','temp_o90'))
    wsstats['room_id'] = 'Outdoor'
    #concatenate cr and ws profile stats
    xstats = pd.concat([crstats,wsstats])
    xstats['year_i'] = pd.to_datetime(xstats['ym']).dt.year
    xstats['month_i'] = pd.to_datetime(xstats['ym']).dt.month - 1
    #trim columns
    fstats = xstats[['room_id','year_i','month_i','temp_min','temp_avg','temp_max','temp_std','temp_n','temp_o85','temp_o90']]
    #return formatted stats
    if len(fstats.index) > 0:
        return fstats.to_csv(sep=',',index=False)
    else: 
        return 'None'

##DATATRENDS##
def qSpanProfileData(args):
    #extract args
    siteid = str(args.get('siteid'))
    roomnm = str(args.get('roomnm'))
    start = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    end = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)

    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()

    #fetch profile data
    if roomnm == 'Outdoor':
        #fetch outdoor profile data from 'hidoe_siteweather'
        quer = """SELECT DISTINCT EXTRACT(hour from dt_local), EXTRACT(minute from dt_local)::int/60, AVG(temp_f)
                  FROM hidoe_siteweather2
                  WHERE site_id = '{}' AND dt_local >= '{}' AND dt_local <= '{}'
                  GROUP BY 1,2"""
        curs.execute(quer.format(siteid,start,end))
        data = pd.DataFrame(curs.fetchall(),columns=('h','m','temp_f'))
    else:
        quer = """SELECT room_id FROM hidoe_rooms WHERE site_id = '{}' and room_nm = '{}'"""
        curs.execute(quer.format(siteid,roomnm))
        roomid = curs.fetchall()[0][0]
        #fetch indoor profile data from 'hidoe_interiors'
        quer = """SELECT DISTINCT EXTRACT(hour from dt_local), EXTRACT(minute from dt_local)::int/60, AVG(temp_f)
                  FROM hidoe_interiors
                  WHERE site_id = '{}' AND room_id = '{}' AND dt_local >= '{}' AND dt_local <= '{}'
                  GROUP BY 1,2"""
        curs.execute(quer.format(siteid,roomid,start,end))
        data = pd.DataFrame(curs.fetchall(),columns=('h','m','temp_f'))
    #format response
    data['time_str'] = data['h'].map(str) + ':' + ((data['m']*3).map(int)).map(str) + '0'
    data['time'] = pd.to_datetime(data['time_str'],format='%H:%M')
    #return formatted data
    if len(data.index) > 0:
        data = data[['time_str','time','temp_f']]
        return data.to_csv(sep=',',index=False)
    else: 
        return 'None'

def qSpanProfileStats(args):
    #extract args
    siteid = str(args.get('siteid'))
    roomnm = str(args.get('roomnm'))
    start = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    end = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    schlhrs = str(args.get('schlhrs'))
    schldays = str(args.get('schldays'))
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch profile stats
    if roomnm == 'Outdoor':
        if (schlhrs == 'yes') & (schldays == 'yes'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_siteweather2 WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and EXTRACT(DOW from dt_local) != 0 and EXTRACT(DOW from dt_local) != 6 and EXTRACT(HOUR from dt_local) >= 8 and EXTRACT(HOUR from dt_local) < 14
                """
        elif (schlhrs == 'no') & (schldays == 'yes'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_siteweather2 WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and EXTRACT(HOUR from dt_local) >= 8 and EXTRACT(HOUR from dt_local) < 14
                """
        elif (schlhrs == 'yes') & (schldays == 'no'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_siteweather2 WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and EXTRACT(DOW from dt_local) != 0 and EXTRACT(DOW from dt_local) != 6
                """
        elif (schlhrs == 'no') & (schldays == 'no'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_siteweather2 WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}'
                """
        else:
            print('error')
        curs.execute(quer.format(siteid,start,end))
        data = pd.DataFrame(curs.fetchall(),columns=('temp_min','temp_avg','temp_max','temp_n','temp_o85','temp_o90'))
    else:
        quer = """SELECT room_id FROM hidoe_rooms WHERE site_id = '{}' and room_nm = '{}'"""
        curs.execute(quer.format(siteid,roomnm))
        roomid = curs.fetchall()[0][0]
        if (schlhrs == 'yes') & (schldays == 'yes'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_interiors WHERE site_id = '{}' and room_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and EXTRACT(DOW from dt_local) != 0 and EXTRACT(DOW from dt_local) != 6 and EXTRACT(HOUR from dt_local) >= 8 and EXTRACT(HOUR from dt_local) < 14
                """
        elif (schlhrs == 'no') & (schldays == 'yes'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_interiors WHERE site_id = '{}' and room_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and EXTRACT(HOUR from dt_local) >= 8 and EXTRACT(HOUR from dt_local) < 14
                """
        elif (schlhrs == 'yes') & (schldays == 'no'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_interiors WHERE site_id = '{}' and room_id = '{}' and dt_local >= '{}' and dt_local <= '{}' and EXTRACT(DOW from dt_local) != 0 and EXTRACT(DOW from dt_local) != 6
                """
        elif (schlhrs == 'no') & (schldays == 'no'):
            quer = """
                SELECT MIN(temp_f), AVG(temp_f), MAX(temp_f), COUNT(temp_f), SUM(CASE WHEN temp_f > 85 THEN 1 ELSE 0 END), SUM(CASE WHEN temp_f > 90 THEN 1 ELSE 0 END)
                FROM hidoe_interiors WHERE site_id = '{}' and room_id = '{}' and dt_local >= '{}' and dt_local <= '{}'
                """
        else:
            print('error')
        curs.execute(quer.format(siteid,roomid,start,end))
        data = pd.DataFrame(curs.fetchall(),columns=('temp_min','temp_avg','temp_max','temp_n','temp_o85','temp_o90'))
    #assess completeness
    if roomnm == 'Outdoor':
        quer = """
            SELECT DISTINCT date_trunc('hour',dt_local), EXTRACT(minute from dt_local)::int/60, COUNT(temp_f)
            FROM hidoe_siteweather2 WHERE site_id = '{}' and dt_local >= '{}' and dt_local <= '{}' GROUP BY 1,2
            """
        curs.execute(quer.format(siteid,start,end))
        dcount = len(curs.fetchall())
    else:
        quer = """
            SELECT DISTINCT date_trunc('hour',dt_local), EXTRACT(minute from dt_local)::int/60, COUNT(temp_f)
            FROM hidoe_interiors WHERE site_id = '{}' and room_id = '{}' and dt_local >= '{}' and dt_local <= '{}' GROUP BY 1,2
            """
        curs.execute(quer.format(siteid,roomid,start,end))
        dcount = len(curs.fetchall())
    compl = float(dcount/(int((end - start).days + 1.0) * 24.0) * 100.0)
    if (siteid == '414') & (roomnm == 'Room 30'):
        compl = compl * 2
    data['compl'] = compl
    #close rs connection
    rs_conn.close()
    return data.to_csv(sep=',',index=False)

def qDataExtents(args):
    #extract args
    siteid = str(args.get('siteid'))
    roomid = str(args.get('roomid'))
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    if roomid == 'Outdoor':
        quer = """SELECT MIN(dt_local), MAX(dt_local) FROM hidoe_siteweather2 WHERE site_id = '{}'"""
        curs.execute(quer.format(siteid))
        extents = pd.DataFrame(curs.fetchall(),columns=('start','end'))
    else:
        quer = """SELECT MIN(dt_local), MAX(dt_local) FROM hidoe_interiors WHERE site_id = '{}' AND room_id = '{}'"""
        curs.execute(quer.format(siteid,roomid))
        extents = pd.DataFrame(curs.fetchall(),columns=('start','end'))
    extents['start'] = pd.to_datetime(extents['start'])
    extents['end'] = pd.to_datetime(extents['end'])
    #close rs connection,return data
    rs_conn.close()
    return extents.to_csv(sep=',',index=False)

##DOWNLOAD DATA TRENDS##
def qGroupFile(args):
    #extract args
    siteid = str(args.get('siteid'))
    srmids = pd.DataFrame(str(args.get('srmids')).split('|'),columns=['room_id'])
    start = dt.datetime.strptime(str(args.get('start')),'%Y-%m-%d').replace(hour=0,minute=0)
    end = dt.datetime.strptime(str(args.get('end')),'%Y-%m-%d').replace(hour=23,minute=59)
    #connect to redshift
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    #fetch room data from 'hidoe_interiors'
    quer = """SELECT room_id,dt_local,temp_f,dewpt_f,relhum_pct
              FROM hidoe_interiors 
              WHERE site_id = '{}' AND dt_local >= '{}' AND dt_local <= '{}'"""
    curs.execute(quer.format(siteid,start,end))
    rmdata = pd.DataFrame(curs.fetchall(),columns=('room_id','timestamp','temp','dewpt','relh'))
    rmdata['site'] = 'X'
    rmdata['dist'] = 0
    rmdata['location'] = rmdata['room_id']
    rmdata['wspd'] = '-'
    rmdata['gspd'] = '-'
    rmdata['wdir'] = '-'
    rmdata['rain'] = '-'
    rmdata['solr'] = '-'
    rmdata = rmdata[rmdata['room_id'].isin(srmids['room_id'])].dropna(how='all')
    #fetch outdoor profile data from 'hidoe_siteweather'
    quer = """SELECT ws_loc,ws_dist,dt_local,temp_f,dewpt_f,relhum_pct,windspd_mph,gustspd_mph,winddir_deg,precip_inchesperhr,solrad_wpms
              FROM hidoe_siteweather2
              WHERE site_id = '{}' AND dt_local >= '{}' AND dt_local <= '{}'"""
    curs.execute(quer.format(siteid,start,end))
    wsdata = pd.DataFrame(curs.fetchall(),columns=('site','dist','timestamp','temp','dewpt','relh','wspd','gspd','wdir','rain','solr'))
    wsdata['location'] = 'Outdoors'
    #concatenate cr and ws profile data
    xdata = pd.concat([rmdata,wsdata])
    xdata = xdata.reindex(columns=('site','dist','location','timestamp','temp','dewpt','relh','wspd','gspd','wdir','rain','solr'))
    #close rs connection,return data
    rs_conn.close()
    return xdata.to_csv(sep=',',index=False)

#SCHOOLS AIR QUALITY BANNER
def qClosestAqStation(args):
    #process args
    lat = float(args.get('lat'))
    lng = float(args.get('lng'))
    #fetch list of aq stations
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    quer = """SELECT id,lat,lng,location FROM hidoe_stations2 WHERE type = 'airqual'"""
    curs.execute(quer)
    stations = pd.DataFrame(curs.fetchall(),columns=['id','lat','lng','location'])
    rs_conn.close()
    #loop stations find, closest one to site
    dist = 99999999
    # xsta = pd.DataFrame(columns=['id','lat','lng','location','dist'])
    xsta = []
    for i,station in stations.iterrows():
        d = calcdist(lat,station['lat'],lng,station['lng'])
        if d < dist:
            dist = d
            xsta = [[station['id'],station['lat'],station['lng'],station['location'],d]]
    return pd.DataFrame(xsta,columns=['id','lat','lng','location','dist']).to_csv(sep=',',index=False)

def qAqStationData(args):
    #process args
    aqid = str(args.get('id'))
    lat = float(args.get('lat'))
    lng = float(args.get('lng'))
    #query rs
    rs_conn = pg.connect(host=rs[0],user=rs[1],port=rs[2],password=rs[3],dbname=rs[4])
    curs = rs_conn.cursor()
    quer = """SELECT location,dt_local,so2_ppb,so2_aqi,pm10_ugperm3,pm10_aqi,pm2pt5_ugperm3,pm2pt5_aqi,co_ppm,co_aqi,o3_ppb,o3_aqi FROM hidoe_airquality WHERE id = '{}' and dt_local >= '{}'"""
    curs.execute(quer.format(aqid,'2018-01-01'))
    data = curs.fetchall()
    rs_conn.close()
    data = pd.DataFrame(data,columns=['location','dt_local','SO2_ppb','SO2_aqi','PM10_μg/m3','PM10_aqi','PM2pt5_μg/m3','PM2pt5_aqi','CO_ppm','CO_aqi','OZONE_ppb','OZONE_aqi'])
    data['lat'] = lat
    data['lng'] = lng
    data = data[['lat','lng','location','dt_local','SO2_ppb','SO2_aqi','PM10_μg/m3','PM10_aqi','PM2pt5_μg/m3','PM2pt5_aqi','CO_ppm','CO_aqi','OZONE_ppb','OZONE_aqi']]
    data = data.applymap(lambda x: '-' if isnegative(x) else x)
    return data.to_csv(sep=',',index=False)